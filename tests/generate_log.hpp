/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Standard headers */
#include <cstddef>
#include <string>

/* Include config */
#include "config.hpp"

/* Local headers */
#include "error.hpp"
#include "logfile_writer.h"
#include "particle_type.hpp"

/* Define the masks */
// The two first are already picked (special flags + timestamp)
#define MASK_POSITION (1 << 2)
#define MASK_VELOCITY (1 << 3)
#define MASK_ACCELERATION (1 << 4)
#define MASK_ID (1 << 5)

#define TEST_NUMBER_MASKS 4
#define number_steps 100

struct csds_part {
  /* Position */
  double x[3];

  /* Velocity */
  float v[3];

  /* Acceleration */
  float a[3];

  /* IDs of the particle */
  long long id;

  /* Offset of the last record */
  uint64_t last_offset;

  /* Time bin (number of steps skipped) */
  int time_bin;
};

/**
 * @brief Generate the data of a bunch of particles.
 *
 * @param parts The list of particles.
 * @param nparts The number of particles.
 */
void generate_particles(struct csds_part *parts, size_t nparts) {

  for (size_t i = 0; i < nparts; i++) {
    parts[i].id = i;
    parts[i].last_offset = 0;

    for (int j = 0; j < 3; j++) {
      parts[i].x[j] = j;
      parts[i].v[j] = 0;
      parts[i].a[j] = 0;
    }

    /* Add time bin in order to skip particles. */
    parts[i].time_bin = (i % 10) + 1;
  }
}

/** @brief Get a double value from an int */
double get_double_time(int i) { return i * 0.12; }

/** @brief Get an integertime_t from an int */
integertime_t get_integer_time(int i) { return 100 * i; }

/**
 * @brief Write a timestamp into the logfile.
 *
 * @param log The #csds_logfile_writer.
 * @param ti_int The time as an integertime_t
 * @param ti_double The time as a double
 * @param offset (in) The last offset of the timestamp. (out) The new offset.
 */
void csds_log_timestamp(struct csds_logfile_writer *log, integertime_t ti_int,
                        double ti_double, size_t *offset) {

  /* Start by computing the size of the message. */
  const int size = CSDS_TIMESTAMP_SIZE + CSDS_HEADER_SIZE;

  /* Allocate a chunk of memory in the logfile of the right size. */
  size_t offset_new;
  char *buff = (char *)csds_logfile_writer_get(log, size, &offset_new);

  /* Write the header. */
  unsigned int mask = CSDS_TIMESTAMP_MASK;
  buff = csds_write_record_header(buff, &mask, offset, offset_new);

  /* Store the timestamp. */
  memcpy(buff, &ti_int, sizeof(integertime_t));
  buff += sizeof(integertime_t);

  /* Store the time. */
  memcpy(buff, &ti_double, sizeof(double));

  /* Update the log message offset. */
  *offset = offset_new;
}

/**
 * @brief Write a particle into the logfile.
 *
 * @param log The #csds_logfile_writer.
 * @param part The #csds_part to log.
 */
void csds_log_part(struct csds_logfile_writer *log, struct csds_part *part,
                   const enum csds_special_flags flag) {
  const size_t size_flag = flag == csds_flag_none ? 0 : CSDS_SPECIAL_FLAGS_SIZE;
  const size_t size_total = sizeof(part->x) + sizeof(part->v) +
                            sizeof(part->a) + sizeof(part->id) +
                            CSDS_HEADER_SIZE + size_flag;

  // For performances, this should never be done on a single
  // particle at a time.
  size_t offset = 0;
  char *buff = (char *)csds_logfile_writer_get(log, size_total, &offset);
  const unsigned int mask_flag =
      flag == csds_flag_none ? 0 : CSDS_SPECIAL_FLAGS_MASK;
  const unsigned int mask =
      MASK_POSITION | MASK_VELOCITY | MASK_ACCELERATION | MASK_ID | mask_flag;

  /* Write the header */
  buff = csds_write_record_header(buff, &mask, &part->last_offset, offset);
  part->last_offset = offset;

  /* Write the special flag */
  if (flag != csds_flag_none) {
    const uint32_t data = csds_pack_flags_and_data(flag, 0, csds_type_gas);
    memcpy(buff, &data, sizeof(uint32_t));
    buff += sizeof(uint32_t);
  }

  /* Write the position */
  memcpy(buff, part->x, sizeof(part->x));
  buff += sizeof(part->x);

  /* Write the velocity */
  memcpy(buff, part->v, sizeof(part->v));
  buff += sizeof(part->v);

  /* Write the acceleration */
  memcpy(buff, part->a, sizeof(part->a));
  buff += sizeof(part->a);

  /* Write the ID */
  memcpy(buff, &part->id, sizeof(part->id));
  buff += sizeof(part->id);
}

/**
 * @brief Write all the particles into the logfile.
 *
 * @param log The #csds_logfile_writer.
 * @param parts The #csds_part to log.
 * @param nparts The number of particles to log.
 * @param first_log Is it the first or last log?
 */
void csds_log_all_particles(struct csds_logfile_writer *log,
                            struct csds_part *part, size_t nparts,
                            int first_log) {

  const enum csds_special_flags flag =
      first_log ? csds_flag_create : csds_flag_delete;

#pragma omp parallel for
  for (size_t i = 0; i < nparts; i++) {
    csds_log_part(log, &part[i], flag);
  }
}

/**
 * @brief Write a few particles during multiple time steps.
 *
 * As only the csds is tested, there is no need to really
 * evolve the particles.
 *
 * @param log The #csds_logfile_writer.
 * @param parts The particles to "simulate".
 * @param nparts The number of particles
 */
void write_particles(struct csds_logfile_writer *log, struct csds_part *parts,
                     size_t nparts, size_t *timestamp_offset) {

  /* Loop over all the steps. */
  for (int i = 1; i <= number_steps; i++) {
    integertime_t ti_int = get_integer_time(i);
    double ti_double = get_double_time(i);

    /* Mark the current time step in the particle csds file. */
    csds_log_timestamp(log, ti_int, ti_double, timestamp_offset);

    /* Make sure that we have enough space in the particle csds file
     * to store the particles in current time step. */
    // Here we need to have enough place for the part + the record header.
    csds_logfile_writer_ensure(log, 1.2 * nparts * sizeof(struct csds_part),
                               5 * nparts * sizeof(struct csds_part));

    /* Loop over all the particles. */
#pragma omp parallel for
    for (size_t j = 0; j < nparts; j++) {

      /* Skip some particles. */
      if (i % parts[j].time_bin != 0) continue;

      /* Write a time information to check that the correct particle is read. */
      parts[j].x[0] = i;

      csds_log_part(log, &parts[j], csds_flag_none);
    }
  }
}

/**
 * @brief Generate a logfile.
 *
 * @param parts The array of #csds_part to use
 * @param nparts The number of particles.
 * @param filename The filename of the logfile.
 */
void generate_log(struct csds_part *parts, size_t nparts,
                  std::string filename) {

#ifdef HAVE_PYTHON
  message(
      "WARNING: error messages might crash as python is enabled but not "
      "initialized. If you obtain a segmentation fault, please recompile "
      "without python");
#endif

  /* Initialize the particles */
  generate_particles(parts, nparts);

  /* Initialize the writer */
  struct csds_logfile_writer log;

  const size_t size1 = sizeof(struct csds_part) * 1000;
  const size_t size2 = 1.5 * nparts * sizeof(struct csds_part);
  const size_t init_size = size1 > size2 ? size1 : size2;
  csds_logfile_writer_init(&log, filename.data(), init_size);

  /* Write file header */
  char *first_offset = csds_logfile_writer_write_begining_header(&log);

  /* Write the number of masks */
  size_t file_offset = 0;
  // Add timestamp + special flags
  const unsigned int number_masks = TEST_NUMBER_MASKS + 2;
  csds_write_data(&log, &file_offset, sizeof(unsigned int),
                  (char *)&number_masks);

  /* Write the masks */
  const char *names[TEST_NUMBER_MASKS + 2] = {
      CSDS_SPECIAL_FLAGS_NAME, CSDS_TIMESTAMP_NAME, "Coordinates", "Velocities",
      "Accelerations",         "ParticleIDs",
  };
  const unsigned int sizes[TEST_NUMBER_MASKS + 2] = {
      CSDS_SPECIAL_FLAGS_SIZE, CSDS_TIMESTAMP_SIZE, sizeof(parts->x),
      sizeof(parts->v),        sizeof(parts->a),    sizeof(parts->id),
  };
  for (unsigned int i = 0; i < number_masks; i++) {
    char tmp[CSDS_STRING_SIZE];
    strcpy(tmp, names[i]);
    csds_write_data(&log, &file_offset, CSDS_STRING_SIZE, tmp);
    csds_write_data(&log, &file_offset, sizeof(unsigned int),
                    (char *)&sizes[i]);
  }

  /* Write the number of fields per particle types */
  int number_fields[csds_type_count];
  for (int i = 0; i < csds_type_count; i++) {
    number_fields[i] = TEST_NUMBER_MASKS;
  }
  csds_write_data(&log, &file_offset, csds_type_count * sizeof(int),
                  (char *)number_fields);

  /* Write the order for each particle type */
  for (int i = 0; i < csds_type_count; i++) {
    const int order[TEST_NUMBER_MASKS] = {2, 3, 4, 5};
    csds_write_data(&log, &file_offset, TEST_NUMBER_MASKS * sizeof(int),
                    (char *)order);
  }

  /* Write the end of the header */
  csds_logfile_writer_write_end_header(&log, first_offset);

  /* Mark the current time step in the particle csds file. */
  integertime_t ti_current = 0;
  double time = 0;
  size_t timestamp_offset = 0;
  csds_log_timestamp(&log, ti_current, time, &timestamp_offset);

  /* Make sure that we have enough space in the particle csds file
   * to store the particles in current time step. */
  csds_logfile_writer_ensure(&log, 1.2 * nparts * sizeof(struct csds_part),
                             5 * nparts * sizeof(struct csds_part));

  /* Log all the particles before starting */
  csds_log_all_particles(&log, parts, nparts, /* first_log */ 1);

  /* Write particles */
  write_particles(&log, parts, nparts, &timestamp_offset);

  /* Write a sentinel timestamp */
  ti_current = get_integer_time(number_steps);
  time = get_double_time(number_steps);
  csds_log_timestamp(&log, ti_current, time, &timestamp_offset);

  /* Write all the particles at the end */
  csds_log_all_particles(&log, parts, nparts, /* first_log */ 0);

  /* Write a sentinel timestamp */
  csds_log_timestamp(&log, ti_current, time, &timestamp_offset);

  /* Cleanup the memory */
  csds_logfile_writer_close(&log);
}
