/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include local headers */
#include "header.hpp"
#include "logfile.hpp"
#include "parameters.hpp"
#include "particle.hpp"

/* Include standard headers */
#include <assert.h>
#include <math.h>

#define err_abs(a, b) fabs(a - b)

void pos_func(double t, double *p) {
  const double t2 = t * t;
  const double f = exp(t2 - 5 * t - 2);
  p[0] = f;
  p[1] = f;
  p[2] = f;
}

void vel_func(double t, float *v) {
  const double t2 = t * t;
  const float f = (2 * t - 5) * exp(t2 - 5 * t - 2);
  v[0] = f;
  v[1] = f;
  v[2] = f;
}

void acc_func(double t, float *a) {
  const double t2 = t * t;
  const float f = (4 * t2 - 20 * t + 27) * exp(t2 - 5 * t - 2);
  a[0] = f;
  a[1] = f;
  a[2] = f;
}

long long id_func(void) { return 1698241543; }

int main(void) {
  /* Initialize the parameters */
  Parameters params("test_interpolation.yml", 2);

  /* Construct the fields */
  const int n_fields = 4;
  std::vector<struct mask_data> data(n_fields);

  /* Set the coordinate */
  data[0].size = 3 * sizeof(double);
  data[0].mask = 1 << 2;
  strcpy(data[0].name, "Coordinates");

  /* Set the velocity */
  data[1].size = 3 * sizeof(float);
  data[1].mask = 1 << 3;
  strcpy(data[1].name, "Velocities");

  /* Set the acceleration */
  data[2].size = 3 * sizeof(float);
  data[2].mask = 1 << 4;
  strcpy(data[2].name, "Accelerations");

  /* Set the IDs */
  data[3].size = sizeof(id_type);
  data[3].mask = 1 << 5;
  strcpy(data[3].name, "ParticleIDs");

  /* Construct the field information */
  std::vector<Field> fields;
  fields.reserve(n_fields);
  for (int i = 0; i < n_fields; i++) {
    fields.emplace_back(data[i].name, data);
  }

  /* Now test */
  const int n_evaluations = 10;
  const double t_beg = 0.2;
  const double t_end = 0.8;
  const double t_eval = 0.5 * (t_beg + t_end);
  double last_error[n_fields];
  for (int i = 0; i < n_fields; i++) {
    last_error[i] = 0;
  }

  /* Check the convergence */
  for (int i = 1; i <= n_evaluations; i++) {
    /* Get the time */
    const double dt = 0.5 * (t_end - t_beg) / i;
    const double t1 = t_eval - dt;
    const double t2 = t_eval + dt;
    message("Interpolating at t=" << t_eval << " from t=[" << t1 << ", " << t2
                                  << "]");

    /* Set the state at t1 and t2 */
    double p1[3];
    pos_func(t1, p1);
    double p2[3];
    pos_func(t2, p2);

    float v1[3];
    vel_func(t1, v1);
    float v2[3];
    vel_func(t2, v2);

    float a1[3];
    acc_func(t1, a1);
    float a2[3];
    acc_func(t2, a2);

    long long id1 = id_func();
    long long id2 = id_func();

    /* Create the output variables */
    double out_p[3];
    float out_v[3];
    float out_a[3];
    long long out_id = 0;

    /* Do the interpolations */
    /* Coordinates */
    struct csds_reader_field before = {
        .field = p1,
        .first_deriv = v1,
        .second_deriv = a1,
    };
    struct csds_reader_field after = {
        .field = p2,
        .first_deriv = v2,
        .second_deriv = a2,
    };

    csds_particle_interpolate_field(t1, before, t2, after, &out_p, t_eval,
                                    fields[0], params);

    /* Velocities */
    before = (struct csds_reader_field){
        .field = v1,
        .first_deriv = a1,
        .second_deriv = NULL,
    };
    after = (struct csds_reader_field){
        .field = v2,
        .first_deriv = a2,
        .second_deriv = NULL,
    };

    csds_particle_interpolate_field(t1, before, t2, after, &out_v, t_eval,
                                    fields[1], params);

    /* Accelerations */
    before = (struct csds_reader_field){
        .field = a1,
        .first_deriv = NULL,
        .second_deriv = NULL,
    };
    after = (struct csds_reader_field){
        .field = a2,
        .first_deriv = NULL,
        .second_deriv = NULL,
    };

    csds_particle_interpolate_field(t1, before, t2, after, &out_a, t_eval,
                                    fields[2], params);

    /* IDs */
    before = (struct csds_reader_field){
        .field = &id1,
        .first_deriv = NULL,
        .second_deriv = NULL,
    };
    after = (struct csds_reader_field){
        .field = &id2,
        .first_deriv = NULL,
        .second_deriv = NULL,
    };

    csds_particle_interpolate_field(t1, before, t2, after, &out_id, t_eval,
                                    fields[3], params);

    /* Now check the results */
    double current_error[n_fields];
    double p_ref[3];
    pos_func(t_eval, p_ref);
    current_error[0] = err_abs(out_p[0], p_ref[0]);

    float v_ref[3];
    vel_func(t_eval, v_ref);
    current_error[1] = err_abs(out_v[0], v_ref[0]);

    float a_ref[3];
    acc_func(t_eval, a_ref);
    current_error[2] = err_abs(out_a[0], a_ref[0]);

    assert(out_id == id_func());

    /* For the first element, we cannot check the convergence */
    if (i != 1) {
      const double fact = (i - 1.) / (double)i;
      /* Quintic convergence for coordinates */
      assert(current_error[0] <= fmax(last_error[0] * pow(fact, 5), 1e-10));
      /* Cubic convergence for velocities */
      assert(current_error[1] <= fmaxf(last_error[1] * pow(fact, 3), 1e-6));
      /* Linear convergence for accelerations */
      assert(current_error[2] <= fmaxf(last_error[2] * fact, 1e-6));
    }

    /* Update the last error */
    for (int field = 0; field < n_fields; field++) {
      last_error[field] = current_error[field];
    }
  }

  return 0;
}
