/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/* Standard headers */
#include <stddef.h>

/* Include config */
#include "config.hpp"

/* Local headers */
#include "logfile_writer.h"
#include "particle_type.hpp"

#define TEST_NUMBER_MASKS 1
#define MASK_FIELD 1 << 2

void main(void) {

  char filename[] = "test_writer_0000.dump";

  /* Initialize the writer */
  struct csds_logfile_writer log;
  const size_t init_size = 10000; // Randomly picked a "large" number
  csds_logfile_writer_init(&log, filename, init_size);

  /* The field to write */
  double field = 10;

  /* Write file header */
  char *first_offset = csds_logfile_writer_write_begining_header(&log);

  /* Write the number of masks */
  size_t file_offset = 0;
  // Add timestamp + special flags
  const unsigned int number_masks = TEST_NUMBER_MASKS + 2;
  csds_write_data(&log, &file_offset, sizeof(unsigned int),
                  (char *)&number_masks);

  /* Write the masks */
  const char *names[TEST_NUMBER_MASKS + 2] = {
      CSDS_SPECIAL_FLAGS_NAME, CSDS_TIMESTAMP_NAME, "Coordinates"
  };
  const unsigned int sizes[TEST_NUMBER_MASKS + 2] = {
      CSDS_SPECIAL_FLAGS_SIZE, CSDS_TIMESTAMP_SIZE, sizeof(field)
  };

  for (unsigned int i = 0; i < number_masks; i++) {
    char tmp[CSDS_STRING_SIZE];
    strcpy(tmp, names[i]);
    csds_write_data(&log, &file_offset, CSDS_STRING_SIZE, tmp);
    csds_write_data(&log, &file_offset, sizeof(unsigned int),
                    (char *)&sizes[i]);
  }

  /* Write the number of fields per particle types */
  int number_fields[csds_type_count];
  for (int i = 0; i < csds_type_count; i++) {
    number_fields[i] = TEST_NUMBER_MASKS;
  }
  csds_write_data(&log, &file_offset, csds_type_count * sizeof(int),
                  (char *)number_fields);

  /* Write the order for each particle type */
  for (int i = 0; i < csds_type_count; i++) {
    const int order[TEST_NUMBER_MASKS] = {2};
    csds_write_data(&log, &file_offset, TEST_NUMBER_MASKS * sizeof(int),
                    (char *)order);
  }

  /* Write the end of the header */
  csds_logfile_writer_write_end_header(&log, first_offset);

  /* Get the buffer for the record */
  size_t offset = 0;
  size_t size = sizeof(field) + CSDS_HEADER_SIZE + CSDS_SPECIAL_FLAGS_SIZE;
  char *buff = (char *) csds_logfile_writer_get(&log, size, &offset);

  /* Write the header */
  const unsigned int mask = MASK_FIELD | CSDS_SPECIAL_FLAGS_MASK;
  buff = csds_write_record_header(buff, &mask, &offset, offset);

  /* Write the special mask */
  const uint32_t data = csds_pack_flags_and_data(csds_flag_create,
                                                 0, csds_type_gas);
  memcpy(buff, &data, sizeof(data));

  /* Write the data */
  memcpy(buff, &field, sizeof(field));

  /* Cleanup the memory */
  csds_logfile_writer_close(&log);
}
