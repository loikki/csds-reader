/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "time_array.hpp"

#include <assert.h>
#include <stdlib.h>
#include <time.h>

#define NUMBER_OF_ELEMENT 10000
#define TIME_BASE 0.04
#define OFFSET_BASE 1000

int main(void) {

  /* Fix the random seed in order to reproduce the results */
  srand(100);

  /* Initialize the time array */
  TimeArray times;

  /* Add elements */
  for (size_t i = 0; i < NUMBER_OF_ELEMENT; i++) {
    struct time_record time;
    time.int_time = i;
    time.time = i * TIME_BASE;
    time.offset = i * OFFSET_BASE;

    times.Append(time);
  }

  /* Check the elements */
  for (size_t i = 0; i < NUMBER_OF_ELEMENT; i++) {
    integertime_t int_time = i;
    double time = i * TIME_BASE;
    size_t offset = i * OFFSET_BASE;

    /* Ensure that we can get the correct offset when looking
       in between the records. */
    int r = rand() % OFFSET_BASE;
    size_t read_offset = offset + r;

    /* The offset cannot be larger than the largest one */
    if (i == NUMBER_OF_ELEMENT - 1) {
      read_offset = offset;
    }

    /* Get the index from the offset */
    size_t ind = times.GetIndexFromOffset(read_offset);

    /* Check the values obtained */
    assert(i == ind);
    assert(int_time == times[ind].int_time);
    assert(time == times[ind].time);
    assert(offset == times[ind].offset);
  }

  return 0;
}
