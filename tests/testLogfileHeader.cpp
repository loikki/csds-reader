/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include local headers */
#include "generate_log.hpp"
#include "header.hpp"
#include "logfile.hpp"
#include "parser.hpp"

/* Include standard headers */
#include <assert.h>

int main(void) {

  /*
    First generate the file.
  */

  message("Generating the dump.");
  /* Create required structures. */
  std::string basename = "test_header_0000";
  std::string filename = basename + ".dump";

  /* Write file header. */
  struct csds_part *parts = NULL;
  size_t nparts = 0;
  generate_log(parts, nparts, filename);

  /*
    Then read the file.
  */

  message("Reading the header.");
  /* Generate required structure for reading. */
  LogFile logfile(basename,
                  /* only_header */ 1,
                  /* verbose */ 2);
  /*
    Finally check everything.
  */

  const Header &h = logfile.GetHeader();
  message("Checking versions.");
  assert(h.GetMajorVersion() == CSDS_MAJOR_VERSION);
  assert(h.GetMinorVersion() == CSDS_MINOR_VERSION);

  message("Checking number of masks");
  /* Compute the number of masks */
  int count_fields = 2;  // Timestamp + special flags
  for (int type = 0; type < csds_type_count; type++) {
    count_fields += h.GetFields()[type].size();
    /* Remove the copies of the special flags */
    if (h.GetFields()[type].size() != 0) count_fields -= 1;
  }
  const int number_masks_ref = csds_type_count * TEST_NUMBER_MASKS + 2;
  assert(number_masks_ref == count_fields);

  message("Checking offset direction");
  assert(h.GetOffsetDirection() == csds_offset_backward);

  return 0;
}
