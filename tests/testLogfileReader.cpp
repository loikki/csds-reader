/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Local header */
#include "header.hpp"
#include "mapped_file.hpp"
#include "particle.hpp"
#include "reader.hpp"

/* Tests header */
#include "generate_log.hpp"

#include <assert.h>

#define number_parts 100

/** Count the number of active particles. */
int get_number_active_particles(int step, struct csds_part *p) {
  int count = 0;
  for (int i = 0; i < number_parts; i++) {
    if (step % p[i].time_bin == 0) count += 1;
  }
  return count;
}

/**
 * @brief Check that the reader contains the correct data
 *
 * @param reader The #csds_reader.
 */
void check_data(Reader &reader, struct csds_part *parts) {

  /* No need to check the header, this is already done in testHeader.c */

  /* Get required structures. */
  LogFile &logfile = reader.GetLogFile();
  const Header &h = reader.GetHeader();

  /* Index of the fields */
  int index_ids = -1;
  int index_coordinates = -1;

  /* Create a particle */
  std::vector<CsdsArray> output;
  output.reserve(h.GetFields()[csds_type_gas].size());
  for (size_t i = 0; i < h.GetFields()[csds_type_gas].size(); i++) {
    output.emplace_back(1, h.GetFields()[csds_type_gas][i]);
    if (h.GetFields()[csds_type_gas][i].GetField() ==
        field_enum_particles_ids) {
      index_ids = i;
    }
    if (h.GetFields()[csds_type_gas][i].GetField() == field_enum_coordinates) {
      index_coordinates = i;
    }
  }

  /* Define a few variables */

  /* Number of particle found during this time step. */
  int count = 0;
  /* Set it to an impossible value in order to flag it. */
  const uint64_t id_flag = 5 * number_parts;
  uint64_t previous_id = id_flag;

  size_t offset = h.GetOffsetFirstRecord();
  size_t offset_time = offset;
  int step = -1;
  /* Loop over each record. */
  while (offset < logfile.GetFileSize()) {
    const bool is_particle = offset != offset_time;

    struct time_record record;
    struct record_header header;
    logfile.ReadRecordHeader(offset, header);

    /* Read the particle */
    if (is_particle) {
      size_t offset_after = 0;
      for (auto &current : output) {
        offset_after = csds_particle_read_field(
            offset, current[0], current.GetField(),
            /* derivative */ 0, header, h.GetFields()[csds_type_gas], logfile);
      }
      offset = offset_after;
    }
    /* Read the time */
    else {
      offset = logfile.ReadTimeRecord(record, offset);
      offset_time += header.offset;
    }

    /* Do the particle case */
    if (is_particle) {
      count += 1;

      /*
        Check that we are really increasing the id in the logfile.
        See the writing part to see that we are always increasing the id.
      */
      const uint64_t current_id = *(uint64_t *)output[index_ids][0];

      if (previous_id != id_flag && previous_id >= current_id) {
        csds_error("Wrong particle found");
        previous_id = current_id;
      }

      /* Get the corresponding particle */
      if (current_id >= number_parts) csds_error("Wrong id: " << current_id);

      struct csds_part *p = &parts[current_id];
      const double *pos = (double *)output[index_coordinates][0];

      /* Check the record's data. */
      for (int i = 0; i < 3; i++) {
        /* in the first index, we are storing the step information. */
        if (i == 0) {
          double tmp = step;
          /* At the end, we are not updating the particle */
          if (step >= number_steps + 1) {
            int n_1 = number_steps;
            tmp = n_1 - (n_1 % p->time_bin);
          }
          assert(tmp == pos[i]);
        } else
          assert(p->x[i] == i);
      }
    }
    /* Time stamp case. */
    else {
      step += 1;
      message("Step: " << step);
      /* Check if we have the current amount of particles in previous step. */
      if (step != 0 && step != number_steps + 2 &&
          count != get_number_active_particles(step - 1, parts))
        csds_error(
            "The reader did not find the correct number of particles during "
            "step "
            << step << ": " << count
            << " != " << get_number_active_particles(step, parts));

      /* Reset some variables. */
      previous_id = id_flag;
      count = 0;

      /* Check the record's data. */
      const int tmp_step = step >= number_steps ? number_steps : step;
      assert(record.time == get_double_time(tmp_step));
    }
  }
}

int main(void) {

  /*
    First generate the file.
  */

  message("Generating the dump.");

  /* Create required structures. */
  char filename[200] = "test_reader_0000.dump";

  /* Delete the index files */
  remove("test_reader_0000_0000.index");
  remove("test_reader_0000_0001.index");
  remove("test_reader_0000_0002.index");
  remove("test_reader_0000_0003.index");
  remove("test_reader_0000_0004.index");

  /* Initialize the particles. */
  struct csds_part *parts;
  if ((parts = (struct csds_part *)malloc(sizeof(struct csds_part) *
                                          number_parts)) == NULL)
    csds_error("Failed to allocate particles array.");

  /* Write a 'simulation' */
  generate_log(parts, number_parts, filename);

  /*
    Then read the file.
  */

  message("Reading the header.");

  /* Generate required structure for reading. */
  std::string basename = "test_reader_0000";
  Reader reader(basename, /* verbose */ 1,
                /* number_index*/ 5,
                /* restart */ 0,
                /* use_cache */ 0);

  /*
    Finally check everything.
  */
  check_data(reader, parts);

  /* Do some cleanup. */
  free(parts);

  return 0;
}
