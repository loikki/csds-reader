/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (C) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *                    Florian Cabot (florian.cabot@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include configuration */
#include "config.hpp"

/* Standard include */
#include <stdio.h>
#include <stdlib.h>

/* Local include */
#include "generate_log.hpp"
#include "reader.hpp"

#define number_parts 100

/**
 * This function test the csds in the Virtual Reality mode.
 * The idea is to simply read a snapshot at a given time and
 * then simply advance in time the particles.
 */
int main(void) {
  /* Create required structures. */
  char filename[200] = "testvr_0000.dump";

  /* Delete the index files */
  remove("testvr_0000_0000.index");
  remove("testvr_0000_0001.index");
  remove("testvr_0000_0002.index");
  remove("testvr_0000_0003.index");
  remove("testvr_0000_0004.index");

  /* Initialize the particles. */
  struct csds_part *parts;
  if ((parts = (struct csds_part *)malloc(sizeof(struct csds_part) *
                                          number_parts)) == NULL)
    csds_error("Failed to allocate particles array.");

  /* Write a 'simulation' */
  generate_log(parts, number_parts, filename);

  /* Initialize the reader */
  std::string basename = "testvr_0000";
  Reader reader(basename,
                /* Verbose */ 2,
                /* number_index */ 5,
                /* restart */ 0,
                /* use_cache */ 1);

  /* Read the time limits */
  double begin = reader.GetTimeBegin();
  double end = reader.GetTimeEnd();

  /* Set the time */
  reader.SetTime(begin);

  /* Create the variables for the number of particles */
  const int n_type = csds_type_count;
  std::array<uint64_t, csds_type_count> n_parts;
  std::array<bool, csds_type_count> read_types;

  /* Set the flags in order to read everything */
  for (int i = 0; i < n_type; i++) {
    read_types[i] = true;
  }

  /* Get the number of particles */
  reader.GetNumberParticles(n_parts, read_types);

  uint64_t n_tot = 0;
  for (int i = 0; i < n_type; i++) {
    n_tot += n_parts[i];
  }

  /* Create the output */
  std::vector<CsdsArray> output;
  LogFile &log = reader.GetLogFile();
  for (auto field : log.GetHeader().GetFields()[csds_type_gas]) {
    if (field.GetField() == field_enum_coordinates ||
        field.GetField() == field_enum_particles_ids) {
      output.emplace_back(n_tot, field);
    }
  }

  /* Read the next time */
  reader.ReadAllParticles(begin, output, n_parts);

  /* Loop over time for a single particle */
  int part_ind = 0;
  for (double t = begin; t < end; t += (end - begin) / number_steps) {

    reader.SetTime(t);

    reader.UpdateParticles(t, output, n_parts);

    long long *ids = (long long *)output[0][part_ind];
    double *pos = (double *)output[1][part_ind];

    message("Particle " << *ids << ": " << pos[0] << " " << pos[1] << ": "
                        << pos[2] << " " << t);
  }

  /* Cleanup the memory */
  free(parts);
  return 0;
}
