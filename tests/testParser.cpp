/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Standard headers */
#include <assert.h>
#include <cstddef>
#include <math.h>
#include <string.h>

/* Local headers */
#include "definitions.hpp"
#include "parser.hpp"

int main(void) {

  Parser parser("test_parser.yml");
  parser.PrintParams();

  /* scalar reader */
  assert(parser.GetParam<char>("Test:char") == 'c');
  assert(parser.GetParam<int>("Test:int") == 10);
  float ftest = parser.GetParam<float>("Test:float");
  assert(fabsf(ftest - 0.2f) < 1e-6);
  assert(parser.GetParam<double>("Test:double") == 0.2);
  assert(parser.GetParam<long long>("Test:longlong") == 1254108);

  char ret_param[CSDS_STRING_SIZE];
  parser.GetParamString("Test:string", ret_param);
  assert(strcmp(ret_param, "test") == 0);

  /* Optional scalar reader (failed) */
  assert(parser.GetOptParam<char>("Test:test", 'd') == 'd');
  assert(parser.GetOptParam<int>("Test:test", 2) == 2);
  ftest = parser.GetOptParam<float>("Test:test", 0.1);
  assert(fabsf(ftest - 0.1f) < 1e-6);
  assert(parser.GetOptParam<double>("Test:test", 0.1) == 0.1);
  assert(parser.GetOptParam<long long>("Test:test", 1) == 1);

  parser.GetOptParamString("Test:test", ret_param, "test1");
  assert(strcmp(ret_param, "test1") == 0);

  /* Optional scalar reader (pass) */
  assert(parser.GetOptParam<char>("Test:char", 'd') == 'c');
  assert(parser.GetOptParam<int>("Test:int", 1) == 10);
  ftest = parser.GetOptParam<float>("Test:float", 0.1);
  assert(fabsf(ftest - 0.2f) < 1e-6);
  assert(parser.GetOptParam<double>("Test:double", 0.1) == 0.2);
  assert(parser.GetOptParam<long long>("Test:longlong", 1) == 1254108);

  parser.GetOptParamString("Test:string", ret_param, "test1");
  assert(strcmp(ret_param, "test") == 0);

  /* Array reader */
  const int nval = 3;
  char c[nval];
  int i[nval];
  float f[nval];
  double d[nval];
  long long l[nval];

  parser.GetParamArray<char, nval>("Test:char_array", c);
  parser.GetParamArray<int, nval>("Test:int_array", i);
  parser.GetParamArray<float, nval>("Test:float_array", f);
  parser.GetParamArray<double, nval>("Test:double_array", d);
  parser.GetParamArray<long long, nval>("Test:longlong_array", l);

  /* Check the values */
  assert(c[0] == 'a');
  assert(c[1] == 'b');
  assert(c[2] == 'c');

  for (int k = 0; k < nval; k++) {
    int m = k + 1;
    assert(i[k] == m);
    assert(fabsf(f[k] - m * 0.1f) < 1e-6);
    assert(fabs(d[k] - m * 0.1) < 1e-12);
    if (k == 0) {
      assert(l[k] == 1234567890);
    } else {
      assert(l[k] == m);
    }
  }

  /* Optional array reader (failed) */
  c[0] = 'e';
  c[1] = 'f';
  c[2] = 'g';
  i[0] = 3;
  i[1] = 4;
  i[2] = 5;
  f[0] = 0.3;
  f[1] = 0.4;
  f[2] = 0.5;
  d[0] = 0.3;
  d[1] = 0.4;
  d[2] = 0.5;
  l[0] = 9876543210;
  l[1] = 4;
  l[2] = 5;

  parser.GetOptParamArray<char, nval>("Test:test", c);
  parser.GetOptParamArray<int, nval>("Test:test", i);
  parser.GetOptParamArray<float, nval>("Test:test", f);
  parser.GetOptParamArray<double, nval>("Test:test", d);
  parser.GetOptParamArray<long long, nval>("Test:test", l);

  /* Check the values */
  assert(c[0] == 'e');
  assert(c[1] == 'f');
  assert(c[2] == 'g');

  for (int k = 0; k < nval; k++) {
    int m = k + nval;
    assert(i[k] == m);
    assert(fabsf(f[k] - m * 0.1f) < 1e-6);
    assert(fabs(d[k] - m * 0.1) < 1e-12);
    if (k == 0) {
      assert(l[k] == 9876543210);
    } else {
      assert(l[k] == m);
    }
  }

  /* Optional array reader (success) */
  c[0] = 'e';
  c[1] = 'f';
  c[2] = 'g';

  i[0] = 3;
  i[1] = 4;
  i[2] = 5;

  f[0] = 0.3;
  f[1] = 0.4;
  f[2] = 0.5;

  d[0] = 0.3;
  d[1] = 0.4;
  d[2] = 0.5;

  l[0] = 9876543210;
  l[1] = 4;
  l[2] = 5;

  parser.GetOptParamArray<char, nval>("Test:char_array", c);
  parser.GetOptParamArray<int, nval>("Test:int_array", i);
  parser.GetOptParamArray<float, nval>("Test:float_array", f);
  parser.GetOptParamArray<double, nval>("Test:double_array", d);
  parser.GetOptParamArray<long long, nval>("Test:longlong_array", l);

  /* Check the values */
  assert(c[0] == 'a');
  assert(c[1] == 'b');
  assert(c[2] == 'c');

  for (int k = 0; k < nval; k++) {
    int m = k + 1;
    assert(i[k] == m);
    assert(fabsf(f[k] - m * 0.1f) < 1e-6);
    assert(fabs(d[k] - m * 0.1) < 1e-12);
    if (k == 0) {
      assert(l[k] == 1234567890);
    } else {
      assert(l[k] == m);
    }
  }
}
