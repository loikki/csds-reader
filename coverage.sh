#!/bin/bash

set -e

args=$@
if [ "$#" -eq 0 ]; then
    echo "Running the full script"
    args="-r -g -c"
fi

function run() {
    make check -j 10
}

function compile() {
    ./autogen.sh
    ./configure --enable-coverage --enable-debugging-checks
    make -j 10
}

function generate() {
    lcov -c --directory src/ --directory tests/ --output-file test
    genhtml test --output-directory coverage
}

function run_integration() {
    gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root src/
}

if [[ $args == *"-c"* ]]; then
    echo Compiling the code
    compile
fi

if [[ $args == *"-r"* ]]; then
    run
fi

if [[ $args == *"-i"* ]]; then
    run_integration
fi

if [[ $args == *"-g"* ]]; then
    generate
fi
