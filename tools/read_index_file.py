#!/usr/bin/env python3

import sys
import numpy as np
import mmap
import struct
import os


class IndexReader():
    n_type = 7
    swift_type_dark_matter = 1

    # dtype for the particle's data
    index_dtype = np.dtype([("ids", np.ulonglong),
                            ("offset", np.uint64)])

    def __init__(self, filename):
        self.filename = filename
        self.read_index()

    def read_index(self):
        filename = self.filename

        # Read the file
        with open(filename, "rb") as f:
            # read the time
            time = np.fromfile(f, dtype=float, count=1)
            time_int = np.fromfile(f, dtype=np.longlong, count=1)
            print("Time: {}, integer time: {}".format(
                time[0], time_int[0]))

            # read the number of particles
            nparts = np.fromfile(f, dtype=np.uint64, count=IndexReader.n_type)

            print("Number of particles:", nparts)

            # read if the file is sorted
            sort = np.fromfile(f, dtype=np.bool, count=1)
            print("File is sorted?", sort[0])

            # read the memory alignment garbage
            n = ((f.tell() + 7) & ~7) - f.tell()
            f.read(n)

            # read the particles
            print("Particles data (ids / offset):")
            data = []
            for n in nparts:
                if n == 0:
                    data.append(None)
                    continue

                tmp = np.fromfile(f, dtype=IndexReader.index_dtype, count=n)
                data.append(tmp)

            print("\t", data)

            # print the history of new particles
            n_new = np.fromfile(f, dtype=np.uint64, count=IndexReader.n_type)
            print("New particles: ", n_new)

            new = []
            for n in n_new:
                if n == 0:
                    new.append(None)
                    continue

                new.append(np.fromfile(
                    f, dtype=IndexReader.index_dtype, count=n))
            print("\t", new)

            # print the history of particles removed
            n_rem = np.fromfile(f, dtype=np.uint64, count=IndexReader.n_type)
            print("Particles removed: ", n_rem)

            removed = []
            for n in n_rem:
                if n == 0:
                    removed.append(None)
                    continue

                removed.append(np.fromfile(
                    f, dtype=IndexReader.index_dtype, count=n))
            print("\t", removed)
            return data, new, removed


if __name__ == "__main__":
    index = sys.argv[-1]

    # Open the logfile
    reader = IndexReader(index)
