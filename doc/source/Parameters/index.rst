.. Parameters
   Loic Hausammann 2021

Parameters File
===============

Some information about the simulation are stored within a parameter file.
The internal units are stored in this file.
Among all the parameters, an important block is for the cosmological parameters that helps to
do the interpolation in cosmological simulations.

The initial number of particles is used for the generation of index files.
The CSDS uses them as initial guess on the number of particles contained in the logfile.
Without it, the CSDS would have to allocate a small initial array and increase it multiple times until
reaching a size large enough to store all the particles.
This allows to quickly obtain an array large enough.


.. literalinclude:: example.yml
