.. Continuous Simulation Data Stream documentation master file, created by
   sphinx-quickstart on Fri May 28 11:18:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Continuous Simulation Data Stream's documentation!
=============================================================

The Continuous Simulation Data Stream (CSDS) is a new output system
that is especially designed for gravity based simulations.
In such simulations, gravity is producing large differences of timescale between different area.
An individual time step strategy has been designed a long time ago to speed up the simulations.
Currently, the snapshots consists in writing the complete state of the simulation
at regular interval.
This approach does not take into account the timescales and thus produce large outputs.
The CSDS has been designed to fully take into account the timescales to reduce the storage space.
Our approach consists in using a single file describing the whole evolution of the simulation.
In it, the particles/cells are written independently and according to their own time step.
To reconstruct the state of the simulation at a given time, the particles are then
simply interpolated from the nearest writing.
Thanks to our approach, we do not only reduce the storage space, but also ensure to
be able to recover the simulation at any time and not only a few discrete times.
As the particles/cells are written at their own time scale, this approach ensures to have
the same relative accuracy for all the particles.
For more details on the technique, please look at `Hausammann, Gonnet and Schaller (2021)`.
For simplicity, in the rest of the documentation, I will only mention the particles, but
everything can be applied to cells.

Our strategy for simulations requiring distributed memory (e.g. MPI) is
to simply consider one output file per rank and thus each one can be considered
independently from the others. It means that the arrays provided by the code
needs to be concatenated together in order to get the full state.

A few terms will be use within this documentation and need to be defined.
The outputs from the CSDS are composed of three different (set of) files.
The most important one is the logfile (``.dump``) and contains the whole simulation.
Then comes the parameter file (``.yml``) that contains some information about the simulation
(e.g. cosmological parameters, box size, ...).
The last files are the index files (``.index``).
They are generated by the reader and not during the simulation.
They contain part of the information inside the logfile and
are just a way to quickly access this large file.
Within the logfile, everything is written in the form of records.
Those small chunk of data can represent either a time step or a particle.


The CSDS is licensed under the LGPL version 3.0.

.. toctree::
   :maxdepth: 2

   
   QuickStart/index.rst
   Examples/index.rst
   Parameters/index.rst
   NewScheme/index.rst
   Doxygen/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
