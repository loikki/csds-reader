.. NewScheme
   Loic Hausammann 2021

.. _implementation:

Implementing new Scheme
=======================

To add the CSDS to a new scheme, a few files need to be created.
Let's use the Gadget2 module for the illustration.
First, the file :file:`src/hydro/Gadget2/hydro_csds.h` describes
how to write the particles inside the file.
The most important function is :command:`csds_hydro_define_fields` that defines all the fields.
The names should be the same than within the reader and correspond to the python API.
A conversion function is given for the acceleration :command:`csds_hydro_convert_acc`.
For the compilation, the files need to be included in :file:`src/hydro_csds.h`
and :file:`src/Makefile.am`.

The reader is independent from the modules in order to simplify its usage.
Therefore the new fields need to be added within :file:`csds/src/csds_fields.h`,
:file:`csds/src/csds_fields.c` and :file:`csds/src/csds_particles.c`.
The two first files define the properties of the fields (name, size, derivatives
and the python interface).
In the particle file, the interpolation is defined for each field.
