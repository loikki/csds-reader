.. Examples
   Loic Hausammann 2021

Examples
========

Three different examples are available in the repository.
The first one is described in :ref:`running`.
Therefore I will only described the two others.

Generating a Snapshot
---------------------

When running a deep analysis of a single given time, it might be faster
(or more convenient) to use a snapshot.
The file ``examples/create_snapshot.py`` can generate such files.
It takes three parameters: the time (``--time``), the output file (``--output``) and
the logfiles.
Then it simply loops over all the existing fields thanks to ``get_list_fields`` and write
them into an HDF5 file.


Simple Orbital Simulation
-------------------------

The example in ``examples/SimpleOrbits`` consists in a
single particle orbiting around a point mass (see ``README`` for more details).
The parameters for the snapshots and the CSDS are set in order to have a large
time resolution for the snapshots while a poor resolution for the CSDS.
Even in such unfavorable case, the CSDS is still able to manage an accurate evolution
of the kinetic energy (see figure below).
The places where the CSDS line touches the snapshot line correspond to
where the records are written.

.. figure:: Energy.png
    :width: 400px
    :align: center
    :figclass: align-center
    :alt: Kinetic energy as function of time

    This figure is produced by ``examples/SimpleOrbits`` and shows the evolution of
    kinetic energy. As the energy is conserved, an oscillation is present due to the
    conversion between kinetic and potential energy.
    As it can be seen, in this example, the CSDS at low time resolution is as accurate
    than the snapshots at high resolution.
