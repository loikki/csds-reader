.. CompilingCode
   Loic Hausammann 2021

.. _compiling_code:

How to Compile the Code
=======================

To configure the CSDS (with the python wrapper), you need to use the following commands:

.. code-block:: bash

   ./autogen.sh
   ./configure --with-python=$PYTHON_PATH

On a standard ubuntu, ``$PYTHON_PATH`` is given by ``/usr/``.
It is worth to mention that the CSDS will not work with python 2.7.x
and that the user should move to 3.0 as soon as possible.

Then the CSDS can be compiled:

.. code-block:: bash

   make -j 10

where 10 is the number of threads for the compilation.
