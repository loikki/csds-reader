.. QuickStart
   Loic Hausammann 2021

.. _quick_start:

QuickStart
==========

Here you will find how to compile the CSDS and run it on your first simulations.
This code was initially designed for SWIFT, but can be used with other codes
if the output files are correctly written.
SWIFT is the reference implementation for the CSDS writer.
Due to the large number of schemes, not all of them have a CSDS implementation.
Therefore you should try with the default ones first.
If you wish to implement a new one, please follow the implementation
within the other schems and :ref:`implementation`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   compiling_code
   running_first
   using_python
