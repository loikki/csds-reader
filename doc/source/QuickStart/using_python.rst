.. PythonAPI
   Loic Hausammann 2021

.. _python:

Python API
==========

The python API is relatively simple and fully self documented.
Here, I will explain the main features and how they work.
If you do not wish to install the CSDS as a python library,
you will need be manually import it:

.. code-block:: python

   import sys
   sys.path.append("CSDS_PATH/src/.libs/")
   import libcsds as csds

This module contains a single object (``Reader``) that fully deals with
the CSDS files.
To open a logfile, this object is call in a with statement:

.. code-block:: python

   with csds.Reader(filename, verbose=0, number_index=10,
                    restart_init=False, use_cache=False) as reader:
       # Use the Reader

In this example, all the parameters are set with their default values.
The Reader is able to make some operations in parallel and
will openmp to make them.
The index files are used to speedup the reading and are set at
regular interval of simulation time (e.g. every :math:`(t_e - t_b) / (N - 1)`
where :math:`t_b` (:math:`t_e`) is the initial (final) time and :math:`N` the number of index file.
To get the best performances, the best is to write an index file as close
as possible and below any future requested time.
As they might take some time to generate, it is possible to restart their generation
from an interrupted run with the parameter ``restart_init``.

Now the Reader can provide some information about the simulation:

.. code-block:: python

   reader.get_list_fields(part_type=None)

The parameter ``part_type`` can be used to get the fields of a given particle type.
It could be an int (e.g. 0 for the gas) or an array of int (e.g. [0, 1] for the gas and dark matter).
In such cases, the fields will be restricted to the common ones.
We provide some enums to simplify the usage of particle types in ``csds.particle_types``:
``gas``, ``dark_matter``, ``dark_matter_background``, ``sinks``, ``stars``, ``black_holes``,  ``neutrino``,
``count``.

.. code-block:: python

   t0, t1 = reader.get_time_limits()
   t0 = reader.get_time_begin()
   t1 = reader.get_time_end()

Theses functions return the minimal and maximal time contained within the simulation.
Any requested time should stay within the two limits (boundary included).

.. code-block:: python

   reader.get_box_size()

This last method returns the box size of the simulation.

Let's now focus on getting the particles. This is done by a single method called ``get_data``.
Depending on the parameters, this function will have different optimizations:

.. code-block:: python

   reader.get_data(fields, time=0, filter_by_ids=None, part_type=None)

It is mandatory to provide the required fields. If you wish to get all
of them, simply provide the output of ``reader.get_list_fields``.
```get_data`` returns a dictionary with the keys given by ``fields`` and
the values are numpy arrays.
The arrays are sorted according to the particle types.
For the units, all the arrays are in internal units
(see SWIFT for more details and be especially careful with cosmological simulations).
Obviously, the parameter ``time`` is the requested time (in internal units)
that should be within the limits provided by ``get_time_limits``.
``filter_by_ids`` allows to pick only a subset of the particles.
This parameter takes a list containing an array for each type of particle (or None).
The list should have the same size than the number of particle types in the CSDS.
The list of IDs will be modified by the CSDS in order to represent only the particles present
within the simulation.
The last parameter allows to read only some type of particles (e.g. ``part_type=0`` or
``part_type=[0, 1]``).
The two last parameters cannot be used together.

When reading only a subset of the particles (``filter_by_ids``), the particles are searched
within the index file and then read in the logfile.
Due to the search, this parameter is especially efficient for a number
of particles largely below the total number.
It is worth to explicitly mention that the function will be inefficient
if almost all the particles are requested.
In such cases, it is better to request all the particles and then to drop the unwanted ones.

When requesting all the particles (of a single type or many types),
the code starts with the first particle in the index file and
then read them the one after the onter.
If some particles are removed from the simulation, the code stops with the current one
and goes to the next one.
While it is possible to find the removed particles with only the index file,
it would take too much time to do it.
The only case where anticipating the removal of particles would provide better results is when
most of the particles are removed.

Once the particles read, it is possible to quickly update them with the following function:

.. code-block:: python

   out = reader.update_particles(fields=fields, time=1)

This function will retrieve the particles from the cache in order to interpolate them at the new time.
In the current implementation, this is not compatible with ``filter_by_ids`` and will not work if the time interval is too large (e.g. larger than the time difference between index files).
