.. RunningFirst
   Loic Hausammann 2021

.. _running:


Running Your First Example
==========================

The best approach to test the CSDS is to use an example within SWIFT.
Let's start with the compilation of SWIFT and running our first example.


.. code-block:: bash

   ./autogen.sh
   ./configure --enable-csds --with-python=$PYTHON_PATH
   make -j 10
   cd examples/HydroTests/SedovBlast_3D
   ./run.sh

The file ``run.sh`` is not running with the CSDS by default and you will need to add
the runtime parameter ``--csds`` to the command calling SWIFT
(e.g. ``../../swift --csds --hydro --limiter --threads=4 sedov.yml``).
If everything is fine, the last message from SWIFT should be ``Done``
and two files have appeared (``index_0000.dump`` and ``index.yml``)
Then the reader example can be used to interpret the output:

.. code-block:: bash

   cd ../../../csds/examples
   python3 reader_example.py

This example should give you the internal energy as function of the radius:

.. figure:: example.png
    :width: 400px
    :align: center
    :figclass: align-center
    :alt: Internal energy as function of the radius for the Sedov Blast

    This figure shows the internal energy as function of the radius for the
    Sedov Blast. At such early time, all the energy is concentrated at the
    center.

This example can be used to see the evolution of the blast thanks to the parameter
``--time`` and will read any logfile provided (by default the Sedov Blast example).
This example is adapted only for hydrodynamics simulations and will raise an error
on simulations without hydrodynamics.
