[![pipeline status](https://gitlab.cosma.dur.ac.uk/lhausammann/csds-reader/badges/master/pipeline.svg)](https://gitlab.cosma.dur.ac.uk/lhausammann/csds-reader/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/continuous-simulation-data-stream/badge/?version=latest)](https://continuous-simulation-data-stream.readthedocs.io/en/latest/?badge=latest)

# CSDS Reader

The Continuous Simulation Data Stream is a new output system for SWIFT designed to replace the snapshots in simulation using gravity.
This is a submodule of SWIFT containing only the reader.

For details on the coverage, please see https://gitlab.cosma.dur.ac.uk/jenkins/job/CSDS-Pipeline/.

To compile the code, run `./autogen.sh` and then `./configure; make -j 10`.
To ensure its quality, the code is compiled by default with `-Wall`.
If you need to disable it, configure the code with `./configure --enable-compiler-warnings=yes`.


# File Description

This description corresponds to the CSDS version 1.4.
If this file is not up to date, please look at the following file ``src/csds_header.c``
and the function ``header_read``.
Except from the header, the file consists in a long list of records (see next section).
In order to differentiate the records, a mask is present in the record's header.
They are defined within the logfile's header and their value is given by `1 << i`
where i is the index according to the order given in the header.
The two first masks are always reserved, in the order,
for the special flags (e.g. deletion / creation of particles)
and the timestamp (time information).
The special flag has a size of 4 bytes.
The first contains the special flag, the next two some related data and finally
the particle type.
The timestamp consists in a `long long` (`integertime_t` in SWIFT)
and a `double` (time or scale factor).

A few variables needs to be defined first:
 - `STRING_SIZE` set to 200. This variable is used for all the strings within the library and the file format name.
 - `CSDS_OFFSET_SIZE` set to 6. This variable provides the number of bytes to use for any offset within the file.
 - `CSDS_MASK_SIZE` set to 2. This variables provides the number of bytes to use for the masks in the records.
 - `swift_type_count` set to 7. This variable contains the number of particle types available in SWIFT.
 

The structure of the file is the following one:
 - The file format: "SWIFT_CSDS" (string of `STRING_SIZE` `char`)
 - Major version number (`int`)
 - Minor version number (`int`)
 - Offset direction (`int`). This should be backward (`0`) after a simulation.
 - Offset to the first record (`unsigned int` with a size of `CSDS_OFFSET_SIZE` bytes). This value provides directly the position of the first record from the beginning of the file.
 - The length of the strings in the logfile (`unsigned int`): `string_length`
 - The number of masks available (`unsigned int`).
 - For each mask:
   - The name of the mask (string of `string_length` `char`)
   - Size of the mask in bytes (`unsigned int`)
 - Read the number of fields per particle type (`swift_type_count` `int`): `number_fields`.
 - For each particle type:
   - The order of the fields (`number_fields` `int`). The values correspond to the index within the mask read previously.
 - A list of masks

# Record Description

Each record has the following structure:
 - The mask (`CSDS_MASK_SIZE` bytes). This describes the data contained in the tail of the record.
 - The offset (`CSDS_OFFSET_SIZE` bytes). The distance in the file to the next (previous) corresponding record (depending on the offset direction in the logfile header).
 - The data (variable size). Each field is given according to the particle type, the order of the fields and the mask in the logfile's header.
