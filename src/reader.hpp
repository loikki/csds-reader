/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @file csds_reader.hpp
 * @brief This file contains the C functions shown to the external user.
 *
 * Here is a quick summary of our different elements:
 *
 * The csds is a time adaptive way to write snapshots.
 * It consists of a set of files: the log file, the parameter file and the index
 * files.
 *
 * The <b>parameter file</b> contains all the information related to the code
 * (e.g. boxsize, scheme, ...).
 *
 * The <b>index files</b> are not mandatory files that indicates the position of
 * the particles in the log file at a given time step. They are useful to
 * speedup the reading.
 *
 * The <b>log file</b> consists in a large file where the particles are logged
 * one after the other. It contains a <b>log file header</b> at the beginning of
 * the file and a large collection of <b>records</b>.
 *
 * The records are logged one after the other and each contains a <b>record
 * header</b> and then a list of <b>named entries</b>. In the record header, a
 * <b>mask</b> is provided that corresponds to the type of named entries present
 * in this record. It also contains the <b>offset</b> to the previous or next
 * record for this particle.
 */

#ifndef CSDS_READER_H
#define CSDS_READER_H

/* Standard header */
#include <string>
#include <unordered_map>
#include <vector>

/* Local headers */
#include "cache.hpp"
#include "csds_array.hpp"
#include "index_file.hpp"
#include "logfile.hpp"
#include "mapped_file.hpp"
#include "parameters.hpp"
#include "particle.hpp"

typedef std::array<std::unordered_map<id_type, uint64_t>, csds_type_count>
    CsdsUnorderedMap;
class IndexWriter;

/**
 * @brief Main structure of the csds.
 *
 * This structure contains all the variables required for the csds.
 * It should be the only structure that the user see.
 */
class Reader {

 public:
  /**
   * @brief Initialize the reader.
   *
   * @param basename The basename of the csds files.
   * @param verbose The verbose level.
   * @param number_index The number of index files to write.
   * @param restart_init Are we restarting the initial reading?
   * @param use_cache Are we using the cache?
   */
  Reader(std::string basename, int verbose, int number_index, bool restart_init,
         bool use_cache);

  /** @brief Free the reader. */
  ~Reader();

  /* Functions related to the time */
  /**
   * @brief Set the reader to a given time and read the correct index file.
   * @param time The requested time.
   */
  void SetTime(double time);
  /** @brief Get the simulation initial time. */
  double GetTimeBegin();

  /** @brief Get the simulation final time. */
  double GetTimeEnd();
  /**
   * @brief Provides the number of particle (per type) from the index file.
   * @param n_parts (out) Number of particles at the time set in the reader.
   * @param read_types Should we read this type of particles?
   */
  void GetNumberParticles(std::array<uint64_t, csds_type_count> &n_parts,
                          const std::array<bool, csds_type_count> &read_types);

  /* Read particles from the logfile */
  /**
   * @brief Read all the particles from the index file.
   * @param time The requested time for the particle.
   * @param output The output array
   * @param n_part Number of particles of each type.
   */
  void ReadAllParticles(double time, std::vector<CsdsArray> &output,
                        const std::array<uint64_t, csds_type_count> &n_part);
  /**
   * @brief Update the particles from the index file.
   *
   * @param time The requested time for the particle.
   * @param output Pointer to the output array.
   * @param n_part Number of particles of each type.
   */
  void UpdateParticles(double time, std::vector<CsdsArray> &output,
                       const std::array<uint64_t, csds_type_count> &n_part);
  /**
   * @brief Read the particles from the index file and their ids.
   *
   * @param time The requested time for the particle.
   * @param output The output array.
   * @param n_part Number of particles of each type.
   * Updated with the number of particles found.
   * @param ids The particles' ids.
   * The ids not found are removed from this array.
   */
  void ReadParticlesFromIds(double time, std::vector<CsdsArray> &output,
                            std::array<uint64_t, csds_type_count> &n_part,
                            std::array<long long *, csds_type_count> &ids);

  /* Getters and setters */
  Header const &GetHeader() const { return mLog->GetHeader(); }
  int GetVerbose() const { return mVerbose; }
  void SetVerbose(int verbose) { mVerbose = verbose; }
  std::array<Cache, csds_type_count> const &GetCache() const { return mCache; }

#ifdef CSDS_TEST
  /* Getter for the tests */
  LogFile &GetLogFile() const { return *mLog; }
#endif

 protected:
  /**
   * @brief Get the offset of the last timestamp before a given time.
   * @param time The requested time.
   * @return The offset of the timestamp.
   */
  size_t GetNextOffsetFromTime(double time);
  /** @brief Provide the name of an index file according to its number */
  std::string GetIndexName(int count);

  /* Read the particles */

  /**
   * @brief Read a single particle.
   *
   * @param time The time to interpolate
   * @param output The pointers to the output arrays
   * @param offset_from_index The offset of the particle to read
   * @param output_index The index of the particle within the output arrays
   * @param type The type of particle
   * @parma number_jumps (out) The number of jumps required to get the particle
   */
  int ReadParticle(double time, std::vector<CsdsArray> &output,
                   size_t offset_from_index, size_t output_index,
                   enum part_type type, int &number_jumps);

  /**
   * @brief Read all the particles of a given type from the index file.
   *
   * @param time The requested time for the particle.
   * @param output The output array.
   * @param n_part Number of particles of each type.
   * @param type The particle type
   */
  void ReadAllParticlesSingleType(
      double time, std::vector<CsdsArray> &output,
      const std::array<uint64_t, csds_type_count> &n_part, enum part_type type);
  /**
   * @brief Read the particles of a given type from the index file and their
   * ids.
   *
   * @param time The requested time for the particle.
   * @param output The output array.
   * @param n_part Number of particles of each type.
   * Updated with the number of particles found.
   * @param type The particle type
   * @param ids The particles' ids.
   * The ids not found are removed from this array.
   */
  void ReadParticlesFromIdsSingleType(
      double time, std::vector<CsdsArray> &output,
      std::array<uint64_t, csds_type_count> &n_part, enum part_type type,
      long long *ids);

  /* Update particles */
  /**
   * @brief Update the particles of a given type from the cache.
   *
   * @param time The requested time for the particle.
   * @param output The output array.
   * @param n_part Number of particles of each type.
   * @param type The particle type
   */
  void UpdateParticlesSingleType(
      double time, std::vector<CsdsArray> &output,
      const std::array<uint64_t, csds_type_count> &n_part, enum part_type type);
  /**
   * @brief Update a particle in the cache
   *
   * @param cache The #csds_cache.
   * @param index The index of the particle.
   * @param type The type of particle.
   * @param time The time for the interpolation
   * @param offset_time The offset of the interpolation time
   * @param field_index The index of the field in the cache
   * @param output The buffer to write the output
   *
   * @return if the particle was removed
   */
  bool UpdateSingleParticle(const size_t &index, const enum part_type &type,
                            const double &time, const size_t &offset_time,
                            const size_t &field_index, void *output);

  /* Generation of index files */
  /**
   * @brief Initialize the indexes.
   *
   * @param number_index Number of requested index files.
   * @param current_index Current index to write (for restarting the writing)
   */
  void InitIndex(int number_index, int current_index);
  /**
   * @brief Generate the index files from the logfile.
   * @param number_index The number of index to generate.
   * @param current_index Last index written (> 0 if restart)
   */
  void GenerateIndexFiles(int number_index, int current_index);
  /**
   * @brief Get the initial state of the simulation for the generation of
   * index file.
   *
   * @param current_state The arrays that will contain the initial state.
   * @param time_record (output) The first #time_record in the logfile (for
   * writing the index file).
   *
   * @return The offset of the second #time_record
   * (the first record that does not correspond to the IC).
   */
  size_t GetInitialState(CsdsUnorderedMap &current_state,
                         struct time_record &time_record);
  /**
   * @brief Write an index file.
   *
   * @param current_state The arrays containing the current
   * state with the last full offset.
   * @param parts_created The arrays containing the first reference
   * (since last index file) of created particles.
   * @param parts_removed The arrays containing the last reference
   * (since last index file) of removed particles.
   * @param time The time corresponding to the current index file.
   * @param file_number The current file number.
   */
  void WriteIndex(CsdsUnorderedMap &current_state, IndexWriter &parts_created,
                  IndexWriter &parts_removed, const struct time_record &time,
                  int file_number);
  /**
   * @brief Get the last offset of a record before a given offset.
   *
   * @param data The #index_data of the particle.
   * @param offset_limit The upper limit of the offset to get.
   *
   * @return The last offset before offset_limit.
   */
  size_t GetLastOffsetBefore(const struct index_data &data,
                             size_t offset_limit);
  /**
   * @brief Update the state of the simulation until the next index file along
   * with the history.
   *
   * @param init_offset The initial offset to read.
   * @param time_record The #time_record of the next index file.
   * @param current_state The arrays containing the state of the simulation.
   * @param parts_created The arrays containing the particles created since last
   * index.
   * @param parts_removed The arrays containing the particles removed since last
   * index.
   *
   * @return The starting offset for the next update.
   */
  size_t UpdateStateToNextIndex(size_t init_offset,
                                struct time_record time_record,
                                CsdsUnorderedMap &current_state,
                                IndexWriter &parts_created,
                                IndexWriter &parts_removed);

  /* Compute number of particles */
  /**
   * @brief Count the number of new particles at the time set since last index
   * file.
   *
   * @param part_type The index corresponding to the particle type.
   *
   * @param The number of new particles.
   */
  size_t CountNumberNewParticles(enum part_type part_type);
  /**
   * @brief Count the number of removed particles at the time set since last
   * index file.
   * @param part_type The index corresponding to the particle type.
   * @param The number of removed particles.
   */
  size_t CountNumberRemovedParticles(enum part_type part_type);

  /* Base name of the files */
  std::string mBasename;

  struct {
    /* Information contained in the previous index file. */
    IndexFile *index_prev;

    /* Information contained in the next index file. */
    IndexFile *index_next;

    /* Number of index files */
    int n_files;

    /* Time of each index file */
    std::vector<double> times;
  } mIndex;

  /* Informations contained in the file header. */
  LogFile *mLog;

  /* Information about the current time */
  struct {
    /* Double time */
    double time;

    /* Integer time */
    integertime_t int_time;

    /* Offset of the chunk */
    size_t time_offset;
  } mTime;

  /* Information from the yaml file */
  Parameters mParams;

  /* Level of verbosity. */
  int mVerbose;

  /* Are we using the cache? */
  bool mUseCache;

  /* The cache for the evolution of particles. */
  std::array<Cache, csds_type_count> mCache;
};

#endif  // CSDS_READER_H
