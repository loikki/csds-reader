/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @file mapped_file.hpp
 * @brief This file contains basic IO function.
 */
#ifndef CSDS_LOADER_IO_H
#define CSDS_LOADER_IO_H

#include "field.hpp"
#include "header.hpp"
#include "mapped_tracking_element.hpp"
#include "tools.hpp"

/* Standard headers */
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#define CSDS_VERBOSE_TIMERS -1

size_t csds_loader_io_get_file_size(int fd);
INLINE static bool csds_file_exist(const std::string &filename) {
  /* Check if the file exists. */
  std::ifstream file(filename);
  bool ret = file.good();
  file.close();
  return ret;
}

/* Structure for mapping a file. */
class MappedFile {

 public:
  /**
   * @brief Map the file.
   * @param filename The filename.
   * @param read_only Open the file in read only mode?
   * @param track_mmap Should we track the map access?
   * In order to track the accesses, the code should be compiled with
   * --enable-mmap-tracking.
   */
  MappedFile(const std::string filename, bool read_only, bool track_mmap) {
    Open(filename, read_only, track_mmap);
  };
  ~MappedFile() {
    if (mMap != NULL) Close();
  };

  /** @brief Close the file */
  void Close();
  /** @brief Open the file
   * @param filename The filename.
   * @param read_only Open the file in read only mode?
   * @param track_mmap Should we track the map access?
   * In order to track the accesses, the code should be compiled with
   * --enable-mmap-tracking.
   */
  void Open(const std::string filename, bool read_only, bool track_mmap);

  /**
   * @brief read a single value from a file.
   *
   * @param offset The offset in the file
   * @param size size of the data to read.
   * @param p pointer where to store the data.
   *
   * @return The offset after the data read.
   */
  INLINE size_t ReadData(size_t offset, const size_t size, void *p);

  /**
   * @brief write a single value in a file.
   *
   * @param offset The offset in the file
   * @param size size of the data to write.
   * @param p pointer to the data.
   *
   * @return memory after the data written.
   */
  size_t WriteData(size_t offset, const size_t size, const void *p);

  /** @brief Warn the OS that the file will be read in a sequential way */
  void AdviceSequentialReading() {
    /* Warn the OS that we will read in a sequential way */
    int test_ret = madvise(mMap, mMapSize, MADV_SEQUENTIAL);
    if (test_ret != 0) {
      csds_error("Failed to advise the mmap");
    }
  }

  /** @brief Warn the OS that the file will be read in a "normal" way */
  void AdviceNormalReading() {
    /* Warn the OS that we will read in a normal way */
    int test_ret = madvise(mMap, mMapSize, MADV_NORMAL);
    if (test_ret != 0) {
      csds_error("Failed to advise the mmap");
    }
  }

  /** @brief Warn the OS that we will need a given page.
   * @param addr The pointer to the address (not rounded to the page)
   * @param length The length of the area
   */
  void AdviceWillNeed(size_t addr, size_t length);

  size_t GetFileSize() const { return mMapSize; }

 protected:
  /* Mapped data. */
  char *mMap;
  /* File size. */
  size_t mMapSize;

#ifdef CSDS_MMAP_TRACKING
  /** @brief Write a tracking element into the file */
  void WriteTracking(const MMapTrackingElement &el);

  struct {
    /* File for the tracking (should not use mmap file) */
    std::ofstream stream;

    /* Size of the file? */
    size_t size;

    /* Current number of elements */
    int64_t number_elements;

    /* Are we tracking the reading? */
    bool use_tracking;
  } mTracking;
#endif
};

inline size_t MappedFile::ReadData(size_t offset, const size_t size, void *p) {
#ifdef CSDS_MMAP_TRACKING
  MMapTrackingElement el(offset);
#endif

  memcpy(p, this->mMap + offset, size);

#ifdef CSDS_MMAP_TRACKING
  /* Write the result into the file */
  if (this->mTracking.use_tracking) {
    this->WriteTracking(el);
  }
#endif

  return offset + size;
};

inline size_t MappedFile::WriteData(size_t offset, const size_t size,
                                    const void *p) {

#ifdef CSDS_MMAP_TRACKING
  MMapTrackingElement el(offset);
#endif

  memcpy(this->mMap + offset, p, size);

#ifdef CSDS_MMAP_TRACKING
  /* Write the result into the file */
  if (this->mTracking.use_tracking) {
    this->WriteTracking(el);
  }
#endif

  return offset + size;
};

inline void MappedFile::AdviceWillNeed(size_t addr, size_t length) {
  /* Adjust the size to be at least the page size. */
  const size_t page_size = sysconf(_SC_PAGE_SIZE);
  const size_t mem_align = (addr + (page_size - 1)) & ~(page_size - 1);

  /* Warn the OS that we will read soon this address  */
  int test_ret = madvise(mMap + mem_align, length + page_size, MADV_WILLNEED);
  if (test_ret != 0) {
    csds_error("Failed to advise the mmap");
  }
}

#ifdef CSDS_MMAP_TRACKING
inline void MappedFile::WriteTracking(const MMapTrackingElement &el) {
#pragma omp critical
  {
    /* Stop when file becomes too huge */
    if (this->mTracking.size < CSDS_TRACKING_SIZE_BYTES) {

      /* Write the element */
      this->mTracking.stream << el;

      /* Update the counter */
      this->mTracking.number_elements++;

      /* Write it back to the file */
      const std::streampos pos = this->mTracking.stream.tellp();

      /* See MappedFile constructor for the value */
      this->mTracking.stream.seekp(20);
      this->mTracking.stream << this->mTracking.number_elements;
      this->mTracking.stream.seekp(pos);

      /* Update the file size */
      this->mTracking.size += sizeof(MMapTrackingElement);
    }
  }
}
#endif

#endif  // CSDS_LOADER_IO_H
