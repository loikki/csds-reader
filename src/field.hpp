/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions that deals with the fields
 */
#ifndef CSDS_FIELD_H
#define CSDS_FIELD_H

/* Standard libraries */
#include <vector>

/* Configuration options */
#include "config.hpp"

/* Standard headers */
#ifdef HAVE_PYTHON
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

#define PYTHON_SET_FIELD(name, element_size, format) \
  {                                                  \
    names.append(name);                              \
    formats.append(format);                          \
    offsets.append(current_offset);                  \
    current_offset += element_size;                  \
  }
#endif  // HAVE_PYTHON

/* Include CSDS headers */
#include "single_field.hpp"

/**
 * @brief Class representing a complete field
 * including its derivatives.
 */
class Field {
 public:
  /**
   * @brief Construct the field.
   * @param name The name of the field.
   * @param all_data The masks extracted from the logfile.
   */
  Field(std::string name, const std::vector<struct mask_data>& all_data);

  /* Getters and Setters */
  /* Field and derivatives sizes */
  INLINE int GetFieldSize() const { return mField.GetSize(); }
  INLINE int GetFirstDerivativeSize() const {
    return mFirstDerivative.GetSize();
  }
  INLINE int GetSecondDerivativeSize() const {
    return mSecondDerivative.GetSize();
  }

  /* Field objects for the field and its derivatives */
  INLINE SingleField const& GetField() const { return mField; }
  INLINE SingleField const& GetFirstDerivative() const {
    return mFirstDerivative;
  }
  INLINE SingleField const& GetSecondDerivative() const {
    return mSecondDerivative;
  }

  /* Check if the derivatives exist */
  INLINE bool HasFirstDerivative() const { return mHasFirstDerivative; }
  INLINE bool HasSecondDerivative() const { return mHasSecondDerivative; }

  /* Getters for the field (not the derivatives) */
  INLINE mask_type GetMask() const { return mField.GetMask(); }
  INLINE const std::string GetName() const { return mField.GetName(); }

  /** @brief Makes the derivatives inexistant */
  INLINE void DisableFirstDerivative() { mHasFirstDerivative = false; }
  INLINE void DisableSecondDerivative() { mHasSecondDerivative = false; }

#ifdef HAVE_PYTHON
  /**
   * @brief Create a dtype object for the field.
   */
  INLINE boost::python::numpy::dtype GetPythonDtype();
#endif  // HAVE_PYTHON

 protected:
  SingleField mField;
  SingleField mFirstDerivative;
  SingleField mSecondDerivative;
  bool mHasFirstDerivative;
  bool mHasSecondDerivative;
};

/* Now defines the inlined functions */

inline Field::Field(std::string name,
                    const std::vector<struct mask_data>& all_data)
    : mField(name), mHasFirstDerivative(false), mHasSecondDerivative(false) {

  /* Set the derivatives */
  const std::string first = SingleField::sNames[mField.GetFirstDerivative()];
  mFirstDerivative = SingleField(first, /* allow_none */ true);
  const std::string second = SingleField::sNames[mField.GetSecondDerivative()];
  mSecondDerivative = SingleField(second, /* allow_none */ true);

  /* Check the input */
  bool found = false;
  for (size_t i = 0; i < all_data.size(); i++) {
    /* Do the field */
    if (strcmp(name.c_str(), all_data[i].name) == 0) {
      found = true;
      mField.CheckSize(all_data[i].size);
      mField.SetMask(all_data[i].mask);
    }
    /* Do the first derivative */
    if (mFirstDerivative.GetFieldEnum() != field_enum_none &&
        strcmp(mFirstDerivative.GetName().c_str(), all_data[i].name) == 0) {
      mHasFirstDerivative = true;
      mFirstDerivative.CheckSize(all_data[i].size);
      mFirstDerivative.SetMask(all_data[i].mask);
    }
    /* Do the second derivative */
    if (mSecondDerivative.GetFieldEnum() != field_enum_none &&
        strcmp(mSecondDerivative.GetName().c_str(), all_data[i].name) == 0) {
      mHasSecondDerivative = true;
      mSecondDerivative.CheckSize(all_data[i].size);
      mSecondDerivative.SetMask(all_data[i].mask);
    }
  }

  /* Check if we found the field */
  if (!found) {
    csds_error("The field " << name << " was not found");
  }
}

#ifdef HAVE_PYTHON
/**
 * @brief Create a dtype object for the field.
 */
inline boost::python::numpy::dtype Field::GetPythonDtype() {
  namespace bp = boost::python;
  /* Initialize */
  bp::list formats;
  bp::list names;
  bp::list offsets;
  int current_offset = 0;

  switch (mField.GetFieldEnum()) {
    case field_enum_coordinates:
      formats.append("3f8");
      break;

    case field_enum_velocities:
    case field_enum_accelerations:
      formats.append("3f4");
      break;

    case field_enum_masses:
    case field_enum_smoothing_lengths:
    case field_enum_internal_energies:
    case field_enum_entropies:
    case field_enum_birth_time:
    case field_enum_densities:
      formats.append("f4");
      break;

    case field_enum_particles_ids:
      formats.append("q");
      break;

    case field_enum_sphenix_secondary_fields: {
      PYTHON_SET_FIELD("Entropies", sizeof(float), "f4");
      PYTHON_SET_FIELD("Pressure", sizeof(float), "f4");
      PYTHON_SET_FIELD("ViscosityParameters", sizeof(float), "f4");
      PYTHON_SET_FIELD("DiffusionParameters", sizeof(float), "f4");
      PYTHON_SET_FIELD("LaplacianInternalEnergies", sizeof(float), "f4");
      PYTHON_SET_FIELD("VelocityDivergences", sizeof(float), "f4");
      PYTHON_SET_FIELD("VelocityDivergenceTimeDifferentials", sizeof(float),
                       "f4");
      break;
    }

    case field_enum_gear_star_formation: {
      PYTHON_SET_FIELD("BirthDensities", sizeof(float), "f4");
      PYTHON_SET_FIELD("BirthMasses", sizeof(float), "f4");
      PYTHON_SET_FIELD("ProgenitorIDs", sizeof(long long), "q");
      break;
    }

    case field_enum_gear_chemistry_part: {
      PYTHON_SET_FIELD("SmoothedMetalMassFractions",
                       GEAR_CHEMISTRY_ELEMENT_COUNT * sizeof(double),
                       std::to_string(GEAR_CHEMISTRY_ELEMENT_COUNT) + "f8");
      PYTHON_SET_FIELD("MetalMassFractions",
                       GEAR_CHEMISTRY_ELEMENT_COUNT * sizeof(double),
                       std::to_string(GEAR_CHEMISTRY_ELEMENT_COUNT) + "f8");
      break;
    }

    case field_enum_gear_chemistry_spart:
      formats.append(std::to_string(GEAR_CHEMISTRY_ELEMENT_COUNT) + "f8");
      break;

    default:
      csds_error("The field " << SingleField::sNames[mField.GetFieldEnum()]
                              << " is not implemented");
  }

  /* Creates the dtype */
  if (len(formats) == 1) {
    /* Here we store only a single field */
    return bp::numpy::dtype(formats[0]);
  } else {
    /* Here we store multiple fields*/
    bp::dict out;
    if (len(names) != 0) {
      out["names"] = names;
    }
    if (len(offsets) != 0) {
      out["offsets"] = offsets;
    }
    out["formats"] = formats;
    return bp::numpy::dtype(out);
  }
}
#endif  // HAVE_PYTHON

#endif  // CSDS_FIELD_H
