/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#ifndef CSDS_ARRAY_HPP
#define CSDS_ARRAY_HPP

/* Include configuration */
#include "config.hpp"

#ifdef HAVE_PYTHON
#undef error
#include <boost/python/numpy.hpp>
namespace bp = boost::python;
#endif

/* Local headers */
#include "error.hpp"
#include "field.hpp"

/**
 * @brief An abstract array for the output data.
 *
 * This array ca
 */
class CsdsArray {

 public:
  CsdsArray(std::size_t size, Field field,
            bool include_first_derivative = false,
            bool include_second_derivative = false);

  /* The arrays are expecting to be large => no copy */
  CsdsArray(CsdsArray const &) = delete;
  CsdsArray(CsdsArray &&other)
      : mpData(other.mpData),
        mElementSize(other.mElementSize),
        mStolen(other.mStolen),
        mSize(other.mSize),
        mField(other.mField) {
    // prevent other from cleaning buffer if it destroys itself
    other.mStolen = true;
  };
  void operator=(CsdsArray const &) = delete;

  ~CsdsArray() {
    if (!mStolen) {
      delete[] mpData;
    }
  };

  /** @brief Save a particle into the array.
   * @param index The index of the particle within the array.
   * @param field The #csds_reader_field containing the field of the particle.
   */
  INLINE void Save(size_t index, const struct csds_reader_field &field);

  /**
   * @brief Create a #csds_reader_field structure
   * containing the particle.
   * @param index The index of the particle
   */
  INLINE struct csds_reader_field GetBuffers(size_t index);

  /**
   * @brief Access an element
   */
  INLINE char *operator[](std::size_t i) {
#ifdef CSDS_DEBUG_CHECKS
    if (i >= this->mSize) {
      csds_error(
          "Trying to access an element larger"
          " than the maximal size: "
          << i << " " << this->mSize);
    }
#endif
    return mpData + i * mElementSize;
  }

#ifdef HAVE_PYTHON
  /** @brief Generate a numpy array around the buffer.
   * @param stealing_buffer Does the numpy array steal the buffer?
   * It means that the numpy array will take care of freeing the buffer.
   * @return The numpy array
   */
  bp::numpy::ndarray ToNumpy(bool stealing_buffer) {
    mStolen = stealing_buffer;
    bp::tuple shape = bp::make_tuple(mSize);
    bp::tuple stride = bp::make_tuple(mElementSize);
    bp::numpy::dtype dt = mField.GetPythonDtype();
    bp::numpy::ndarray out =
        bp::numpy::from_data(mpData, dt, shape, stride, bp::object());
    return out;
  };
#endif

  /** @brief Provides the array size */
  INLINE size_t Size() const { return mSize; }
  INLINE int ElementSize() const { return mElementSize; }
  INLINE Field const &GetField() const { return mField; }

 protected:
  /*! The array */
  char *mpData;
  /*! The size of each elements  */
  int mElementSize;
  /*! Were the data stolen (e.g. returned to numpy) */
  bool mStolen;
  /*! The size of the array */
  std::size_t mSize;
  /*! The field contained within the array */
  Field mField;
};

inline CsdsArray::CsdsArray(std::size_t size, Field field,
                            bool include_first_derivative,
                            bool include_second_derivative)
    : mStolen(false), mSize(size), mField(field) {
  mElementSize = mField.GetFieldSize();

  /* Deal with the first derivative */
  if (include_first_derivative && mField.HasFirstDerivative()) {
    mElementSize += mField.GetFirstDerivativeSize();
  } else {
    mField.DisableFirstDerivative();
  }

  /* Deal with the second derivative */
  if (include_second_derivative && mField.HasSecondDerivative()) {
    mElementSize += mField.GetSecondDerivativeSize();
  } else {
    mField.DisableSecondDerivative();
  }
  mpData = new char[size * mElementSize];
}

inline void CsdsArray::Save(size_t index,
                            const struct csds_reader_field &field) {
  char *buffer = (*this)[index];
  memcpy(buffer, field.field, mField.GetFieldSize());
  buffer += mField.GetFieldSize();

  if (mField.HasFirstDerivative()) {
    memcpy(buffer, field.first_deriv, mField.GetFirstDerivativeSize());
    buffer += mField.GetFirstDerivativeSize();
  }
  if (mField.HasSecondDerivative()) {
    memcpy(buffer, field.first_deriv, mField.GetSecondDerivativeSize());
    buffer += mField.GetSecondDerivativeSize();
  }
}

inline struct csds_reader_field CsdsArray::GetBuffers(size_t index) {
  struct csds_reader_field out;

  char *buffer = (*this)[index];
  out.field = buffer;
  buffer += mField.GetFieldSize();

  if (mField.HasFirstDerivative()) {
    out.first_deriv = buffer;
    buffer += mField.GetFirstDerivativeSize();
  } else {
    out.first_deriv = NULL;
  }

  if (mField.HasSecondDerivative()) {
    out.second_deriv = buffer;
    buffer += mField.GetSecondDerivativeSize();
  } else {
    out.second_deriv = NULL;
  }

  return out;
}
#endif
