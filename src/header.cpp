/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/* Standard library */
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* This field header */
#include "header.hpp"

/* Local headers */
#include "logfile.hpp"
#include "mapped_file.hpp"
#include "reader.hpp"
#include "tools.hpp"

const std::string Header::sOffsetName[] = {
    "Backward",
    "Forward",
    "Corrupted",
};

void Header::Print() const {
#ifdef CSDS_DEBUG_CHECKS
  message("Debug checks enabled.");
#endif
  message("First Offset: " << mOffsetFirstRecord);
  message("Offset direction: " << sOffsetName[mOffsetDirection]);

  for (int type = 0; type < csds_type_count; type++) {
    message("Masks for particle type " << type << ":");
    for (auto const &field : mFields[type]) {
      const SingleField &tmp = field.GetField();
      /* Do the field */
      message(std::setfill(' ')
              << std::setw(15) << tmp.GetName() << ":\t mask=" << tmp.GetMask()
              << "\t size=" << tmp.GetSize());

      /* Do the first derivative */
      if (field.HasFirstDerivative()) {
        const SingleField &first = field.GetFirstDerivative();
        message(std::setfill(' ')
                << std::setw(30) << "First derivative: " << first.GetName());
      }

      /* Do the second derivative */
      if (field.HasSecondDerivative()) {
        const SingleField &second = field.GetSecondDerivative();
        message(std::setfill(' ')
                << std::setw(30) << "Second derivative: " << second.GetName());
      }
    }
  }
};

void Header::WriteOffsetDirection(enum csds_offset_direction new_value,
                                  LogFile &log) {
  mOffsetDirection = new_value;
  /* Skip file format and version numbers. */
  size_t offset = CSDS_STRING_SIZE + 2 * sizeof(int);

  log.WriteData(offset, sizeof(unsigned int), &new_value);
}

void Header::Read(LogFile &log, int verbose) {

  /* Position in the file */
  size_t offset = 0;

  /* read the file format. */
  char file_format[CSDS_STRING_SIZE];
  offset = log.ReadData(offset, CSDS_STRING_SIZE, &file_format);
  if (strcmp(file_format, CSDS_FORMAT_STRING))
    csds_error("Wrong file format: " << file_format);

  /* Read the major version number. */
  offset = log.ReadData(offset, sizeof(int), &mMajorVersion);

  /* Read the minor version number. */
  offset = log.ReadData(offset, sizeof(int), &mMinorVersion);

  /* Check the mask size */
  if (sizeof(mask_type) != CSDS_MASK_SIZE)
    csds_error("The mask sizes are not consistent ("
               << sizeof(mask_type) << " != " << CSDS_MASK_SIZE << ")");

  if (verbose > 0)
    message("File version " << mMajorVersion << "." << mMinorVersion);

  /* Read the offset directions. */
  offset = log.ReadData(offset, sizeof(int), &mOffsetDirection);

  if (!OffsetsAreForward() && !OffsetsAreBackward() && !OffsetsAreCorrupted())
    csds_error("Wrong offset value in the header:" << mOffsetDirection);

  /* Read offset to first record. */
  mOffsetFirstRecord = 0;
  offset = log.ReadData(offset, CSDS_OFFSET_SIZE, &mOffsetFirstRecord);

  /* Read the size of the strings. */
  unsigned int string_length = 0;
  offset = log.ReadData(offset, sizeof(unsigned int), &string_length);

  /* Check if value defined in this file is large enough. */
  if (CSDS_STRING_SIZE < string_length) {
    csds_error("Name too large in log file" << string_length);
  }

  /* Read the number of masks. */
  unsigned int masks_count = 0;
  offset = log.ReadData(offset, sizeof(unsigned int), &masks_count);

  /* Allocate the masks memory. */
  std::vector<struct mask_data> masks(masks_count);

  /* Loop over all masks. */
  for (unsigned int i = 0; i < masks_count; i++) {
    /* Read the mask name. */
    offset = log.ReadData(offset, string_length, masks[i].name);

    /* Set the mask value. */
    masks[i].mask = 1 << i;

    /* Read the mask data size. */
    offset = log.ReadData(offset, sizeof(unsigned int), &masks[i].size);

    /* Print the information. */
    if (verbose > 1) {
      message("Field found in the logfile: " << masks[i].name);
    }
  }

  /* Check that the special flag is at the expected place. */
  const int special_flag = 0;
  if (strcmp(masks[special_flag].name, CSDS_SPECIAL_FLAGS_NAME) != 0) {
    csds_error("The special flag should be the first mask: "
               << masks[special_flag].name);
  }

  /* Check the special flag mask */
  if (masks[special_flag].mask != CSDS_SPECIAL_FLAGS_MASK) {
    csds_error("The mask of the special flag should be "
               << CSDS_SPECIAL_FLAGS_MASK);
  }

  /* Check the size of the special flag. */
  if (masks[special_flag].size != CSDS_SPECIAL_FLAGS_SIZE) {
    csds_error("The special flag is expected to have the following size: "
               << CSDS_SPECIAL_FLAGS_SIZE);
  }

  /* Check that the timestamp is at the expected place */
  const int timestamp = 1;
  if (strcmp(masks[timestamp].name, CSDS_TIMESTAMP_NAME) != 0) {
    csds_error("The time stamp should be the second mask");
  }

  /* Check the timestamp mask */
  if (masks[timestamp].mask != CSDS_TIMESTAMP_MASK) {
    csds_error("The mask of the timestamp should be: " << CSDS_TIMESTAMP_MASK);
  }

  /* Check the size of the timestamp. */
  if (masks[timestamp].size != CSDS_TIMESTAMP_SIZE) {
    csds_error(
        "The timestamp is expected to be an integertime_t and a double.");
  }

  /* Read the number of fields per particle  */
  int number_fields[csds_type_count];
  offset = log.ReadData(offset, sizeof(number_fields), number_fields);

  /* Read the order of the fields */
  for (int type = 0; type < csds_type_count; type++) {
    if (number_fields[type] == 0) {
      continue;
    }

    /* Allocate and read the order */
    size_t size = number_fields[type] * sizeof(int);
    std::vector<int> order(number_fields[type]);
    offset = log.ReadData(offset, size, order.data());

    /* Set the special flag */
    mFields[type].emplace_back(CSDS_SPECIAL_FLAGS_NAME, masks);

    /* Set the fields */
    for (int k = 0; k < number_fields[type]; k++) {
      size_t index = order[k];
      mFields[type].emplace_back(masks[index].name, masks);
    }
  }

  /* Check the logfile header's size. */
  if (offset != mOffsetFirstRecord) {
    csds_error("Wrong header size (in header " << mOffsetFirstRecord
                                               << ", current" << offset << ")");
  }

  /* Ensures that the first offset is a timestep */
  struct record_header header;
  log.ReadRecordHeader(mOffsetFirstRecord, header);

  if (header.mask != CSDS_TIMESTAMP_MASK)
    csds_error("Log file should begin by timestep.");
};

size_t Header::GetRecordSizeFromMask(mask_type mask) const {
  size_t count = 0;

  /* Do the time stamp */
  if (mask & CSDS_TIMESTAMP_MASK) {
    count += CSDS_TIMESTAMP_SIZE;
    mask &= ~CSDS_TIMESTAMP_MASK;
  }

  /* Loop over each masks. */
  for (int part_type = 0; part_type < csds_type_count; part_type++) {
    for (auto const &field : mFields[part_type]) {

      if (mask & field.GetMask()) {
        count += field.GetFieldSize();
        mask &= ~field.GetMask();

        /* Early exit */
        if (mask == 0) {
          part_type = csds_type_count;
          break;
        }
      }
    }
  }

  /* Ensure that we found all the masks */
  if (mask != 0) {
    csds_error("A mask was not found");
  }
  return count;
}

Field const &Header::GetFieldFromName(const std::string name,
                                      enum part_type part_type) const {

  for (auto const &field : mFields[part_type]) {

    if (name.compare(field.GetName()) == 0) {
      return field;
    }
  }

  /* Check if we obtained a field. */
  csds_error("The logfile does not contain the field "
             << name << " for particle type " << part_type);
}
