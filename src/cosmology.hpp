/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/**
 * @file csds_cosmology.h
 * @brief This file contains the functions that deals with the cosmology.
 */

#ifndef CSDS_COSMOLOGY_H
#define CSDS_COSMOLOGY_H

/* local includes */
#include "inline.hpp"

/* Some standard headers */
#include <math.h>

class Parser;

/**
 * @brief Structure containing the cosmological parameters.
 */
class Cosmology {

 public:
  /**
   * @brief Compute E(a) for the derivatives of the scale factor.
   * @param scale The scale factor
   * @return The first derivative of the scale factor.
   */
  template <class Type>
  INLINE Type GetE(const Type scale) const;

  /**
   * @brief Compute the first time derivative of the scale factor
   * @param scale The scale factor
   * @return The first derivative of the scale factor.
   */
  template <class Type>
  INLINE Type GetScaleFactorDerivative(const Type scale) const {
    const Type E = GetE<Type>(scale);
    return mH0 * scale * E;
  }

  /**
   * @brief Compute the first time derivative of the scale factor
   * @param scale The scale factor
   * @return The first derivative of the scale factor.
   */
  template <class Type>
  INLINE Type GetScaleFactorSecondDerivative(const Type scale) const;

  /**
   * @brief Add the cosmological factors for the position.
   * See Hausammann et al. 2021 for details.
   *
   * @param scale The scale factor.
   * @param vel The velocity to modify.
   * @param acc The acceleration to modify.
   */
  template <class TypeVel, class TypeAcc>
  INLINE void AddFactorPosition(const TypeVel scale, TypeVel *vel,
                                TypeAcc *acc) const;
  /**
   * @brief Add the cosmological factors for the velocities.
   * \f$ \frac{dv'}{da} = \frac{g'}{\dot{a}a} \f$.
   *
   * @param scale The scale factor.
   * @param acc The acceleration to modify.
   */
  template <class TypeAcc>
  INLINE void AddFactorVelocity(const TypeAcc scale, TypeAcc *acc) const {

    const TypeAcc a_dot = GetScaleFactorDerivative<TypeAcc>(scale);
    *acc /= scale * a_dot;
  }

  /**
   * @brief Initialize the #Cosmology from a #Parser.
   */
  Cosmology(const Parser &params);
  Cosmology() {}

 protected:
  /**
   * @brief Compute the dark energy equation of state.
   * @param scale The scale factor
   * @return The equation of state.
   */
  template <class Type>
  INLINE Type GetWTilde(const Type scale) const {
    const Type w_tilde = (scale - 1.) * mWa - (1. + mW0 + mWa) * log(scale);
    return exp(3. * w_tilde);
  }

  /*! Cold dark matter density parameter */
  double mOmegaCdm;

  /*! Baryon density parameter */
  double mOmegaBaryon;

  /*! Cosmological constant density parameter */
  double mOmegaLambda;

  /*! Total radiation density parameter (photons and other relics) */
  double mOmegaRadiation;

  /*! Curvature density parameter */
  double mOmegaCurvature;

  /*! Dark-energy equation of state at z=0 */
  double mW0;

  /*! Dark-energy evolution parameter */
  double mWa;

  /*! Hubble constant at z = 0 (in internal units) */
  double mH0;
};

template <class Type>
inline Type Cosmology::GetE(const Type scale) const {

  const Type a_inv = 1. / scale;
  const Type a_inv2 = a_inv * a_inv;
  const Type omega_matter = mOmegaBaryon + mOmegaCdm;
  const Type dark_eos = GetWTilde<Type>(scale);
  const Type E = sqrt(mOmegaRadiation * a_inv2 * a_inv2 + /* Radiation */
                      omega_matter * a_inv * a_inv2 +     /* Matter */
                      mOmegaCurvature * a_inv2 +          /* Curvature */
                      mOmegaLambda * dark_eos);
  return E;
}

template <class Type>
inline Type Cosmology::GetScaleFactorSecondDerivative(const Type scale) const {

  /* Compute some powers of a */
  const Type a_inv = 1. / scale;
  const Type a_inv2 = a_inv * a_inv;
  const Type a_inv4 = a_inv2 * a_inv2;

  /* Compute E */
  const Type E = GetE<Type>(scale);

  /* Compute the derivative of E */
  Type dark_eos_der = (mWa) - (1 + mW0 + mWa) * a_inv;
  dark_eos_der *= GetWTilde<Type>(scale);
  const Type omega_matter = mOmegaBaryon + mOmegaCdm;
  const Type E_der =
      (-4 * mOmegaRadiation * a_inv4 * a_inv - 3 * omega_matter * a_inv4 -
       2 * mOmegaCurvature * a_inv2 * a_inv + dark_eos_der);

  /* Compute the full expression */
  const Type a_dot = GetScaleFactorDerivative<Type>(scale);
  return mH0 * (a_dot * E + 0.5 * scale * E_der / E);
}

template <class TypeVel, class TypeAcc>
inline void Cosmology::AddFactorPosition(const TypeVel scale, TypeVel *vel,
                                         TypeAcc *acc) const {

  const TypeVel a_dot = GetScaleFactorDerivative<TypeVel>(scale);
  const TypeVel scale2 = scale * scale;
  if (acc) {
    const TypeAcc a_dot_inv = 1. / a_dot;
    const TypeAcc scale2_inv = 1. / scale2;
    const TypeAcc a_ddot = GetScaleFactorSecondDerivative<TypeAcc>(scale);
    *acc = (*acc - 2 * a_dot * *vel) * scale;
    *acc -= scale2 * a_ddot * *vel * a_dot_inv;
    *acc *= scale2_inv * scale2_inv * a_dot_inv * a_dot_inv;
  }
  *vel /= scale2 * a_dot;
}
#endif  // CSDS_CSDS_PARAMETERS_H
