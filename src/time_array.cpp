/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* System includes */
// #include <unistd.h>
#include <algorithm>
#include <fstream>
#include <iostream>

/* This header */
#include "time_array.hpp"

/* Local includes */
#include "index_file.hpp"
#include "mapped_file.hpp"

using namespace std;

/**
 * @brief Save the array into a file.
 *
 * @param filename The filename of the index file to use.
 */
void TimeArray::Save(const string &filename) const {
  /* Check if the file exists. */
  if (!csds_file_exist(filename)) {
    csds_error("Cannot save the time array without the first index file.");
  }

  /* Open the file */
  std::ofstream f(filename, std::ofstream::out | std::ofstream::app |
                                std::ofstream::binary);
  if (!f.good()) {
    csds_error("Failed to open the file: " << filename);
  }

  /* Dump the array */
  uint64_t size = mRecords.size();
  f.write((char *)&size, sizeof(size));
  f.write((char *)mRecords.data(), sizeof(struct time_record) * size);
}

/**
 * @brief Load the array from a file
 *
 * @param filename The filename of the index file to use.
 * @param verbose The verbosity level
 *
 * @return Is the time array loaded?
 */
bool TimeArray::Load(const string &filename, int verbose) {
  /* Check if the file exists. */
  if (!csds_file_exist(filename)) {
    return false;
  }

  /* Initialize the index file that might contain the time array */
  IndexFile index(filename, /* sorted */ false, verbose);

  /* Check if the index file contains the time array */
  if (!index.ContainsTimeArray()) {
    return false;
  }

  /* Warn the user */
  if (verbose > 0) {
    message("Restoring the time array from the index file");
  }

  /* Get the position of the time array. */
  char *map = (char *)index.GetRemovedHistory(csds_type_count);

  /* Read the array */
  uint64_t size = 0;
  memcpy(&size, map, sizeof(uint64_t));
  map += sizeof(uint64_t);

  /* Allocate the array */
  mRecords.resize(size);

  /* Read the elements */
  memcpy(mRecords.data(), map, size * sizeof(struct time_record));

  return true;
}

/**
 * @brief print a #TimeArray
 *
 * @param t #time_array to print
 */
void TimeArray::Print() const {
  const size_t threshold = 5;

  size_t n = mRecords.size();
  size_t up_threshold = n - threshold;

  printf("Times (size %lu): [%lli (%g)", n, mRecords[0].int_time,
         mRecords[0].time);

  /* Loop over all elements. */
  for (size_t i = 0; i < n; i++) {
    /* Skip the times at the center of the array. */
    if (i < threshold || i > up_threshold)
      printf(", %zi (%g)", mRecords[i].offset, mRecords[i].time);

    if (i == threshold) printf(", ...");
  }

  printf("]\n");
}
