/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions that deals with the fields
 */
#ifndef CSDS_SINGLE_FIELD_H
#define CSDS_SINGLE_FIELD_H

/* Standard headers */
#include <cstddef>

/* Configuration options */
#include "config.hpp"

/* Include CSDS headers */
#include "definitions.hpp"
#include "error.hpp"
#include "tools.hpp"

#ifndef GEAR_CHEMISTRY_ELEMENT_COUNT
#define GEAR_CHEMISTRY_ELEMENT_COUNT 10
#endif

/**
 * @brief All the fields that are implemented */
enum field_enum {
  field_enum_special_flags = 0, /* Should be the first */
  field_enum_coordinates,
  field_enum_velocities,
  field_enum_accelerations,
  field_enum_masses,
  field_enum_smoothing_lengths,
  field_enum_internal_energies,
  field_enum_entropies,
  field_enum_particles_ids,
  field_enum_densities,
  field_enum_sphenix_secondary_fields,
  field_enum_birth_time,
  field_enum_gear_star_formation,
  field_enum_gear_chemistry_part,
  field_enum_gear_chemistry_spart,
  field_enum_none, /* Should be just before count */
  field_enum_count
};

/** @brief Structure for storing the data from the logfile header */
struct mask_data {
  /* Number of bytes for a mask. */
  int size;

  /* Mask value. */
  mask_type mask;

  /* Name of the mask. */
  char name[CSDS_STRING_SIZE];
};

/**
 * @brief Class representing a single field (without its derivative)
 */
class SingleField {
 public:
  SingleField() : mField(field_enum_none), mMask(0), mSize(-1) {}

  /**
   * @brief Initialize a field from its name.
   * @param name Name of the field.
   * @param allow_none Allow the field #field_enum_none.
   */
  SingleField(const std::string name, bool allow_none = false);

  /**
   * @brief Get the field.
   */
  INLINE field_enum GetFieldEnum() const { return mField; }

  /**
   * @brief Get the name of the field.
   */
  INLINE std::string GetName() const { return sNames[mField]; }

  INLINE static std::string GetNameFromEnum(field_enum field) {
    return sNames[field];
  }

  /**
   * @brief Get the #field_enum for the first derivative.
   */
  INLINE field_enum GetFirstDerivative() const;

  /**
   * @brief Get the #field_enum for the second derivative.
   */
  INLINE field_enum GetSecondDerivative() const;

  /**
   * @brief Get the size of the field.
   */
  INLINE int GetSize() const;

  /** @brief Set the mask of the field. */
  INLINE void SetMask(mask_type mask) {
    if (mMask != 0) {
      csds_error("Cannot set multiple times the mask and the position");
    }
    mMask = mask;
  };

  /** @brief Check if the expected size is correct */
  INLINE void CheckSize(int size) const {
    if (mSize != size) {
      csds_error("Inconsistent size for " << GetName() << " (" << size
                                          << " != " << mSize << ")");
    }
  }

  INLINE mask_type GetMask() const { return mMask; }

  /** @brief == and != work on the enum */
  INLINE bool operator==(const SingleField &b) const {
    return mField == b.mField;
  }
  INLINE bool operator!=(const SingleField &b) const {
    return mField != b.mField;
  }
  INLINE bool operator==(const field_enum &b) const { return mField == b; }
  INLINE bool operator!=(const field_enum &b) const { return mField != b; }
  /** @brief & and | work on the mask */
  INLINE mask_type operator&(const mask_type &b) const { return mMask & b; }
  INLINE bool operator|(const mask_type &b) const { return mMask | b; }

  /* The name of all the fields */
  static const std::string sNames[field_enum_count];

 protected:
  /* The enum corresponding to the field */
  enum field_enum mField;

  /* Its mask */
  mask_type mMask;

  /* Its size */
  int mSize;
};

inline SingleField::SingleField(const std::string name, bool allow_none)
    : mField(field_enum_none), mMask(0), mSize(-1) {

  /* Get the field */
  for (int i = 0; i < field_enum_count; i++) {
    if (name.compare(sNames[i]) == 0) {
      mField = (field_enum)i;
    }
  }

  /* Ensure that we found a field */
  if (mField == field_enum_none) {
    if (allow_none) {
      return;
    } else {
      csds_error("Field not found: " << name);
    }
  }

  /* Set the size */
  mSize = GetSize();
}

inline field_enum SingleField::GetFirstDerivative() const {
  switch (mField) {
    case field_enum_coordinates:
      return field_enum_velocities;
    case field_enum_velocities:
      return field_enum_accelerations;

    default:
      return field_enum_none;
  }
}

inline field_enum SingleField::GetSecondDerivative() const {
  switch (mField) {
    case field_enum_coordinates:
      return field_enum_accelerations;

    default:
      return field_enum_none;
  }
}

inline int SingleField::GetSize() const {
  if (mSize >= 0) return mSize;

  switch (mField) {

    case field_enum_special_flags:
      return CSDS_SPECIAL_FLAGS_SIZE;

    case field_enum_coordinates:
      return 3 * sizeof(double);

    case field_enum_velocities:
    case field_enum_accelerations:
      return 3 * sizeof(float);

    case field_enum_masses:
    case field_enum_smoothing_lengths:
    case field_enum_internal_energies:
    case field_enum_entropies:
    case field_enum_birth_time:
    case field_enum_densities:
      return sizeof(float);

    case field_enum_particles_ids:
      return sizeof(long long);

    case field_enum_sphenix_secondary_fields:
      return 7 * sizeof(float);

    case field_enum_gear_star_formation:
      return 2 * sizeof(float) + sizeof(long long);

    case field_enum_gear_chemistry_part:
      return 2 * GEAR_CHEMISTRY_ELEMENT_COUNT * sizeof(double);

    case field_enum_gear_chemistry_spart:
      return GEAR_CHEMISTRY_ELEMENT_COUNT * sizeof(double);

    default:
      csds_error("The field " << sNames[mField] << "is not implemented");
  }
}
#endif  // CSDS_SINGLE_FIELD_H
