/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include header */
#include "cosmology.hpp"

/* Include CSDS */
#include "parser.hpp"
#include "tools.hpp"

Cosmology::Cosmology(const Parser &params) {

  /* Read Omega_cdm */
  mOmegaCdm = params.GetParam<double>("Cosmology:Omega_cdm");

  /* Read Omega_lambda */
  mOmegaLambda = params.GetParam<double>("Cosmology:Omega_lambda");

  /* Read Omega_b */
  mOmegaBaryon = params.GetParam<double>("Cosmology:Omega_b");

  /* Read Omega_r */
  mOmegaRadiation = params.GetParam<double>("Cosmology:Omega_r");

  /* Read Omega_k */
  mOmegaCurvature = params.GetParam<double>("Cosmology:Omega_k");

  /* Read Omega_nu_0 */
  double omega_nu_0 = params.GetOptParam<double>("Cosmology:Omega_nu_0", 0);

  if (omega_nu_0 != 0)
    csds_error("Neutrinos are not implemented in the cosmology.");

  /* Read w_0 */
  mW0 = params.GetParam<double>("Cosmology:w_0");

  /* Read w_a */
  mWa = params.GetParam<double>("Cosmology:w_a");

  /* Read the Hubble constant at z=0 */
  mH0 = params.GetParam<double>("Cosmology:Hubble0");
}
