/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions related to openmp.
 */
#ifndef CSDS_OPENMP_H
#define CSDS_OPENMP_H

#include "../config.hpp"

/* Include standard headers */
#ifdef CSDS_WITH_OPENMP
#include <omp.h>
#endif
#include <vector>

/* Local includes */
#include "inline.hpp"

/**
 * @brief Wrapper for omp_get_max_threads
 */
INLINE static int csds_get_max_threads(void) {
#ifdef CSDS_WITH_OPENMP
  return omp_get_max_threads();
#else
  return 1;
#endif
}

/**
 * @brief Wrapper for omp_get_thread_num
 */
INLINE static int csds_get_thread_num(void) {
#ifdef CSDS_WITH_OPENMP
  return omp_get_thread_num();
#else
  return 0;
#endif
}

/**
 * @brief Wrapper for omp_in_parallel
 */
INLINE static bool csds_in_parallel(void) {
#ifdef CSDS_WITH_OPENMP
  return omp_in_parallel();
#else
  return false;
#endif
}

#endif  // CSDS_OPENMP_H
