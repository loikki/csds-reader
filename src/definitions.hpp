/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains a few definitions useful for
 * both the reader and writer.
 */
#ifndef CSDS_SPECIAL_FLAGS_H
#define CSDS_SPECIAL_FLAGS_H

/* WARNING THIS FILE SHOULD NOT INCLUDE CONFIG.H*/
#include "version.hpp"

/* Standard headers */
#include <stdint.h>

/* Size of the strings. */
#define CSDS_STRING_SIZE 200
#define CSDS_FORMAT_STRING "CSDS"

/* Defines the special flags */
#define CSDS_SPECIAL_FLAGS_NAME "SpecialFlags"
// The fields arrays are constructed such as the special
// flag is always first (see csds_header.c)
#define CSDS_SPECIAL_FLAGS_INDEX 0
#define CSDS_SPECIAL_FLAGS_SIZE sizeof(uint32_t)
#define CSDS_SPECIAL_FLAGS_MASK (1 << CSDS_SPECIAL_FLAGS_INDEX)

/* Number of bytes for the mask information in the headers
   (need to be large enough for the larges mask) */
#define CSDS_HEADER_SIZE 8
#define CSDS_MASK_SIZE 2
/* Number of bytes for the offset size in the headers */
#define CSDS_OFFSET_SIZE (CSDS_HEADER_SIZE - CSDS_MASK_SIZE)

/** @brief type to use for the masks */
typedef short int mask_type;

/* Defines the timestamp */
typedef long long integertime_t;
#define CSDS_TIMESTAMP_INDEX 1
#define CSDS_TIMESTAMP_MASK (1 << CSDS_TIMESTAMP_INDEX)
#define CSDS_TIMESTAMP_NAME "Timestamp"
#define CSDS_TIMESTAMP_SIZE sizeof(integertime_t) + sizeof(double)

/** @brief Value of the special flag */
enum csds_special_flags {
  csds_flag_none = 0,        /* No flag */
  csds_flag_change_type = 1, /* Flag for a change of particle type */
  csds_flag_mpi_enter, /* Flag for a particle received from another  MPI rank
                        */
  csds_flag_mpi_exit,  /* Flag for a particle sent to another MPI rank */
  csds_flag_delete,    /* Flag for a deleted particle */
  csds_flag_create,    /* Flag for a created particle */
} __attribute__((packed));

/*
 * @brief Direction of the offsets.
 */
enum csds_offset_direction {
  csds_offset_backward = 0,
  csds_offset_forward,
  csds_offset_corrupted,
  /* Number of offset type. */
  csds_offset_count,
};

#define id_type int64_t

/**
 * @brief Data structure contained in the index files.
 */
struct index_data {
  /* Id of the particle. */
  id_type id;

  /* Offset of the particle in the file. */
  uint64_t offset;
};

/** @brief Structure for the record header */
struct record_header {
  mask_type mask;
  size_t offset;
};

/**
 * @brief This structure contains all the information present in a time record.
 */
struct time_record {
  /* Integertime of the records. */
  // TODO remove this
  integertime_t int_time;

  /* Double time of the records. */
  double time;

  /* Offset in the file of the time records. */
  size_t offset;
};

#endif  // CSDS_SPECIAL_FLAGS_H
