/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions for the cache.
 */
#ifndef CSDS_CACHE_H
#define CSDS_CACHE_H

/* Include config */
#include "../config.hpp"

/* Include CSDS headers */
#include "csds_array.hpp"
#include "error.hpp"
#include "field.hpp"
#include "inline.hpp"
#include "particle.hpp"
#include "particle_type.hpp"
#include "tools.hpp"

/* Standard headers */
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

/** @brief Structure for the cache.
 * It allows an efficient evolution of the particles.
 *
 * The pointers are always in this order:
 *  - The type of particles
 *  - The particle
 *
 * The index of a field (e.g. last.fields) is given by:
 * part_index * cache->last_time_offset + cache->offset[field_index]
 *
 * Each field contains: the field, its first derivative and its second
 * derivative. If the field does not have derivatives, they will simply be empty
 * (size 0).
 */
class Cache {

 public:
  /** @brief Default constructor (implemented for arrays) */
  Cache() : mCurrentIndex(0), mSize(0) {}

  /**
   * @brief Initialize the structure.
   *
   * @param number_particles The number of particles to allocate
   * @param output_array The output array (that contains the fields)
   * @param over_allocation The over allocation (required for creation /
   * deletion of particles)
   * @param init_size The initial size of the arrays if no particles are present
   * @param time_offset The time offset of the current reading
   */
  Cache(uint64_t number_particles, const std::vector<CsdsArray> &output_array,
        float over_allocation, int64_t init_size, size_t time_offset);

  /** @brief Get the field index in the cache */
  INLINE int GetFieldIndex(const Field &field) const {
    for (int i = 0; i < mNFields; i++) {
      const Field &current_field = mPrev.fields[i].GetField();
      if (current_field.GetField() == field.GetField()) {
        return i;
      }
    }

    /* Check that we found a field */
    csds_error("The field not found in the cache" << field.GetName());
  }

  /**
   * @brief Save a field inside the cache.
   *
   * @param index The index of the particle in the cache (-1 to get a new one)
   * @param time_before The time of the previous record
   * @param before The field of the previous record
   * @param time_after The time of the next record
   * @param after The field of the next record
   * @param offset_next The offset of the next record
   * @param field The field stored in after and before
   */
  int64_t Save(int64_t index, double time_before,
               const struct csds_reader_field &before, double time_after,
               const struct csds_reader_field &after, size_t offset_next,
               const Field &field);
  /**
   * @brief Remove a particle from the cache.
   *
   * @param index The index of the particle in the cache
   */
  void RemoveParticle(size_t index);

  /** @brief Reset the index used for parallel loops */
  INLINE void ResetCurrentIndex() { mCurrentIndex = 0; }

  /** @brief Update the time and offset of the particle.
   *
   * @param index The index of the particle within the cache.
   * @param time_before The time of the previous record.
   * @param time_after The time of the next record.
   * @param offset_after The offset of the next record.
   */
  INLINE void UpdateTime(int64_t index, double time_before, double time_after,
                         size_t offset_after) {
    mPrev.time[index] = time_before;
    mNext.time[index] = time_after;
    mNext.offset[index] = offset_after;
  }

#ifdef CSDS_DEBUG_CHECKS
  /** @brief Do an expansive check */
  void Check();
#endif

  /* Getters and Setters */
  /* Time */
  INLINE double GetTimeBefore(int64_t index) const { return mPrev.time[index]; }
  INLINE double GetTimeAfter(int64_t index) const { return mNext.time[index]; }
  INLINE size_t GetOffsetAfter(int64_t index) const {
    return mNext.offset[index];
  }
  INLINE size_t GetLastTimeOffset() const { return mLastTimeOffset; }
  INLINE void SetLastTimeOffset(size_t offset) { mLastTimeOffset = offset; }

  /* Fields */
  INLINE int GetNumberFields() const { return mNFields; }
  INLINE struct csds_reader_field GetFieldAfter(int64_t index,
                                                int field_index) {
    return mNext.fields[field_index].GetBuffers(index);
  };
  INLINE struct csds_reader_field GetFieldBefore(int64_t index,
                                                 int field_index) {
    return mPrev.fields[field_index].GetBuffers(index);
  };
  INLINE Field const &GetFieldFromIndex(int i) const {
    return mPrev.fields[i].GetField();
  }

  void SetCurrentIndex(size_t i) { mCurrentIndex = i; };

  /* Size getters */
  INLINE size_t Size() const { return mSize; }
  INLINE size_t Capacity() const { return mPrev.time.capacity(); }

 private:
  /* The current index to use */
  size_t mCurrentIndex;

  /* The size of the arrays */
  size_t mSize;

  /* Variables of the last record */
  struct {
    /* The fields read */
    std::vector<CsdsArray> fields;

    /* The time of the record */
    std::vector<double> time;
  } mPrev;

  /* Variables of the next record */
  struct {
    /* The fields read */
    std::vector<CsdsArray> fields;

    /* The time of the record */
    std::vector<double> time;

    /* The offset of the record */
    std::vector<size_t> offset;
  } mNext;

  /* The number of fields */
  int mNFields;

  /* Last time offset requested */
  size_t mLastTimeOffset;
};

/* Define inlined functions */

inline Cache::Cache(uint64_t number_particles,
                    const std::vector<CsdsArray> &output_array,
                    float over_allocation, int64_t init_size,
                    size_t time_offset)
    : mCurrentIndex(0),
      mSize(0),
      mNFields(output_array.size()),
      mLastTimeOffset(time_offset) {

  /* Compute the size of the arrays */
  size_t capacity =
      number_particles == 0 ? init_size : number_particles * over_allocation;

  /* Copy the header's field into the cache */
  for (auto const &array : output_array) {
    mPrev.fields.emplace_back(capacity, array.GetField());
    mNext.fields.emplace_back(capacity, array.GetField());
  }

  /* Allocate the memory */
  mPrev.time.reserve(capacity);
  mNext.time.reserve(capacity);
  mNext.offset.reserve(capacity);
}

inline int64_t Cache::Save(int64_t index, double time_before,
                           const struct csds_reader_field &before,
                           double time_after,
                           const struct csds_reader_field &after,
                           size_t offset_next, const Field &field) {

  const bool new_part = index < 0;

  /* Get a new index if needed */
  if (new_part) {
#pragma omp atomic capture
    {
      index = mCurrentIndex;
      mCurrentIndex++;
    }
#pragma omp atomic update
    mSize++;
  }

  /* Ensures that we have enough place */
  if ((size_t)index >= mPrev.time.capacity()) {
    csds_error("The cache is too small. Please increase Cache:OverAllocation.");
  }

  /* Get the index of the field */
  int field_index = GetFieldIndex(field);

#ifdef CSDS_DEBUG_CHECKS
  /* Check if the data are compatible with the previous writing */
  if (!new_part) {
    if (time_before != mPrev.time[index] || time_after != mNext.time[index] ||
        offset_next != mNext.offset[index]) {
      csds_error("The data within the cache are not compatible");
    }
  }
#endif

  /* Copy the data into the arrays */
  if (new_part) {
    mPrev.time[index] = time_before;
    mNext.time[index] = time_after;
    mNext.offset[index] = offset_next;
  }

  /* Copy the fields into the arrays */
  mPrev.fields[field_index].Save(index, before);
  mNext.fields[field_index].Save(index, after);

  return index;
}

inline void Cache::RemoveParticle(size_t index) {
  // TODO make it thread safe.
  /* The tricky part is to consider a swap between two
   * particles that should be removed */

  /* Decrease the counters */
  mSize--;
  const size_t size = mSize;

  /* Ensures that the particle was not already removed */
  if (index >= size) {
    return;
  }

  /* Swap with the last particle */
  for (int i = 0; i < mNFields; i++) {
    /* Previous field */
    struct csds_reader_field tmp = mPrev.fields[i].GetBuffers(size);
    mPrev.fields[i].Save(index, tmp);

    /* Next field */
    tmp = mNext.fields[i].GetBuffers(size);
    mNext.fields[i].Save(index, tmp);
  }

  /* Update the arrays */
  mPrev.time[index] = mPrev.time[size];
  mNext.time[index] = mNext.time[size];
  mNext.offset[index] = mNext.offset[size];

#ifdef CSDS_DEBUG_CHECKS
  /* Ensure that something will go wrong if trying to access
   * the removed particle */
  mPrev.time[size] = -1;
  mNext.time[size] = -2;
  mNext.offset[size] = 0;
#endif
}

#ifdef CSDS_DEBUG_CHECKS
inline void Cache::Check() {
  message("Checking");
  /* Check if the times are correct */
  for (size_t part = 0; part < mSize; part++) {
    const double t0 = mPrev.time[part];
    const double t1 = mNext.time[part];
    if (t1 < t0) {
      csds_error("Error within the cache");
    }
    if (mNext.offset[part] == 0) {
      csds_error("Found a wrong offset in the cache");
    }
  }

  /* Check if the particles are unique */
  int part_id_index = -1;
  /* Find the index of the particle IDs */
  for (int i = 0; i < mNFields; i++) {
    const Field &field = mPrev.fields[i].GetField();
    if (field.GetField() == field_enum_particles_ids) {
      part_id_index = i;
      break;
    }
  }

  /* Did we found it? */
  if (part_id_index == -1) {
    return;
  }

  /* Now do the check */
#pragma omp parallel for
  for (size_t pi = 0; pi < mSize; pi++) {
    for (size_t pj = pi + 1; pj < mSize; pj++) {

      /* Get the IDs */
      id_type last_id_i = *(id_type *)mPrev.fields[part_id_index][pi];
      id_type next_id_i = *(id_type *)mNext.fields[part_id_index][pi];
      id_type last_id_j = *(id_type *)mPrev.fields[part_id_index][pj];
      id_type next_id_j = *(id_type *)mNext.fields[part_id_index][pj];

      /* Check if evolution is compatible */
      if (last_id_i != next_id_i || last_id_j != next_id_j) {
        csds_error("The particle IDs are not compatible");
      }

      /* Check if we have twice the same particle */
      if (last_id_i == last_id_j) {
        csds_error("Found twice the same particle in the cache");
      }
    }
  }

  message("Checking done");
}
#endif

#endif  // CSDS_CACHE_H
