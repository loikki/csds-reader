/*******************************************************************************
 * This file is part of CSDS and was copied from SWIFT.
 * Copyright (c) 2021 loic.hausammann@epfl.ch
 *               SWIFT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Config parameters. */
#include "../config.hpp"

/* Some standard headers. */
/* Needs to be included so that strtok returns char * instead of a int *. */
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/* This object's header. */
#include "parser.hpp"

/* Local headers. */
#include "error.hpp"

#define FIELD_BUFFER_SIZE 64
#define PARSER_COMMENT_STRING "#"
#define PARSER_COMMENT_CHAR '#'
#define PARSER_VALUE_CHAR ':'
#define PARSER_VALUE_STRING ":"
#define PARSER_START_OF_FILE "---"
#define PARSER_END_OF_FILE "..."

#define CHUNK 10

/**
 * @brief trim leading white space from a string.
 *
 * Returns pointer to first character.
 *
 * @param s the string.
 * @result the result.
 */
char *Parser::TrimLeading(char *s) {
  if (s == NULL || strlen(s) < 2) return s;
  while (isspace(*s)) s++;
  return s;
}

/**
 * @brief trim trailing white space from a string.
 *
 * Modifies the string by adding a NULL to the end.
 *
 * @param s the string.
 * @result the result.
 */
char *Parser::TrimTrailing(char *s) {
  if (s == NULL || strlen(s) < 2) return s;
  char *end = s + strlen(s) - 1;
  while (isspace(*end)) end--;
  *(end + 1) = '\0';
  return s;
}

/**
 * @brief trim leading and trailing white space from a string.
 *
 * Can modify the string by adding a NULL to the end.
 *
 * @param s the string.
 * @result the result.
 */
char *Parser::TrimBoth(char *s) {
  if (s == NULL || strlen(s) < 2) return s;
  return TrimTrailing(TrimLeading(s));
}

/**
 * @brief Reads an input file and stores each parameter in a structure.
 *
 * @param file_name Name of file to be read
 */
Parser::Parser(const std::string file_name) {
  /* Open file for reading */
  FILE *file = fopen(file_name.data(), "r");

  /* Line to parsed. */
  char line[PARSER_MAX_LINE_SIZE];

  /* Initialise parameter count. */
  mParamCount = 0;
  mSectionCount = 0;
  mFilename = file_name;
  mLineNumber = 0;

  /* Check if parameter file exits. */
  if (file == NULL) {
    csds_error("Error opening parameter file: " << file_name);
  }

  /* Read until the end of the file is reached.*/
  while (!feof(file)) {
    if (fgets(line, PARSER_MAX_LINE_SIZE, file) != NULL) {
      mLineNumber++;
      ParseLine(line);
    }
  }

  fclose(file);
}

/**
 * @brief Counts the number of white spaces that prefix a string.
 *
 * @param str String to be checked
 *
 * @return Number of white spaces prefixing str
 */
int Parser::CountIndentation(const char *str) {
  int count = 0;

  /* Check if the line contains the character */
  while (*(++str) == ' ') {
    count++;
  }
  return count;
}

/**
 * @brief Checks if a string is empty.
 *
 * @param str String to be checked
 *
 * @return Returns 1 if str is empty, 0 otherwise
 */
int Parser::IsEmpty(const char *str) {
  int retParam = 1;
  while (*str != '\0') {
    if (!isspace(*str)) {
      retParam = 0;
      break;
    }
    str++;
  }

  return retParam;
}

/**
 * @brief Look for duplicate parameters.
 * @param param_name Name of parameter to be searched for
 */
void Parser::FindDuplicateParams(const char *param_name) const {
  for (int i = 0; i < mParamCount; i++) {
    if (!strcmp(param_name, mData[i].name)) {
      csds_error("Invalid line:" << mLineNumber << " '" << param_name
                                 << "', parameter is a duplicate.");
    }
  }
}

/**
 * @brief Look for duplicate sections.
 * @param section_name Name of section to be searched for
 */
void Parser::FindDuplicateSection(const char *section_name) const {
  for (int i = 0; i < mSectionCount; i++) {
    if (!strcmp(section_name, mSection[i].name)) {
      csds_error("Invalid line:" << mLineNumber << " '" << section_name
                                 << "', parameter is a duplicate.");
    }
  }
}

/**
 * @brief Parses a line from a file and stores any parameters in a structure.
 * @param line Line to be parsed.
 */
void Parser::ParseLine(char *line) {
  /* Parse line if it doesn't begin with a comment. */
  if (line[0] != PARSER_COMMENT_CHAR) {
    char trim_line[PARSER_MAX_LINE_SIZE];
    char tmp_str[PARSER_MAX_LINE_SIZE];
    char *token;

    /* Remove comments at the end of a line. */
    token = strtok(line, PARSER_COMMENT_STRING);
    strcpy(tmp_str, token);

    /* Check if the line is just white space. */
    if (!IsEmpty(tmp_str)) {
      /* Trim '\n' characters from string. */
      token = strtok(tmp_str, "\n");
      strcpy(trim_line, token);

      /* Check if the line contains a value and parse it. */
      if (strchr(trim_line, PARSER_VALUE_CHAR)) {

        /* Trim trailing space before parsing line for a value. */
        char no_space_line[PARSER_MAX_LINE_SIZE];
        strcpy(no_space_line, TrimTrailing(trim_line));

        ParseValue(no_space_line);
      }
      /* Check for invalid lines,not including the start and end of file. */
      else if (strcmp(trim_line, PARSER_START_OF_FILE) &&
               strcmp(trim_line, PARSER_END_OF_FILE)) {
        csds_error("Invalid line:" << mLineNumber << " '" << trim_line << "'");
      }
    }
  }
}

/**
 * @brief Performs error checking and stores a parameter in a structure.
 * @param line Line containing the parameter
 */
void Parser::ParseValue(char *line) {
  static int inSection = 0;
  static char section[PARSER_MAX_LINE_SIZE]; /* Keeps track of current section
                                                name. */
  static int isFirstParam = 1;
  char tmpStr[PARSER_MAX_LINE_SIZE];
  char tmpSectionName[PARSER_MAX_LINE_SIZE];

  char *token;

  /* Check that standalone parameters have correct indentation. */
  if (!inSection && line[0] == ' ') {
    csds_error("Invalid line:" << mLineNumber << " '" << line
                               << "', standalone parameter defined with "
                               << "incorrect indentation.");
  }

  /* Check that it is a parameter inside a section.*/
  if (line[0] == ' ' || line[0] == '\t') {
    ParseSectionParam(line, &isFirstParam, section);
  } else {
    /* It is the start of a new section or standalone parameter.
     * Take first token as the parameter name. */
    token = strtok(line, ":\t");
    strcpy(tmpStr, TrimTrailing(token));

    /* Take second token as the parameter value. */
    token = TrimBoth(strtok(NULL, "#\n"));

    /* If second token is NULL or empty then the line must be a section
     * heading. */
    if (token == NULL || strlen(token) == 0) {
      strcpy(tmpSectionName, tmpStr);
      strcat(tmpSectionName, PARSER_VALUE_STRING);

      /* Check for duplicate section name. */
      FindDuplicateSection(tmpSectionName);

      /* Check for duplicate standalone parameter name used as a section name.
       */
      FindDuplicateParams(tmpStr);

      strcpy(section, tmpSectionName);
      strcpy(mSection[mSectionCount].name, tmpSectionName);
      if (mSectionCount == PARSER_MAX_NO_OF_SECTIONS - 1) {
        csds_error(
            "Maximal number of sections in parameter file reached. Aborting !");
      } else {
        mSectionCount++;
      }
      inSection = 1;
      isFirstParam = 1;
    } else {
      /* Create string with standalone parameter name appended with ":" to aid
       * duplicate search as section names are stored with ":" at the end.*/
      strcpy(tmpSectionName, tmpStr);
      strcat(tmpSectionName, PARSER_VALUE_STRING);

      /* Check for duplicate parameter name. */
      FindDuplicateParams(tmpStr);

      /* Check for duplicate section name used as standalone parameter name. */
      FindDuplicateSection(tmpSectionName);

      /* Must be a standalone parameter so no need to prefix name with a
       * section. */
      strcpy(mData[mParamCount].name, tmpStr);
      strcpy(mData[mParamCount].value, token);
      if (mParamCount == PARSER_MAX_NO_OF_PARAMS - 1) {
        csds_error(
            "Maximal number of parameters in parameter file reached. Aborting "
            "!");
      } else {
        mParamCount++;
      }
      inSection = 0;
      isFirstParam = 1;
    }
  }
}

/**
 * @brief Parses a parameter that appears in a section and stores it in a
 *structure.
 *
 * @param line Line containing the parameter
 * @param isFirstParam Shows if the first parameter of a section has been found
 * @param sectionName String containing the current section name
 */
void Parser::ParseSectionParam(char *line, int *isFirstParam,
                               char *sectionName) {
  static int sectionIndent = 0;
  char tmpStr[PARSER_MAX_LINE_SIZE];
  char paramName[PARSER_MAX_LINE_SIZE];
  char *token;

  /* Count indentation of each parameter and check that it
   * is consistent with the first parameter in the section. */
  if (*isFirstParam) {
    sectionIndent = CountIndentation(line);
    *isFirstParam = 0;
  } else if (CountIndentation(line) != sectionIndent) {
    csds_error("Invalid line:" << mLineNumber << " '" << line
                               << "', parameter has incorrect indentation.");
  }

  /* Take first token as the parameter name and trim leading white space. */
  token = TrimBoth(strtok(line, ":\t"));
  strcpy(tmpStr, token);

  /* Take second token as the parameter value. */
  token = TrimBoth(strtok(NULL, "#\n"));

  /* Prefix the parameter name with its section name and
   * copy it into the parameter structure. */
  strcpy(paramName, sectionName);
  strcat(paramName, tmpStr);

  /* Check for duplicate parameter name. */
  FindDuplicateParams(paramName);

  strcpy(mData[mParamCount].name, paramName);
  strcpy(mData[mParamCount].value, token);
  if (mParamCount == PARSER_MAX_NO_OF_PARAMS - 1) {
    csds_error(
        "Maximal number of parameters in parameter file reached. Aborting !");
  } else {
    mParamCount++;
  }
}

/**
 * @brief Retrieve string parameter from structure.
 *
 * @param name Name of the parameter to be found
 * @param retParam (return) Value of the parameter found
 */
void Parser::GetParamString(const char *name, char *retParam) const {

  for (int i = 0; i < mParamCount; i++) {
    if (!strcmp(name, mData[i].name)) {
      strcpy(retParam, mData[i].value);
      return;
    }
  }

  csds_error("Cannot find '" << name << "' in the structure.");
}

/**
 * @brief Retrieve string parameter from structure.
 *
 * @param name Name of the parameter to be found
 * @param def Default value of the parameter of not found.
 * @param retParam (return) Value of the parameter found
 */
void Parser::GetOptParamString(const char *name, char *retParam,
                               const char *def) const {

  for (int i = 0; i < mParamCount; i++) {
    if (!strcmp(name, mData[i].name)) {
      strcpy(retParam, mData[i].value);
      return;
    }
  }
  strcpy(retParam, def);
}

/**
 * @brief Prints the contents of the parameter structure.
 */
void Parser::PrintParams() const {
  printf("\n--------------------------\n");
  printf("|   CSDS Parameter File  |\n");
  printf("--------------------------\n");

  for (int i = 0; i < mParamCount; i++) {
    printf("Parameter name: %s\n", mData[i].name);
    printf("Parameter value: %s\n", mData[i].value);
  }
}
