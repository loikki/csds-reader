/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_TIMELINE_H
#define CSDS_TIMELINE_H

/* Standard headers */
#include <algorithm>
#include <string>

/* Local headers */
#include "header.hpp"
#include "tools.hpp"

typedef long long integertime_t;

/**
 * @brief This structure contains all the time records.
 *
 * In order to obtain easily the time step of a record,
 * this structure is required. It contains all the time step
 * with their integer time, double time and position in the file.
 * It also contains all the required functions to quickly access
 * this information.
 */
class TimeArray {
 protected:
  /** @brief Compare the offset of two records */
  static bool CompareOffset(const struct time_record &t1,
                            const struct time_record &t2) {
    return t1.offset < t2.offset;
  }
  /** @brief Compare the time of two records */
  static bool CompareTime(const struct time_record &t1,
                          const struct time_record &t2) {
    return t1.time < t2.time;
  }

 public:
  TimeArray(){};

  /** @brief Add a time record to the array */
  void Append(const struct time_record &time) { mRecords.push_back(time); };

  /* The arrays is expecting to be large => no copy */
  TimeArray(TimeArray const &) = delete;
  void operator=(TimeArray const &) = delete;

  /**
   * @brief Get the index of the previous record from an offset.
   * @param offset offset of the record.
   * @return integer time of the record.
   */
  size_t GetIndexFromOffset(const size_t offset) const;

  /**
   * @brief Find the index of the previous time record according to a time.
   * @param time The time requested.
   * @return The index of the last time record.
   */
  size_t GetIndexFromTime(const double time) const;
  /**
   * @brief Get the previous time record from an offset.
   * @param offset offset of the record.
   * @return integer time of the record.
   */
  struct time_record const &GetRecordFromOffset(const size_t offset) const {
    return mRecords[GetIndexFromOffset(offset)];
  };

  /**
   * @brief Find the previous time record from a given time.
   * @param time The time requested.
   * @return The record
   */
  struct time_record const &GetRecordFromTime(const double time) const {
    return mRecords[GetIndexFromTime(time)];
  };

  /** @brief Print the structure */
  void Print() const;

  /** @brief Save the array into an index file given by filename */
  void Save(const std::string &filename) const;
  /** @brief Load the array from an index file.
   * @param filename The name of the file
   * @param verbose The verbose level
   * @return If the array is present in the index file.
   */
  bool Load(const std::string &filename, int verbose);

  size_t Size() const { return mRecords.size(); }

  /**
   * @brief Access an element
   */
  INLINE struct time_record const &operator[](std::size_t i) const {
#ifdef CSDS_DEBUG_CHECKS
    if (i >= Size()) {
      csds_error(
          "Trying to access an element larger"
          " than the maximal size: "
          << i << " " << Size());
    }
#endif
    return mRecords[i];
  }

 protected:
  /* The complete list of time record */
  std::vector<struct time_record> mRecords;
};

inline size_t TimeArray::GetIndexFromOffset(const size_t offset) const {
#ifdef CSDS_DEBUG_CHECKS
  const size_t size = mRecords.size();
  if (offset < mRecords[0].offset || offset > mRecords[size - 1].offset)
    csds_error("Offset outside of range. In the expected order: "
               << mRecords[size - 1].offset << " > " << offset << " > "
               << mRecords[0].offset);
#endif

  /* Find the time_array with the correct offset through a bisection method.
   */
  struct time_record value = {.int_time = 0, .time = 0, .offset = offset};
  auto it = std::lower_bound(mRecords.cbegin(), mRecords.cend(), value,
                             CompareOffset);
  size_t ret = it - mRecords.cbegin();

  /* We want the time before, not after */
  if (it->offset != offset) {
    ret--;
  }

#ifdef CSDS_DEBUG_CHECKS
  if (mRecords[ret].offset > offset ||
      (ret + 1 != size && mRecords[ret + 1].offset <= offset)) {
    csds_error("Found the wrong element");
  }

#endif
  return ret;
}

inline size_t TimeArray::GetIndexFromTime(const double time) const {
  /* Deal with the special case of the last time record
     A sentinel is present, therefore skip it.
  */
  const size_t size = mRecords.size();
  if (time == mRecords[size - 2].time) {
    return size - 2;
  }

#ifdef CSDS_DEBUG_CHECKS
  if (time < mRecords[0].time || time > mRecords[size - 1].time)
    csds_error("Time outside of range. In the expected order: "
               << mRecords[0].time << " < " << time << " < "
               << mRecords[size - 1].time);
#endif

  /* Find the time_array with the correct offset through a bisection method.
   */
  struct time_record value = {.int_time = 0, .time = time, .offset = 0};
  auto it =
      std::lower_bound(mRecords.cbegin(), mRecords.cend(), value, CompareTime);
  size_t ret = it - mRecords.cbegin();

  /* We want the time before, not after */
  if (it->time != time) {
    ret--;
  }

#ifdef CSDS_DEBUG_CHECKS
  if (mRecords[ret].time > time || mRecords[ret + 1].time <= time) {
    csds_error("Found the wrong element");
  }

#endif

  return ret;
}

#endif  // CSDS_TIMELINE_H
