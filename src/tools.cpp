/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/* Standard headers */
#include <iomanip>
#include <stdio.h>

/* This file's header */
#include "tools.hpp"

/* CSDS headers */
#include "header.hpp"
#include "mapped_file.hpp"
#include "particle.hpp"
#include "reader.hpp"

using namespace std::chrono;

/**
 * @brief Remove the previous text and print the progress of current task.
 *
 * @param percent The current progress.
 * @param tic The initial time
 * @param message The message to display before the progress message.
 */
void tools_print_progress(float percent,
                          const high_resolution_clock::time_point init,
                          const std::string message) {

  /* Get the remaining time */
  const float delta_time = GetDeltaTime(init) / 1000.0;
  int remaining_time = delta_time * (100. - percent) / percent;

  /* Compute the time */
  int hour = remaining_time / (60 * 60);
  remaining_time -= hour * 60 * 60;
  int min = remaining_time / 60;
  int sec = remaining_time - min * 60;

  /* Write the message */
  std::ios_base::fmtflags error_flags(std::cout.flags());
  std::streamsize ss = std::cout.precision();
  std::cout << "\r" << message << ": ";
  std::cout << std::fixed << std::setprecision(1) << percent;
  std::cout << "% done, Remaining time: ";

  /* Write the hour */
  if (hour != 0) std::cout << hour << "h ";

  /* Write the minutes */
  if (min != 0) std::cout << min << "min ";

  /* Write the seconds */
  std::cout << sec << "s";
  std::cout.flags(error_flags);
  std::cout.precision(ss);
}
