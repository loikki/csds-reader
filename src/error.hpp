/*******************************************************************************
 * This file is part of CSDS and was copied from SWIFT.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *               SWIFT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions that help to deal with messages.
 */
#ifndef CSDS_ERROR_H
#define CSDS_ERROR_H

/* Standard headers */
#include <chrono>
#include <iomanip>
#include <iostream>

/* Include config */
#include "../config.hpp"

/* Local includes */
#include "inline.hpp"
#include "openmp.hpp"

/* Use exit when not developing, avoids core dumps. */
#ifdef CSDS_DEBUG_CHECKS
#define csds_abort(errcode) abort()
#else
#define csds_abort(errcode) exit(errcode)
#endif

using namespace std::chrono;
const high_resolution_clock::time_point INITIAL_TIME =
    high_resolution_clock::now();

INLINE static int GetDeltaTime(const high_resolution_clock::time_point init,
                               const high_resolution_clock::time_point now =
                                   high_resolution_clock::now()) {
  duration<double, std::milli> span = now - init;
  return span.count();
}

#ifdef CSDS_DEBUG_CHECKS
/**
 * @brief Error macro. Prints the message given in argument and aborts.
 */
#define csds_error(input)                                                   \
  {                                                                         \
    float delta_time_error = GetDeltaTime(INITIAL_TIME);                    \
    delta_time_error /= 1000; /* into seconds */                            \
                                                                            \
    std::cout << std::flush;                                                \
    std::cerr << "[" << std::setfill('0') << std::setw(6)                   \
              << std::setprecision(1) << std::fixed << delta_time_error     \
              << std::scientific << "] " << __FILE__ << ":" << __FUNCTION__ \
              << ":" << __LINE__ << ": " << input << std::endl;             \
    csds_abort(1);                                                          \
  }
#else
/**
 * @brief Error macro. Prints the message given in argument and aborts.
 */
#define csds_error(input)                                                   \
  {                                                                         \
    float delta_time_error = GetDeltaTime(INITIAL_TIME);                    \
    delta_time_error /= 1000; /* into seconds */                            \
                                                                            \
    std::cout << std::flush;                                                \
    std::cerr << "[" << std::setfill('0') << std::setw(6)                   \
              << std::setprecision(1) << std::fixed << delta_time_error     \
              << std::scientific << "] " << __FILE__ << ":" << __FUNCTION__ \
              << ":" << __LINE__ << ": " << input << std::endl;             \
    if (csds_in_parallel())                                                 \
      csds_abort(1);                                                        \
    else                                                                    \
      throw std::runtime_error("See above for the description");            \
  }
#endif

/**
 * @brief Macro to print a localized message with variable arguments.
 */
#define message(input)                                                  \
  {                                                                     \
    float delta_time_error = GetDeltaTime(INITIAL_TIME);                \
    delta_time_error /= 1000; /* into seconds */                        \
    std::ios_base::fmtflags error_flags(std::cout.flags());             \
    std::streamsize ss = std::cout.precision();                         \
    std::cout << "[" << std::setfill('0') << std::setw(6)               \
              << std::setprecision(1) << std::fixed << delta_time_error \
              << "] " << __FUNCTION__ << ": ";                          \
    std::cout.flags(error_flags);                                       \
    std::cout.precision(ss);                                            \
    std::cout << input << std::endl;                                    \
  }

#endif  // CSDS_ERROR_H
