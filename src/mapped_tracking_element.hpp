/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @file mapped_tracking_element.hpp
 * @brief This file contains the class required to track the mmaped access.
 */
#ifndef MAPPED_TRACKING_ELEMENT_HPP
#define MAPPED_TRACKING_ELEMENT_HPP

#include "config.hpp"

#define CSDS_TRACKING_SIZE_BYTES (1024 * 1024 * 1024)

#ifdef CSDS_MMAP_TRACKING
/**
 * @brief Class for tracking the mmap readings.
 */
class MMapTrackingElement {

 public:
  MMapTrackingElement(size_t offset) : mOffset(offset) {
    this->mChrono = high_resolution_clock::now();
  }

  friend std::ostream &operator<<(std::ostream &output,
                                  const MMapTrackingElement &el) {
    int64_t time = GetDeltaTime(el.mChrono);
    output << time << el.mOffset;
    return output;
  }

  /* Chrono for measuring the data access */
  high_resolution_clock::time_point mChrono;

  /* Offset accessed */
  long long mOffset;
};
#endif

#endif  // MAPPED_TRACKING_ELEMENT_HPP
