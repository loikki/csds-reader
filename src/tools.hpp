/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @brief This file contains functions that help to navigate in the logs.
 */
#ifndef CSDS_TOOLS_H
#define CSDS_TOOLS_H

/* Include config */
#include "../config.hpp"

/* Include local headers */
#include "error.hpp"
#include "inline.hpp"
#include "logfile_writer.h"

/**
 * @brief Structure dealing with reading / interpolating a field.
 */
struct csds_reader_field {
  /* Value of the field. */
  void *field;

  /* Value of its first derivative (NULL if not existing). */
  void *first_deriv;

  /* Value of its second derivative (NULL if not existing). */
  void *second_deriv;
};

/** @brief Print a progress bar */
void tools_print_progress(
    float percentage, const std::chrono::high_resolution_clock::time_point init,
    const std::string message);

#endif  // CSDS_TOOLS_H
