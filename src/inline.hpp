/*******************************************************************************
 * This file is part of CSDS and was copied from SWIFT.
 * Copyright (c) 2021 loic.hausammann@epfl.ch
 *               SWIFT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_INLINE_H
#define CSDS_INLINE_H

/* WARNING THIS FILE SHOULD NOT INCLUDE CONFIG.H*/

/**
 * @brief Defines inline
 */
#ifndef INLINE
#ifdef CSDS_NO_INLINE
#define INLINE
#else
#ifdef __cplusplus
#define INLINE __attribute__((always_inline)) inline
#else
#if __GNUC__ && !__GNUC_STDC_INLINE__
#define INLINE __attribute__((always_inline)) extern inline
#else
#define INLINE __attribute__((always_inline)) inline
#endif  // GNU
#endif  // C++
#endif  // CSDS_NO_INLINE
#endif  // INLINE

/**
 * @brief Defines unused
 */
#define ATTR_UNUSED __attribute__((unused))

#endif /* CSDS_INLINE_H */
