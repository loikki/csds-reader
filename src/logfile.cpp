/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#include "logfile.hpp"

#include "mapped_file.hpp"
#include "reader.hpp"

using namespace std;

void LogFile::PopulateTimeArray(const string &basename, int verbose) {
  string filename = basename + "_0000.index";

  /* Try restoring the time array */
  if (mTimes.Load(filename, verbose)) {
    return;
  }

  if (verbose > 0) {
    message("Reading the time array from the logfile.");
  }

  /* get file size. */
  size_t file_size = GetFileSize();

  /* get first timestamp. */
  size_t offset = mHeader.GetOffsetFirstRecord();
  while (offset < file_size) {
    /* read current time record and store it. */
    size_t tmp_offset = offset;
    struct time_record time;
    ReadTimeRecord(time, tmp_offset);
    mTimes.Append(time);

    /* get next record. */
    bool next_present = GetNextRecord(offset);
    if (!next_present) break;
  }
}

LogFile::LogFile(const string basename, bool only_header, int verbose)
    : MappedFile(basename + ".dump", /* read_only */ true,
                 /* track_mmap */ true) {

  /* Read the header. */
  if (verbose > 1) message("Reading the header.");
  mHeader.Read(*this, verbose);

  /* Print the header. */
  if (verbose > 0) {
    mHeader.Print();
  }

  /* No need to continue if only the
     header is required. */
  if (only_header) return;

  /* Check if the offset are corrupted. */
  if (mHeader.OffsetsAreCorrupted()) {
    csds_error("The offsets have been corrupted.");
  }

  /* Reverse the offsets direction. */
  if (mHeader.OffsetsAreBackward()) {
    string logfile_name = basename + ".dump";
    ReverseOffset(logfile_name, verbose);
  }

  /* Initialize the time array. */
  if (verbose > 1) message("Reading the timestamps.");
  PopulateTimeArray(basename, verbose);

  /* Print the time array. */
  if (verbose > 0) {
    mTimes.Print();
  }
}

void LogFile::CheckRecordConsistency(ATTR_UNUSED int verbose) {
#ifdef CSDS_DEBUG_CHECKS
  const Header &header = GetHeader();

  if (verbose > 0) {
    message("Check record's headers...");
  }

  /* Prepare the progress bar. */
  float next_percentage = 0.;
  const float delta_percentage = 0.5;
  const high_resolution_clock::time_point init = high_resolution_clock::now();

  /* check that the record offset points to another record. */
  for (size_t offset_debug = header.GetOffsetFirstRecord();
       offset_debug < GetFileSize();
       offset_debug = CheckCurrentRecordConsistency(offset_debug)) {

    /* Check if we should update the progress bar. */
    float current = 100 * ((float)offset_debug) / GetFileSize();
    if (verbose > 0 && current > next_percentage) {
      /* Print the bar */
      tools_print_progress(current, init, "Checking offsets");

      /* Compute the next update of the progress bar */
      next_percentage += delta_percentage;
    }
  }

  /* Print final message and close progress bar */
  if (verbose > 0) {
    cout << std::endl;
    message("Record's headers are correct.");
  }

#else
  csds_error(
      "This function should not be called outside of the debugging checks");
#endif
}

void LogFile::ReverseOffset(std::string filename, int verbose) {

  /* Close and reopen the file in write mode. */
  Close();
  Open(filename, /* read_only */ false, /* track_mmap */ false);

  /* Check if the offsets need to be reversed. */
  if (!mHeader.OffsetsAreBackward()) {
    csds_error("The offsets are already reversed.");
  }

#ifdef CSDS_DEBUG_CHECKS
  CheckRecordConsistency(verbose);
#endif

  message("WARNING: Modifying the logfile, do not kill the job!");

  /* Set the offset direction to a corrupted status. */
  mHeader.WriteOffsetDirection(csds_offset_corrupted, *this);

  if (verbose > 0) {
    message("Reversing offsets...");
  }

  /* Prepare the progress message. */
  float next_percentage = 0.;
  const float delta_percentage = 0.2;
  const high_resolution_clock::time_point init = high_resolution_clock::now();

  /* reverse the record's offset. */
  for (size_t offset = mHeader.GetOffsetFirstRecord(); offset < GetFileSize();
       offset = ReverseCurrentOffset(offset)) {
    /* Check if we should update the progress. */
    float current = 100 * ((float)offset) / GetFileSize();
    if (verbose > 0 && current > next_percentage) {

      /* Print the remaining time */
      tools_print_progress(current, init, "Reversing the offsets");

      /* Compute the next update of the progress */
      next_percentage += delta_percentage;
    }
  }

  /* Print final message */
  if (verbose > 0) {
    cout << endl;
    message("Reversing done.");
  }

  /* Now that the offset are effectively reversed, can set the direction to
     forward. */
  mHeader.WriteOffsetDirection(csds_offset_forward, *this);

  message("WARNING: Modification done, you can now safely kill the job.");

#ifdef CSDS_DEBUG_CHECKS
  CheckRecordConsistency(verbose);
#endif

  /* Close and reopen the file in read only mode. */
  Close();
  Open(filename, /* read_only */ true, /* track_mmap */ true);
}

bool LogFile::GetNextRecord(size_t &offset) {
  if (mHeader.OffsetsAreForward()) return GetNextRecordForward(offset);
  if (mHeader.OffsetsAreBackward())
    return GetNextRecordBackward(offset);
  else
    csds_error("Offsets are corrupted.");
}

bool LogFile::GetNextRecordForward(size_t &offset) {

  /* Read the offset. */
  struct record_header header;
  ReadRecordHeader(offset, header);

  if (header.offset == 0) return false;

  /* Set the absolute offset. */
  offset += header.offset;
  return true;
}

bool LogFile::GetNextRecordBackward(size_t &offset) {
#ifndef CSDS_DEBUG_CHECKS
  csds_error("Should not be used, method too slow");
#endif
  size_t current_offset = offset;
  size_t record_header = CSDS_MASK_SIZE + CSDS_OFFSET_SIZE;

  while (current_offset < mMapSize) {
    struct record_header header;
    ReadRecordHeader(current_offset, header);

    header.offset = current_offset - header.offset - record_header;
    if (offset == header.offset) {
      offset = current_offset - record_header;
      return true;
    }

    current_offset += mHeader.GetRecordSizeFromMask(header.mask);
  }

  return false;
}

size_t LogFile::ReverseCurrentOffset(size_t offset) {
  const size_t cur_offset = offset;

  /* read mask + offset. */
  struct record_header header;
  offset = ReadRecordHeader(offset, header);

  /* write offset of zero (in case it is the last record). */
  const size_t zero = 0;
  offset -= CSDS_OFFSET_SIZE;
  offset = WriteData(offset, CSDS_OFFSET_SIZE, &zero);

  /* set offset after current record. */
  offset += mHeader.GetRecordSizeFromMask(header.mask);
  const size_t after_current_record = offset;

  /* first records do not have a previous partner. */
  if (header.offset == cur_offset) return after_current_record;
  if (header.offset > cur_offset)
    csds_error("Unexpected offset: header " << header.offset << ", current "
                                            << cur_offset);

  /* modify previous offset. */
  offset = cur_offset - header.offset + CSDS_MASK_SIZE;
  offset = WriteData(offset, CSDS_OFFSET_SIZE, &header.offset);

#ifdef CSDS_DEBUG_CHECKS
  struct record_header prev_header;
  offset -= CSDS_MASK_SIZE + CSDS_OFFSET_SIZE;
  ReadRecordHeader(offset, prev_header);

  /* Check if we are not mixing timestamp and particles */
  if ((prev_header.mask != CSDS_TIMESTAMP_MASK &&
       header.mask == CSDS_TIMESTAMP_MASK) ||
      (prev_header.mask == CSDS_TIMESTAMP_MASK &&
       header.mask != CSDS_TIMESTAMP_MASK))
    csds_error("Unexpected mask: " << header.mask << " got "
                                   << prev_header.mask);

#endif  // CSDS_DEBUG_CHECKS

  return after_current_record;
}

size_t LogFile::CheckCurrentRecordConsistency(size_t offset) {
#ifndef CSDS_DEBUG_CHECKS
  csds_error("Should not check in non debug mode.");
#endif

  const size_t init_offset = offset;

  /* read mask + offset. */
  struct record_header header;
  offset = ReadRecordHeader(offset, header);

  /* set offset after current record. */
  offset += mHeader.GetRecordSizeFromMask(header.mask);
  const size_t offset_ret = offset;

  /* If something happened, skip the check. */
  if (header.mask & CSDS_SPECIAL_FLAGS_MASK) {
    return offset_ret;
  }

  /* get absolute offset. */
  if (mHeader.OffsetsAreForward())
    header.offset += init_offset;
  else if (mHeader.OffsetsAreBackward()) {
    if (init_offset < header.offset)
      csds_error("Offset too large for mask: " << header.mask);
    header.offset = init_offset - header.offset;
  } else {
    csds_error("Offset are corrupted.");
  }

  if (header.offset == init_offset || header.offset == 0) return offset_ret;

  /* read mask of the pointed record. */
  struct record_header pointed_header;
  ReadRecordHeader(header.offset, pointed_header);

  /* check if not mixing timestamp and particles. */
  if ((pointed_header.mask != CSDS_TIMESTAMP_MASK &&
       header.mask == CSDS_TIMESTAMP_MASK) ||
      (pointed_header.mask == CSDS_TIMESTAMP_MASK &&
       header.mask != CSDS_TIMESTAMP_MASK))
    csds_error("Error in the offset for mask: " << header.mask);

  return offset_ret;
}
