/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#ifndef CSDS_INDEX_H
#define CSDS_INDEX_H

/* Standard includes */
#include <algorithm>
#include <array>

/* Local includes */
#include "mapped_file.hpp"
#include "tools.hpp"

/* Define the place and size of each element in the header. */
// TODO could cleanup a bit this
/* The time in double precision. */
#define csds_index_time_offset 0
#define csds_index_time_size sizeof(double)
/* The time on the integer timeline. */
#define csds_index_integer_time_offset \
  csds_index_time_offset + csds_index_time_size
#define csds_index_integer_time_size sizeof(integertime_t)
/* The number of particle (for each type). */
#define csds_index_npart_offset \
  csds_index_integer_time_offset + csds_index_integer_time_size
#define csds_index_npart_size sizeof(uint64_t) * csds_type_count
/* The flag used for checking if the file is sorted or not. */
#define csds_index_is_sorted_offset \
  csds_index_npart_offset + csds_index_npart_size
#define csds_index_is_sorted_size sizeof(char)
/* The array containing the offset and ids. Rounding to a multiplier of 8 in
 * order to align the memory */
#define csds_index_data_offset \
  ((csds_index_is_sorted_offset + csds_index_is_sorted_size + 7) & ~7)

/**
 * @brief Class dealing with a single index file.
 */
class IndexFile : public MappedFile {
 public:
  IndexFile(const std::string filename, bool sorted, int verbose);
  ~IndexFile();

  bool ContainsTimeArray();

  /**
   * @brief Give the offset of a particle given its id.
   *
   * @param id The ID of the particle.
   * @param type The type of particles.
   * @param offset (output) The offset of the particle.
   *
   * @return Did we find the particle?
   */
  INLINE bool GetOffset(int64_t id, enum part_type type,
                        uint64_t &offset) const;
  /**
   * @brief Get the #index_data of a particle type.
   *
   * This function should be used when looking for the last known particles.
   *
   * @param type The particle type.
   */
  // TODO convert the 3 functions into particle accessor (e.g. return const&)
  INLINE struct index_data *GetData(enum part_type type) const;

  /**
   * @brief Get the #index_data for the particles created.
   *
   * This function should be used when looking for the particles created.
   *
   * @param type The particle type.
   */
  INLINE struct index_data *GetCreatedHistory(enum part_type type) const;

  /**
   * @brief Get the #index_data for the particles removed.
   *
   * The #index_data contains the ids and offset of the particle.
   * This function should be used when looking for the particles removed.
   *
   * @param type The particle type.
   */
  INLINE struct index_data *GetRemovedHistory(enum part_type type) const;

  /* Getters and setters */
  double GetTime() const { return mTime; }
  integertime_t GetIntegerTime() const { return mIntegerTime; }
  uint64_t GetNumberParticles(enum part_type type) const {
    return mNParts[type];
  }
  uint64_t GetNumberCreatedParticles(enum part_type type) const {
    return mNPartsCreated[type];
  }
  uint64_t GetNumberRemovedParticles(enum part_type type) const {
    return mNPartsRemoved[type];
  }
  uint64_t GetCurrentNumberCreatedParticles(enum part_type type,
                                            size_t offset) const {

    /* Get the history of created particles. */
    struct index_data *data = GetCreatedHistory(type);
    uint64_t nparts = GetNumberCreatedParticles(type);

    /* Do we have any new particle? */
    if (GetNumberCreatedParticles(type) == 0 || offset < data[0].offset) {
      return 0;
    }

    /* Find the correct record */
    struct index_data value = {.id = 0, .offset = offset};
    struct index_data *it =
        std::lower_bound(data, data + nparts, value, CompareIndexDataOffset);
    size_t ret = it - data;

    /* lower_bound: Returns an iterator pointing to the first
       element in the range [first,last) which does
       not compare less than val.
       Therefore we need to decrease it in order to have the last value before
       val
    */
    if (offset != it->offset) {
      ret--;
    }

#ifdef CSDS_DEBUG_CHECKS
    /* Check if the binary search is correct. */
    if (data[ret].offset > offset) {
      csds_error("Failed to obtain the number of new particles.");
    }
    if (ret != nparts - 1 && data[ret + 1].offset < offset) {
      csds_error("Failed to obtain the number of removed particles");
    }
#endif

    /* We need the count, not the index. */
    return ret + 1;
  }

  uint64_t GetCurrentNumberRemovedParticles(enum part_type type,
                                            size_t offset) const {

    /* Get the history of created particles. */
    struct index_data *data = GetRemovedHistory(type);
    const uint64_t nparts = GetNumberRemovedParticles(type);

    /* Do we have any new particle? */
    if (GetNumberRemovedParticles(type) == 0 || offset < data[0].offset) {
      return 0;
    }

    /* Get the last created particle */
    struct index_data value = {.id = 0, .offset = offset};
    struct index_data *it =
        std::lower_bound(data, data + nparts, value, CompareIndexDataOffset);
    size_t ret = it - data;

    /* lower_bound: Returns an iterator pointing to the first
       element in the range [first,last) which does
       not compare less than val.
       Therefore we need to decrease it in order to have the last value before
       val
     */
    if (offset != it->offset) {
      ret--;
    }

    /* Check if the binary search is correct. */
    if (data[ret].offset > offset) {
      csds_error("Failed to obtain the number of removed particles.");
    }
    if (ret != nparts - 1 && data[ret + 1].offset < offset) {
      csds_error("Failed to obtain the number of removed particles");
    }

    /* We need the count, not the index. */
    return ret + 1;
  }

 protected:
  static bool CompareIndexDataOffset(const struct index_data &d1,
                                     const struct index_data &d2) {
    return d1.offset < d2.offset;
  }
  static bool CompareIndexDataId(const struct index_data &d1,
                                 const struct index_data &d2) {
    return d1.id < d2.id;
  }

  /** @brief Sort the file according to the IDs */
  void SortFile(const std::string filename, int verbose);
  /** @brief Write within the index file that the arrays are sorted. */
  void WriteSorted();

  /* Time of the index file */
  double mTime;

  /* Integer time of the index file */
  // TODO remove
  integertime_t mIntegerTime;

  /* Number of particles in the file */
  std::array<uint64_t, csds_type_count> mNParts;

  /* Is the file sorted ? */
  bool mIsSorted;

  /* Number of particles created. */
  std::array<uint64_t, csds_type_count> mNPartsCreated;

  /* Number of particles removed. */
  std::array<uint64_t, csds_type_count> mNPartsRemoved;
};

/**
 * @brief Give the offset of a particle given its id.
 *
 * @param id The ID of the particle.
 * @param type The type of particles.
 * @param offset (output) The offset of the particle.
 *
 * @return Did we find the particle?
 */
inline bool IndexFile::GetOffset(int64_t id, enum part_type type,
                                 uint64_t &offset) const {

#ifdef CSDS_DEBUG_CHECKS
  /* Ensure that the file is sorted according to the ids. */
  if (!mIsSorted) {
    csds_error("The index file should be already sorted.");
  }
  /* Ensure that we have some particles */
  if (mNParts[type] == 0) {
    csds_error("Trying to find a particle in an empty index.");
  }
#endif

  /* Get the data structure. */
  struct index_data *data = GetData(type);

  /* Search for the value (binary search) */
  struct index_data value = {.id = id, .offset = 0};
  struct index_data *it =
      std::lower_bound(data, data + mNParts[type], value, CompareIndexDataId);

  /* Check if we found the particle */
  if (it->id == id) {
    offset = it->offset;
    return true;
  }

  /* Try to find it in the history */
  // TODO Improve this with a sort?
  data = GetCreatedHistory(type);
  for (uint64_t i = 0; i < mNPartsCreated[type]; i++) {
    if (data[i].id == id) {
      offset = data[i].offset;
      return true;
    }
  }

  /* The particle was not found */
  return false;
}

inline struct index_data *IndexFile::GetData(enum part_type type) const {
  /* Get the offset of particles of this type by skipping entries of lower
   * types. */
  size_t count = 0;
  for (int i = 0; i < type; i++) {
    count += mNParts[i];
  }
  const size_t type_offset = count * sizeof(struct index_data);

  const char *ret = mMap + csds_index_data_offset + type_offset;
  return (struct index_data *)ret;
}

inline struct index_data *IndexFile::GetCreatedHistory(
    enum part_type type) const {

  /* Get the position after the previous array. */
  char *ret = (char *)GetData(csds_type_count);

  /* Get the offset of particles of this type by skipping entries of lower
   * types. */
  size_t count = 0;
  for (int i = 0; i < type; i++) {
    count += mNPartsCreated[i];
  }
  const size_t type_offset = count * sizeof(struct index_data);

  ret += type_offset + csds_index_npart_size;
  return (struct index_data *)ret;
}

inline struct index_data *IndexFile::GetRemovedHistory(
    enum part_type type) const {

  /* Get the position after the previous array. */
  char *ret = (char *)GetCreatedHistory(csds_type_count);

  /* Get the offset of particles of this type by skipping entries of lower
   * types. */
  size_t count = 0;
  for (int i = 0; i < type; i++) {
    count += mNPartsRemoved[i];
  }
  const size_t type_offset = count * sizeof(struct index_data);

  ret += type_offset + csds_index_npart_size;
  return (struct index_data *)ret;
}

#endif  // CSDS_INDEX_H
