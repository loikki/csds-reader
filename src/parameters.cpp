/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include corresponding header */
#include "parameters.hpp"

/* Include the csds */
#include "parser.hpp"
#include "reader.hpp"
#include "tools.hpp"

using namespace std;

Parameters::Parameters(const string filename, int verbose) {

  /* Initialize the parser */
  Parser parser(filename);

  /* Print the parameters */
  if (verbose > 1) parser.PrintParams();

  /* Read the number of dimension */
  mDimension = parser.GetParam<int>("Header:Dimension");

  /* Read the box size */
  double box_size[3];
  parser.GetParamArray<double, 3>("Header:BoxSize", box_size);
  for (int i = 0; i < 3; i++) {
    mBoxSize[i] = box_size[i];
  }

  /* Read the periodicity */
  mPeriodic = parser.GetParam<int>("Header:Periodic");

  /* Read if we are running with cosmology */
  mWithCosmology = parser.GetParam<int>("Policy:cosmological integration");

  /* Initialize the cosmology */
  if (mWithCosmology) mCosmo = Cosmology(parser);

  /* Read the initial number of particles. */
  for (int i = 0; i < csds_type_count; i++) {
    mApproximateNumberParticles[i] = 0;
  }
  mApproximateNumberParticles[csds_type_gas] =
      parser.GetParam<long long>("Header:NumberParts");
  mApproximateNumberParticles[csds_type_stars] =
      parser.GetParam<long long>("Header:NumberSParts");
  mApproximateNumberParticles[csds_type_dark_matter] =
      parser.GetParam<long long>("Header:NumberGParts");

  /* Read the cache parameters */
  mCacheOverAllocation = parser.GetOptParam<float>("Cache:OverAllocation", 1.2);

  long long init_alloc[csds_type_count];
  for (int i = 0; i < csds_type_count; i++) {
    init_alloc[i] = 0;
  }
  parser.GetOptParamArray<long long, csds_type_count>("Cache:InitialAllocation",
                                                      init_alloc);
  for (int i = 0; i < csds_type_count; i++) {
    mCacheInitSize[i] = init_alloc[i];
  }
}
