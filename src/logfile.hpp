/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/**
 * @file csds_logfile.h
 * @brief This file contains the high level function for the log.
 */
#ifndef CSDS_LOGFILE_H
#define CSDS_LOGFILE_H

#include "header.hpp"
#include "mapped_file.hpp"
#include "time_array.hpp"

/**
 * @brief This class deals with the log file.
 */
class LogFile : public MappedFile {
 public:
  /**
   * @brief Initialize the #LogFile.
   *
   * If required this function will also reverse the offsets.
   * @param basename The basename.
   * @param verbose The verbose level.
   * @param only_header Read only the header.
   */
  LogFile(const std::string basename, bool only_header, int verbose);

  /**
   * @brief read a time record.
   *
   * @param time_record (output) The time read.
   * @param offset The offset to read
   *
   * @return The offset after the time record
   */
  size_t ReadTimeRecord(struct time_record &time_record, size_t offset);

  /**
   * @brief read a record header.
   *
   * @param offset The offset in the file
   * @param header (output) header read from the file.
   *
   * @return The offset after the record header.
   */
  size_t ReadRecordHeader(size_t offset, record_header &header);

  /**
   * @brief get the offset of the next corresponding record.
   * @param offset In: initial offset, Out: offset of the next record
   * @return Is a next record present?
   */
  bool GetNextRecord(size_t &offset);

  /** @brief Save the time array into an index file given by filename */
  void SaveTimeArray(std::string filename) const { mTimes.Save(filename); }

  /* Getters and setters */
  TimeArray const &GetTimeArray() const { return mTimes; }
  Header const &GetHeader() const { return mHeader; }

 protected:
  /**
   * @brief Initialize a time array from the logfile.
   *
   * @param basename The basename of the files.
   * @param verbose The verbose level
   */
  void PopulateTimeArray(const std::string &basename, int verbose);

  /**
   * @brief Get the next record pointed by the offset within the header.
   * Works only when the offsets are pointing backward.
   * This is really slow and should not be used.
   * @param offset (Out) offset of the next record
   * @return Is a next record present?
   */
  bool GetNextRecordBackward(size_t &offset);
  /**
   * @brief Get the next record pointed by the offset within the header.
   * Works only when the offsets are pointing forward
   * @param offset (Out) offset of the next record
   * @return Is a next record present?
   */
  bool GetNextRecordForward(size_t &offset);

  /**
   * @brief Reverse offset in log file
   *
   * @param filename The log's filename.
   * @param verbose The verbosity level
   */
  void ReverseOffset(std::string filename, int verbose);

  /**
   * @brief switch side offset.
   *
   * From current record, switch side of the offset of the previous one.
   * @param offset position of the record.
   *
   * @return position after the record.
   */
  size_t ReverseCurrentOffset(size_t offset);

  /**
   * @brief debugging function checking the offset and the mask of all the
   * records.
   * Compare the mask with the one pointed by the header.
   * if the record is a particle, check the id too.
   * @param verbose The verbose level
   */
  void CheckRecordConsistency(int verbose);
  /**
   * @brief debugging function checking the offset and the mask of a record.
   *
   * Compare the mask with the one pointed by the header.
   * if the record is a particle, check the id too.
   *
   * @param offset position of the record.
   *
   * @return position after the record.
   */
  size_t CheckCurrentRecordConsistency(size_t offset);

  /* Information contained in the file header. */
  Header mHeader;

  /* Information about the time records. */
  TimeArray mTimes;
};

inline size_t LogFile::ReadTimeRecord(struct time_record &time_record,
                                      size_t offset) {

  /* Initialize variables. */
  time_record.int_time = 0;
  time_record.time = 0;
  time_record.offset = offset;

  /* read record header. */
  struct record_header header;
  offset = ReadRecordHeader(offset, header);

  /* check if reading a time record. */
  if (CSDS_TIMESTAMP_MASK != header.mask) csds_error("Not a time record.");

  /* read the record. */
  offset =
      ReadData(offset, sizeof(unsigned long long int), &time_record.int_time);
  offset = ReadData(offset, sizeof(double), &time_record.time);

  return offset;
}

inline size_t LogFile::ReadRecordHeader(size_t offset, record_header &header) {
#ifdef CSDS_MMAP_TRACKING
  MMapTrackingElement el(offset);
#endif

  /* read mask */
  header.mask = 0;
  memcpy(&header.mask, this->mMap + offset, CSDS_MASK_SIZE);
  offset += CSDS_MASK_SIZE;

  /* read offset */
  header.offset = 0;
  memcpy(&header.offset, this->mMap + offset, CSDS_OFFSET_SIZE);
  offset += CSDS_OFFSET_SIZE;

#ifdef CSDS_MMAP_TRACKING
  /* Write the result into the file */
  if (this->mTracking.use_tracking) {
    this->WriteTracking(el);
  }
#endif

  return offset;
}

#endif  // CSDS_LOGFILE_H
