/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_HEADER_H
#define CSDS_HEADER_H

/* Standard library */
#include <array>
#include <stdio.h>
#include <stdlib.h>

/* Local headers */
#include "field.hpp"
#include "particle_type.hpp"
#include "tools.hpp"

/* Forward declaration */
class LogFile;

/* Quick typedef as a shortcut in this file */
typedef std::array<std::vector<Field>, csds_type_count> FieldArray;

/**
 * @brief This structure contains everything from the file header.
 * This structure is initialized by #Read.
 */
class Header {
 public:
  /** @brief Print the header. */
  void Print() const;

  /** @brief Read the header from the #LogFile
   * @param log The #LogFile.
   * @param verbose The verbosity level
   */
  void Read(LogFile &log, int verbose);

  /** @brief Compute the size of a record from its mask
   * @param mask The mask to use.
   */
  size_t GetRecordSizeFromMask(mask_type mask) const;

  /** @brief Write the new offset direction into the logfile.
   * @param new_value The new value for the direction.
   * @param log The #LogFile.
   */
  void WriteOffsetDirection(enum csds_offset_direction new_value, LogFile &log);

  /** @brief Get a field from its name and the particle type.
   * @param name The name of the field.
   * @param part_type The type of particle.
   */
  Field const &GetFieldFromName(const std::string name,
                                enum part_type part_type) const;

  /** @brief Check if the offsets are forward. */
  INLINE bool OffsetsAreForward() const {
    return mOffsetDirection == csds_offset_forward;
  }

  /** @brief Check if the offset are backward. */
  INLINE bool OffsetsAreBackward() const {
    return mOffsetDirection == csds_offset_backward;
  }

  /** @brief Check if the offset are corrupted. */
  INLINE bool OffsetsAreCorrupted() const {
    return mOffsetDirection == csds_offset_corrupted;
  }

  /** @brief Returns the array of all the fields defined in the header */
  FieldArray const &GetFields() const { return mFields; }

  /**
   * @brief Get the offset of first time record
   */
  size_t GetOffsetFirstRecord() const { return mOffsetFirstRecord; }

  int GetMajorVersion() const { return mMajorVersion; }
  int GetMinorVersion() const { return mMinorVersion; }
  enum csds_offset_direction GetOffsetDirection() const {
    return mOffsetDirection;
  }

 protected:
  /* The major version. */
  int mMajorVersion;

  /* The minor version. */
  int mMinorVersion;

  /* Offset of the first record. */
  size_t mOffsetFirstRecord;

  /* Direction of the offset in the records. */
  enum csds_offset_direction mOffsetDirection;

  /* The fields for each particle type in the correct order.
     By construction, the special flag is the first element in each array. */
  FieldArray mFields;

  /* The name corresponding to the different offset directions  */
  static const std::string sOffsetName[csds_offset_count];
};

#endif  // CSDS_HEADER_H
