/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2020 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#ifndef CSDS_CSDS_INTERPOLATION_H
#define CSDS_CSDS_INTERPOLATION_H

#include "parameters.hpp"
#include "tools.hpp"

/**
 * @brief Limits the value of x to be between a and b
 *
 * Only wraps once. If x > a + 2(b - a), the returned value will be larger than
 * b. Similarly for x < a - (b - a), the value will be smaller than a.
 */
#define box_wrap(x, a, b)                                              \
  ({                                                                   \
    const __typeof__(x) _x = (x);                                      \
    const __typeof__(a) _a = (a);                                      \
    const __typeof__(b) _b = (b);                                      \
    _x < _a ? (_x + (_b - _a)) : ((_x >= _b) ? (_x - (_b - _a)) : _x); \
  })

/**
 * @brief Compute the quintic hermite spline interpolation.
 * See https://en.wikipedia.org/wiki/Hermite_interpolation for more information.
 * @f$ p(x) = f(x_0) + f'(x_0) (x - x_0) + \frac{1}{2}f''(x_0) (x - x_0)^2 +
 \frac{f(x_1) - f(x_0) - f'(x_0) (x_1 - x_0) - \frac{1}{2} f''(x_0) (x_1 -
 x_0)^2}{(x_1 - x_0)^3} (x - x_0)^3
  + \frac{3 f(x_0) - 3 f(x_1) + \left( 2 f'(x_0) + f'(x_1) \right) (x_1 - x_0) +
 \frac{1}{2} f''(x_0) (x_1 - x_0)^2}{(x_1 - x_0)^4} (x - x_0)^3 (x - x_1)
  + \frac{6 f(x_1) - 6 f(x_0) - 3 \left( f'(x_0) + f'(x_1) \right) (x_1 - x_0) +
 \frac{1}{2}\left( f''(x_1) - f''(x_0) \right) (x_1 - x_0)^2}{(x_1 - x_0)^5} (x
 - x_0)^3 (x - x_1)^2 @f$
 *
 * @param t0 The time at the left of the interval.
 * @param x0 The function at the left of the interval.
 * @param v0 The first derivative at the left of the interval.
 * @param a0 The second derivative at the left of the interval.
 * @param t1 The time at the right of the interval.
 * @param x1 The function at the right of the interval.
 * @param v1 The first derivative at the right of the interval.
 * @param a1 The second derivative at the right of the interval.
 * @param t The time of the interpolation.
 *
 * @return The function evaluated at t.
 */
template <class TypeField, class TypeFirst, class TypeSecond>
INLINE static TypeField interpolate_quintic_hermite_spline(
    const TypeField t0, const TypeField x0, const TypeFirst v0,
    const TypeSecond a0, const TypeField t1, const TypeField x1,
    const TypeFirst v1, const TypeSecond a1, const TypeField t) {

  /* Generates recurring variables  */
  /* Time differences */
  const TypeField dt = t1 - t0;
  const TypeField dt2 = dt * dt;
  const TypeField dt3 = dt2 * dt;
  const TypeField dt4 = dt3 * dt;
  const TypeField dt5 = dt4 * dt;

  const TypeField t_t0 = t - t0;
  const TypeField t_t0_2 = t_t0 * t_t0;
  const TypeField t_t0_3 = t_t0_2 * t_t0;
  const TypeField t_t1 = t - t1;
  const TypeField t_t1_2 = t_t1 * t_t1;

  /* Derivatives */
  const TypeField v0_dt = v0 * dt;
  const TypeField a0_dt2 = 0.5 * a0 * dt2;
  const TypeField v1_dt = v1 * dt;
  const TypeField a1_dt2 = 0.5 * a1 * dt2;

  /* Do the first 3 terms of the hermite spline */
  TypeField x = x0 + v0 * t_t0 + 0.5 * a0 * t_t0_2;

  /* Cubic term */
  x += (x1 - x0 - v0_dt - a0_dt2) * t_t0_3 / dt3;

  /* Quartic term */
  x += (3. * x0 - 3. * x1 + v1_dt + 2. * v0_dt + a0_dt2) * t_t0_3 * t_t1 / dt4;

  /* Quintic term */
  x += (6. * x1 - 6. * x0 - 3. * v0_dt - 3. * v1_dt + a1_dt2 - a0_dt2) *
       t_t0_3 * t_t1_2 / dt5;

  return x;
}

/**
 * @brief Compute the cubic hermite spline interpolation.
 * See https://en.wikipedia.org/wiki/Hermite_interpolation for more information.
 * @f$ p(x) = f(x_0) + f'(x_0) (x - x_0) + \frac{f(x_1) - f(x_0) - f'(x_0) (x_1
 - x_0)}{(x_1 - x_0)^2} (x - x_0)^2
 + \frac{2 f(x_0) - 2 f(x_1) + f'(x_0) (x_1 - x_0) + f'(x_1) (x_1 - x_0)}{(x_1
 - x_0)^3} (x - x_0)^2 (x - x_1) @f$
 *
 * @param t0 The time at the left of the interval.
 * @param v0 The first derivative at the left of the interval.
 * @param a0 The second derivative at the left of the interval.
 * @param t1 The time at the right of the interval.
 * @param v1 The first derivative at the right of the interval.
 * @param a1 The second derivative at the right of the interval.
 * @param t The time of the interpolation.
 *
 * @return The function evaluated at t.
 */
template <class TypeField, class TypeFirst>
INLINE static TypeField interpolate_cubic_hermite_spline(
    const TypeField t0, const TypeField v0, const TypeFirst a0,
    const TypeField t1, const TypeField v1, const TypeFirst a1,
    const TypeField t) {

  /* Generates recurring variables  */
  /* Time differences */
  const TypeField dt = t1 - t0;
  const TypeField dt2 = dt * dt;
  const TypeField dt3 = dt2 * dt;

  const TypeField t_t0 = t - t0;
  const TypeField t_t0_2 = t_t0 * t_t0;
  const TypeField t_t1 = t - t1;
  /* Derivatives */
  const TypeField a0_dt = a0 * dt;
  const TypeField a1_dt = a1 * dt;

  /* Do the first 2 terms of the hermite spline */
  TypeField x = v0 + a0 * t_t0;

  /* Square term */
  x += (v1 - v0 - a0_dt) * t_t0_2 / dt2;

  /* Cubic term */
  x += (2. * v0 - 2. * v1 + a1_dt + a0_dt) * t_t0_2 * t_t1 / dt3;

  return x;
}

/**
 * @brief Compute the linear interpolation.
 *
 * @param t0 The time at the left of the interval.
 * @param v0 The first derivative at the left of the interval.
 * @param t1 The time at the right of the interval.
 * @param v1 The first derivative at the right of the interval.
 * @param t The time of the interpolation.
 *
 * @return The function evaluated at t.
 */
template <class TypeField>
INLINE static TypeField interpolate_linear_spline(const TypeField t0,
                                                  const TypeField v0,
                                                  const TypeField t1,
                                                  const TypeField v1,
                                                  const TypeField t) {

  const TypeField wa = (t - t0) / (t1 - t0);
  const TypeField wb = 1. - wa;
  return wa * v1 + wb * v0;
}

/**
 * @brief Interpolates a field in N dimension using the first and second
 * derivatives if possible.
 *
 * The field is in double and both derivatives in float.
 *
 * @param before Pointer to the #csds_reader_field at a time < t.
 * @param after Pointer to the #csds_reader_field at a time > t.
 * @param otuput Pointer to the output value.
 * @param t_before Time of field_before (< t).
 * @param t_after Time of field_after (> t).
 * @param t Requested time.
 * @param periodic Should the periodic boundary be applied?
 * @param params The simulation's #Parameters.
 */
template <class TypeField, class TypeFirst, class TypeSecond>
INLINE static void interpolate_quintic(const TypeField t_before,
                                       const struct csds_reader_field &before,
                                       const TypeField t_after,
                                       const struct csds_reader_field &after,
                                       void *__restrict__ output,
                                       const TypeField t, const int dimension,
                                       int periodic, const Parameters &params) {

  /* Get the arrays */
  const TypeField *x_bef = (TypeField *)before.field;
  const TypeFirst *v_bef = (TypeFirst *)before.first_deriv;
  const TypeSecond *a_bef = (TypeSecond *)before.second_deriv;

  const TypeField *x_aft = (TypeField *)after.field;
  const TypeFirst *v_aft = (TypeFirst *)after.first_deriv;
  const TypeSecond *a_aft = (TypeSecond *)after.second_deriv;

  TypeField *x = (TypeField *)output;

  /* Loop over the dimensions */
  for (int i = 0; i < dimension; i++) {
    /* Get the current variables */
    TypeField x0 = x_bef[i];
    TypeField x1 = x_aft[i];
    TypeFirst v0 = v_bef ? v_bef[i] : -1;
    TypeFirst v1 = v_aft ? v_aft[i] : -1;
    TypeSecond a0 = a_bef ? a_bef[i] : -1;
    TypeSecond a1 = a_aft ? a_aft[i] : -1;

    /* Add cosmological factor for velocity and acceleration */
    if (params.IsWithCosmology() && v_bef && v_aft) {
      params.GetCosmology().AddFactorPosition<TypeFirst, TypeSecond>(
          t_before, &v0, a_bef ? &a0 : NULL);
      params.GetCosmology().AddFactorPosition<TypeFirst, TypeSecond>(
          t_after, &v1, a_aft ? &a1 : NULL);
    }

    /* Apply the periodic boundaries */
    if (periodic) {
      /* Move towards the origin for more accuracy */
      const TypeField half = 0.5 * params.GetBoxSize()[i];
      if (x0 > x1 + half)
        x0 -= params.GetBoxSize()[i];
      else if (x1 > x0 + half)
        x1 -= params.GetBoxSize()[i];
    }

    /* Use quintic hermite spline. */
    if (v_bef && v_aft && a_bef && a_aft) {
      x[i] =
          interpolate_quintic_hermite_spline<TypeField, TypeFirst, TypeSecond>(
              t_before, x0, v0, a0, t_after, x1, v1, a1, t);
    }
    /* Use cubic hermite spline. */
    else if (v_bef && v_aft) {
      x[i] = interpolate_cubic_hermite_spline<TypeField, TypeFirst>(
          t_before, x0, v0, t_after, x1, v1, t);
    }
    /* Use linear interpolation. */
    else {
      const TypeField wa = (t - t_before) / (t_after - t_before);
      const TypeField wb = 1. - wa;
      x[i] = wa * x1 + wb * x0;
    }

    /* Apply the periodic boundaries to the output */
    if (periodic) {
      /* We suppose that only 1 wrapping is enough
         otherwise something really bad happened */
      x[i] = box_wrap(x[i], 0., params.GetBoxSize()[i]);

#ifdef CSDS_DEBUG_CHECKS
      /* Check if the periodic box is correctly applied */
      if (x[i] >= params.GetBoxSize()[i] || x[i] < 0) {
        csds_error("A particle is outside the periodic box "
                   << params.GetBoxSize()[i] << ": before=" << x0
                   << ", after=" << x1 << ", interpolation=" << x[i]);
      }
#endif
    }
  }
}

/**
 * @brief Interpolates a field in N dimension using the first derivative if
 * possible.
 *
 * The field and the first derivatives are floats.
 *
 * @param before Pointer to the #csds_reader_field at a time < t.
 * @param after Pointer to the #csds_reader_field at a time > t.
 * @param otuput Pointer to the output value.
 * @param t_before Time of field_before (< t).
 * @param t_after Time of field_after (> t).
 * @param t Requested time.
 * @param periodic Should the periodic boundary be applied?
 * @param params The simulation's #csds_parameters.
 */
template <class TypeField, class TypeFirst>
INLINE static void interpolate_cubic(
    const TypeField t_before, const struct csds_reader_field &before,
    const TypeField t_after, const struct csds_reader_field &after,
    void *__restrict__ output, const TypeField t, const int dimension,
    ATTR_UNUSED int periodic, const Parameters &params) {

#ifdef CSDS_DEBUG_CHECKS
  /* The position is expected to be a double => error with periodic */
  if (periodic) {
    csds_error("Not implemented yet");
  }
#endif

  /* Compute the interpolation scaling. */
  const TypeField wa = (t - t_before) / (t_after - t_before);
  const TypeField wb = 1. - wa;

  const TypeField *v_bef = (TypeField *)before.field;
  const TypeFirst *a_bef = (TypeFirst *)before.first_deriv;
  const TypeField *v_aft = (TypeField *)after.field;
  const TypeFirst *a_aft = (TypeFirst *)after.first_deriv;

  for (int i = 0; i < dimension; i++) {
    TypeField *v = (TypeField *)output;
    TypeFirst a0 = a_bef ? a_bef[i] : -1;
    TypeFirst a1 = a_aft ? a_aft[i] : -1;

    /* Add cosmological factor for acceleration */
    if (params.IsWithCosmology() && a_bef && a_aft) {
      params.GetCosmology().AddFactorVelocity<TypeFirst>(t_before, &a0);
      params.GetCosmology().AddFactorVelocity<TypeFirst>(t_after, &a0);
    }

    /* Use a cubic hermite spline. */
    if (a_bef && a_aft) {
      v[i] = interpolate_cubic_hermite_spline<TypeField, TypeFirst>(
          t_before, v_bef[i], a0, t_after, v_aft[i], a1, t);
    }
    /* Use linear interpolation. */
    else {
      v[i] = wa * v_aft[i] + wb * v_bef[i];
    }
  }
}

/**
 * @brief Interpolates a field in N dimension.
 *
 * The field is in float.
 *
 * @param before Pointer to the #csds_reader_field at a time < t.
 * @param after Pointer to the #csds_reader_field at a time > t.
 * @param otuput Pointer to the output value.
 * @param t_before Time of field_before (< t).
 * @param t_after Time of field_after (> t).
 * @param t Requested time.
 * @param periodic Should the periodic boundary be applied?
 * @param params The simulation's #csds_parameters.
 */
template <class TypeField>
INLINE static void interpolate_linear(
    const TypeField t_before, const struct csds_reader_field &before,
    const TypeField t_after, const struct csds_reader_field &after,
    void *__restrict__ output, const TypeField t, const int dimension,
    ATTR_UNUSED int periodic, ATTR_UNUSED const Parameters &params) {

#ifdef CSDS_DEBUG_CHECKS
  /* The position is expected to be a double => error with periodic */
  if (periodic) {
    csds_error("Not implemented yet");
  }
#endif

  /* interpolate vectors. */
  TypeField *a = (TypeField *)output;
  const TypeField *a_bef = (TypeField *)before.field;
  const TypeField *a_aft = (TypeField *)after.field;
  for (int i = 0; i < dimension; i++) {
    a[i] = interpolate_linear_spline(t_before, a_bef[i], t_after, a_aft[i], t);
  }
}

#endif  // CSDS_CSDS_INTERPOLATION_H
