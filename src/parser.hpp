/*******************************************************************************
 * This file is part of CSDS and was copied from SWIFT.
 * Copyright (c) 2021 loic.hausammann@epfl.ch
 *               SWIFT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_PARSER_H
#define CSDS_PARSER_H

/* Config parameters. */
#include "../config.hpp"

/* Standard headers */
#include <stdio.h>
#include <string.h>
#include <string>

/* Local headers */
#include "error.hpp"

#if defined(HAVE_HDF5)
#include <hdf5.h>
#endif

/* Some constants. */
#define PARSER_MAX_LINE_SIZE 256
#define PARSER_MAX_NO_OF_PARAMS 600
#define PARSER_MAX_NO_OF_SECTIONS 64

/* A parameter in the input file */
struct parameter {
  char name[PARSER_MAX_LINE_SIZE];
  char value[PARSER_MAX_LINE_SIZE];
};

struct section {
  char name[PARSER_MAX_LINE_SIZE];
};

/* The array of parameters read from a file */
class Parser {
 public:
  Parser(const std::string file_name);
  void PrintParams() const;

  template <class Type>
  Type GetParam(const char *name) const;
  void GetParamString(const char *name, char *retParam) const;

  template <class Type>
  Type GetOptParam(const char *name, Type def) const;

  void GetOptParamString(const char *name, char *retParam,
                         const char *def) const;

  template <class Type, size_t N>
  void GetParamArray(const char *name, Type (&values)[N]) const;

  template <class Type, size_t N>
  int GetOptParamArray(const char *name, Type (&values)[N]) const;

 protected:
  struct section mSection[PARSER_MAX_NO_OF_SECTIONS];
  struct parameter mData[PARSER_MAX_NO_OF_PARAMS];
  int mSectionCount;
  int mParamCount;
  std::string mFilename;
  int mLineNumber;

  static int IsEmpty(const char *str);
  static int CountIndentation(const char *str);
  void ParseLine(char *line);
  void ParseValue(char *line);
  void ParseSectionParam(char *line, int *isFirstParam, char *sectionName);
  void FindDuplicateParams(const char *param_name) const;
  void FindDuplicateSection(const char *section_name) const;

  template <class Type>
  int GetParamInternal(const char *name, Type *def, Type *result) const;

  template <class Type>
  static std::string GetFormat();

  template <class Type, int N>
  inline int GetParamArrayInternal(const char *name, int required,
                                   Type (&values)[N]) const;

  static char *TrimLeading(char *s);
  static char *TrimTrailing(char *s);
  static char *TrimBoth(char *s);
};

/** @brief Define the printf string for the type of elements */
template <class Type>
std::string Parser::GetFormat() {
  if (std::is_same<Type, char>::value) {
    return "%c";
  } else if (std::is_same<Type, int>::value) {
    return "%d";
  } else if (std::is_same<Type, float>::value) {
    return "%f";
  } else if (std::is_same<Type, double>::value) {
    return "%lf";
  } else if (std::is_same<Type, long long>::value) {
    return "%lld";
  } else {
    csds_error("Not implemented");
  }
}

// Retrieve parameter value from structure. TYPE is the data type, float, int
// etc.
template <class Type>
inline int Parser::GetParamInternal(const char *name, Type *def,
                                    Type *result) const {
  std::string format = GetFormat<Type>();

  char str[PARSER_MAX_LINE_SIZE];
  for (int i = 0; i < mParamCount; i++) {
    if (strcmp(name, mData[i].name) == 0) {
      /* Check that exactly one number is parsed, capture junk. */
      if (sscanf(mData[i].value, (" " + format + "%s ").c_str(), result, str) !=
          1) {
        csds_error("Tried parsing '"
                   << mData[i].name << "' but found '" << mData[i].value
                   << "' with illegal trailing characters '" << str << "'.");
      }
      return 1;
    }
  }
  if (def == NULL)
    csds_error("Cannot find '" << name
                               << "' in the structure, in file: " << mFilename);
  return 0;
}

/* Macro defining functions that get primitive types as simple one-line YAML
 * arrays, that is SEC: [v1,v2,v3...] format, with the extension that the []
 * are optional. TYPE is the data type, float etc. FMT a format to parse a
 * single value, so "%f" for a float and DESC the type description
 * i.e. "float".
 */
template <class Type, int N>
inline int Parser::GetParamArrayInternal(const char *name, int required,
                                         Type (&values)[N]) const {
  std::string format = GetFormat<Type>();
  char str[PARSER_MAX_LINE_SIZE];
  char cpy[PARSER_MAX_LINE_SIZE];

  for (int i = 0; i < mParamCount; i++) {
    if (!strcmp(name, mData[i].name)) {
      char *cp = cpy;
      strcpy(cp, mData[i].value);
      cp = TrimBoth(cp);

      /* Strip off [], if present. */
      if (cp[0] == '[') cp++;
      int l = strlen(cp);
      if (cp[l - 1] == ']') cp[l - 1] = '\0';
      cp = TrimBoth(cp);

      /* Format that captures spaces and trailing junk. */
      char fmt[20];
      sprintf(fmt, " %s%%s ", format.data());

      /* Parse out values which should now be "v, v, v" with
       * internal     whitespace variations. */
      char *p = strtok(cp, ",");
      for (int k = 0; k < N; k++) {
        if (p != NULL) {
          Type tmp_value;
          if (sscanf(p, fmt, &tmp_value, str) != 1) {
            csds_error("Tried parsing '"
                       << mData[i].name << "' but found '" << mData[i].value
                       << "' with illegal trailing characters '" << str
                       << "'.");
          }
          values[k] = tmp_value;
        } else {
          csds_error("Array '" << name << "' with value '" << mData[i].value
                               << "' has too few values, expected " << N);
        }
        if (k < N - 1) p = strtok(NULL, ",");
      }
      return 1;
    }
  }
  if (required)
    csds_error("Cannot find '" << name << "' in the structure, in file '"
                               << mFilename << "'.");
  return 0;
}

/**
 * @brief Retrieve char array parameter from structure.
 *
 * @param name Name of the parameter to be found
 * @param values Values of the parameter found, of size at least N.
 */
template <class Type, size_t N>
void Parser::GetParamArray(const char *name, Type (&values)[N]) const {
  GetParamArrayInternal<Type, N>(name, 1, values);
}

/**
 * @brief Retrieve optional char array parameter from structure.
 *
 * @param name Name of the parameter to be found
 * @param values Values of the parameter found, of size at least N. If the
 *               parameter is not found these values will be returned
 *               unmodified, so should be set to the default values.
 * @return whether the parameter has been found.
 */
template <class Type, size_t N>
int Parser::GetOptParamArray(const char *name, Type (&values)[N]) const {
  if (GetParamArrayInternal<Type, N>(name, 0, values) != 1) {
    return 0;
  }
  return 1;
}

/**
 * @brief Retrieve optional integer parameter from structure.
 *
 * @param params Structure that holds the parameters
 * @param name Name of the parameter to be found
 * @param def Default value of the parameter of not found.
 * @return Value of the parameter found
 */
template <class Type>
Type Parser::GetOptParam(const char *name, Type def) const {
  Type result = 0;
  if (GetParamInternal<Type>(name, &def, &result)) return result;
  return def;
}

/**
 * @brief Retrieve integer parameter from structure.
 *
 * @param name Name of the parameter to be found
 * @return Value of the parameter found
 */
template <class Type>
Type Parser::GetParam(const char *name) const {
  Type result = 0;
  GetParamInternal<Type>(name, NULL, &result);
  return result;
}

#endif /* CSDS_PARSER_H */
