/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Standard headers */
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

/* This file header */
#include "mapped_file.hpp"

/* Local headers */
#include "header.hpp"
#include "tools.hpp"

using namespace std;

/**
 * @brief get the size of a file.
 *
 * @param fd file id.
 *
 * @return file size.
 */
size_t csds_loader_io_get_file_size(int fd) {
  struct stat s;
  int status = fstat(fd, &s);
  if (status != 0) csds_error("Unable to get file size: " << strerror(errno));
  return s.st_size;
}

/**
 * @brief Remove the extension from a name.
 */
string strip_ext(string fname) {
  size_t lastindex = fname.find_last_of(".");
  return fname.substr(0, lastindex);
}

/**
 * @brief Map a file.
 *
 * #csds_loader_io_munmap_file should be called to unmap the file.
 *
 * @param filename file to read.
 * @param read_only Open the file in read only mode?
 * @param track_mmap Should we track the memory reading?
 *
 */
void MappedFile::Open(const string filename, bool read_only,
                      ATTR_UNUSED bool track_mmap) {

  /* open the file. */
  int fd;

  if (read_only)
    fd = open(filename.data(), O_RDONLY);
  else
    fd = open(filename.data(), O_RDWR);

  if (fd == -1)
    csds_error("Unable to open file: " << filename << " (" << strerror(errno)
                                       << ")");

  /* get the file size. */
  this->mMapSize = csds_loader_io_get_file_size(fd);

  /* map the memory. */
  int mode = PROT_READ;
  if (!read_only) mode |= PROT_WRITE;

  this->mMap = (char *)mmap(NULL, this->mMapSize, mode, MAP_SHARED, fd, 0);
  if (this->mMap == MAP_FAILED)
    csds_error("Failed to allocate map of size " << this->mMapSize << " bytes ("
                                                 << strerror(errno) << ")");

  /* Close the file. */
  close(fd);

#ifdef CSDS_MMAP_TRACKING
  /* Now deal with the mmap tracking */
  this->mTracking.number_elements = 0;
  this->mTracking.size = 0;
  this->mTracking.use_tracking = track_mmap;

  /* Nothing to do without tracking */
  if (!track_mmap) return;

  /* Create the filename */
  string tracking_filename = filename;
  tracking_filename = strip_ext(tracking_filename);
  tracking_filename += ".track";

  this->mTracking.stream = std::ofstream(
      tracking_filename, std::ofstream::out | std::ofstream::binary);

  /* Write the header */
  this->mTracking.stream << std::setw(20) << std::left << "CSDS_TRACK";
  this->mTracking.stream << this->mTracking.number_elements;
#endif
}

/**
 * @brief Unmap a file.
 *
 * @param map The #mapped_file.
 */
void MappedFile::Close() {
  /* unmap the file. */
  if (munmap(this->mMap, this->mMapSize) != 0) {
    message("Unable to unmap the file :" << strerror(errno));
    csds_abort(186);
  }
  mMap = NULL;
}
