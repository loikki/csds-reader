/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 loic.hausammann@epfl.ch
 *               2016 Pedro Gonnet (pedro.gonnet@durham.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
/* Some standard headers. */
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

/* Config parameters. */
#include "../config.hpp"

/* This object's header. */
#include "logfile_writer.h"

/**
 * @brief Error macro. Prints the message given in argument and aborts.
 *
 */
#define error_writer(s, ...)                                                 \
  ({                                                                         \
    fflush(stdout);                                                          \
    fprintf(stderr, "%s:%s():%i: " s "\n", __FILE__, __FUNCTION__, __LINE__, \
            ##__VA_ARGS__);                                                  \
    exit(642);                                                               \
  })

/**
 * @brief Provide the current size used by the CSDS in GB (not the allocated
 * one).
 *
 * @param log The #csds_logfile_writer.
 */
EXTERN float csds_logfile_writer_get_current_filesize_used_gb(
    const struct csds_logfile_writer *log) {
  return log->count / (1024.f * 1024.f * 1024.f);
}

/**
 * @brief Ensure that at least size bytes are available in the
 * #csds_logfile_writer.
 *
 * @param d The #csds_logfile_writer.
 * @param required_size The required size for the #csds_logfile_writer
 * @param increase_size If not enough size, increase by this amount
 */
EXTERN void csds_logfile_writer_ensure(struct csds_logfile_writer *d,
                                       size_t required_size,
                                       size_t increase_size) {

  /* If we have enough space already, just bail. */
  if (d->size - d->count > required_size) return;

  /* Unmap the current data. */
  if (munmap(d->data, d->size) != 0) {
    error_writer("Failed to unmap %zi bytes (%s).", d->size, strerror(errno));
  }

  /* Update the size and count. */
  const size_t trunc_count = d->count & d->page_mask;
  d->file_offset += trunc_count;
  d->count -= trunc_count;
  d->size = (d->count + increase_size + ~d->page_mask) & d->page_mask;

  /* Re-allocate the file size. */
  if (posix_fallocate(d->fd, d->file_offset, d->size) != 0) {
    error_writer("Failed to pre-allocate the logfile.");
  }

  /* Re-map starting at the end of the file. */
  if ((d->data = (char *)mmap(NULL, d->size, PROT_WRITE, MAP_SHARED, d->fd,
                              d->file_offset)) == MAP_FAILED) {
    error_writer("Failed to allocate map of size %zi bytes (%s).", d->size,
                 strerror(errno));
  }
}

/**
 * @brief Flush the #csds_logfile_writer to disk.
 */
EXTERN void csds_logfile_writer_sync(struct csds_logfile_writer *d) {
  if (msync(d->data, d->count, MS_SYNC) != 0)
    error_writer("Failed to sync memory-mapped data.");
}

/**
 * @brief Finalize the #csds_logfile_writer.
 */
EXTERN void csds_logfile_writer_close(struct csds_logfile_writer *d) {
  /* Unmap the data in memory. */
  if (munmap(d->data, d->count) != 0) {
    error_writer("Failed to unmap the logfile (%s).", strerror(errno));
  }

  /* Truncate the file to the correct length. */
  if (ftruncate(d->fd, d->file_offset + d->count) != 0) {
    error_writer("Failed to truncate the logfile (%s).", strerror(errno));
  }

  /* Close the memory-mapped file. */
  if (close(d->fd) != 0) error_writer("Failed to close memory-mapped file.");
}

/**
 * @brief Initialize a file csds_logfile_writer.
 *
 * @param d The #csds_logfile_writer to initialize.
 * @param filename The fully qualified name of the file in which to
 * csds_logfile_writer, note that it will be overwritten.
 * @param size The initial buffer size for this #csds_logfile_writer.
 */
EXTERN void csds_logfile_writer_init(struct csds_logfile_writer *d,
                                     const char *filename, size_t size) {

  /* Create the output file.
     The option O_RDWR seems to be required by mmap.
  */
  if ((d->fd = open(filename, O_CREAT | O_RDWR, 0660)) == -1) {
    error_writer("Failed to create the logfile '%s' (%s).", filename,
                 strerror(errno));
  }

  /* Adjust the size to be at least the page size. */
  const size_t page_mask = ~(sysconf(_SC_PAGE_SIZE) - 1);
  size = (size + ~page_mask) & page_mask;

  /* Pre-allocate the file size. */
  if (posix_fallocate(d->fd, 0, size) != 0) {
    error_writer("Failed to pre-allocate the logfile.");
  }

  /* Map memory to the created file. */
  if ((d->data = (char *)mmap(NULL, size, PROT_WRITE, MAP_SHARED, d->fd, 0)) ==
      MAP_FAILED) {
    error_writer("Failed to allocate map of size %zi bytes (%s).", size,
                 strerror(errno));
  }

  /* Init some counters. */
  d->size = size;
  d->count = 0;
  d->file_offset = 0;
  d->page_mask = page_mask;
}

/**
 * @brief Restart a file csds_logfile_writer.
 *
 * @param d The #csds_logfile_writer to restart.
 * @param filename The fully qualified name of the file in which to
 * csds_logfile_writer, note that it will be overwritten.
 */
EXTERN void csds_logfile_writer_restart(struct csds_logfile_writer *d,
                                        const char *filename) {
  /* Create the output file.
     The option O_RDWR seems to be required by mmap.
  */
  if ((d->fd = open(filename, O_RDWR, 0660)) == -1) {
    error_writer("Failed to open the logfile '%s' (%s).", filename,
                 strerror(errno));
  }

  /* Adjust the size to be at least the page size. */
  const size_t page_mask = ~(sysconf(_SC_PAGE_SIZE) - 1);
  size_t size = (d->size + ~page_mask) & page_mask;

  /* Pre-allocate the file size. */
  if (posix_fallocate(d->fd, 0, size) != 0) {
    error_writer("Failed to pre-allocate the logfile.");
  }

  /* Map memory to the created file. */
  if ((d->data = (char *)mmap(NULL, size, PROT_WRITE, MAP_SHARED, d->fd,
                              d->file_offset)) == MAP_FAILED) {
    error_writer("Failed to allocate map of size %zi bytes (%s).", d->size,
                 strerror(errno));
  }
}

/**
 * @brief Write the first part of the header.
 * The next part should be all the informations about the
 * masks.
 *
 * @param logfile The empty #csds_logfile_writer
 *
 * @return The buffer where to write the missing information
 * (should not be modified and simply provided to
 * #csds_logfile_writer_write_end_header).
 */
EXTERN char *csds_logfile_writer_write_begining_header(
    struct csds_logfile_writer *logfile) {

  /* Check if the logfile is empty */
  uint64_t file_offset = logfile->file_offset;
  if (file_offset != 0)
    error_writer(
        "The CSDS is not empty."
        "This function should be called before writing anything in the CSDS");

  /* Write format information. */
  char file_format[CSDS_STRING_SIZE] = CSDS_FORMAT_STRING;
  csds_write_data(logfile, &file_offset, CSDS_STRING_SIZE,
                  (const char *)&file_format);

  /* Write the major version number. */
  int major = CSDS_MAJOR_VERSION;
  csds_write_data(logfile, &file_offset, sizeof(int), (const char *)&major);

  /* Write the minor version number. */
  int minor = CSDS_MINOR_VERSION;
  csds_write_data(logfile, &file_offset, sizeof(int), (const char *)&minor);

  /* write offset direction. */
  const int reversed = csds_offset_backward;
  csds_write_data(logfile, &file_offset, sizeof(int), (const char *)&reversed);

  /* placeholder to write the offset of the first log here. */
  char *skip_header =
      csds_logfile_writer_get(logfile, CSDS_OFFSET_SIZE, &file_offset);

  /* write number of bytes used for names. */
  const unsigned int label_size = CSDS_STRING_SIZE;
  csds_write_data(logfile, &file_offset, sizeof(unsigned int),
                  (const char *)&label_size);

  return skip_header;
}
