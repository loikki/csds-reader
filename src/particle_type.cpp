/*******************************************************************************
 * This file is part of CSDS and was copied from SWIFT.
 * Copyright (c) 2021 loic.hausammann@epfl.ch.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* This object's header. */
#include "particle_type.hpp"

const char* part_type_names[csds_type_count + 1] = {
    "gas",      "dark_matter", "dark_matter_background",
    "sink",     "stars",       "black_holes",
    "neutrino", "count"};
