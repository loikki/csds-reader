/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2021 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Standard headers */
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/python/stl_iterator.hpp>
#include <utility>

/* This file's header */
#include "python_wrapper.hpp"

/* Local headers */
#include "csds_array.hpp"
#include "particle_type.hpp"
#include "reader.hpp"

#define PYTHON_STR(INPUT) bp::extract<std::string>(bp::str(INPUT))()

BOOST_PYTHON_MODULE(libcsds) {
  using namespace boost::python;

  Py_Initialize();
  np::initialize();

  class_<PyReader>(
      "Reader",
      "Deals with a CSDS file and provides the data through"
      " its different methods. The reader is really open with the method "
      "__enter__"
      "(called by `with ...``). Outside the area delimited by with, "
      "the CSDS is not accessible.\n\n"
      "Methods\n"
      "-------\n\n"
      "get_time_begin\n"
      "    Provides the initial time of the simulation\n"
      "get_time_end\n"
      "    Provides the final time of the simulation\n"
      "get_time_limits\n"
      "    Provides the initial and final time of the simulation\n"
      "get_data\n"
      "    Reads some particle's data from the logfile\n\n"
      "update_particles\n"
      "    Update the particles in the cache and returns them\n\n"
      "Examples\n"
      "--------\n\n"
      ">>> import libcsds as csds\n"
      ">>> with csds.Reader(\"index_0000\") as reader:\n"
      ">>>    t0, t1 = reader.get_time_begin(), reader.get_time_end()\n"
      ">>>    fields = reader.get_list_fields()\n"
      ">>>    if \"Coordinates\" not in fields:\n"
      ">>>        raise Exception(\"Field Coordinates not present in the "
      "logfile.\")\n"
      ">>>    pos, ent = reader.get_data([\"Coordinates\", "
      "\"Entropies\"]"
      ", 0.5 * (t0 + t1))\n",
      init<std::string, int, int, bool, bool>(
          (arg("self"), arg("basename"), arg("verbose") = 0,
           arg("number_index") = 10, arg("restart_init") = false,
           arg("use_cache") = false),
          "Parameters\n"
          "----------\n\n"
          "basename: str\n"
          "    Basename of the logfile.\n\n"
          "verbose: int (optional)\n"
          "    Verbose level\n\n"
          "number_index: int (optional)\n"
          "    Number of index files to generate\n\n"
          "restart_init: bool (optional)\n"
          "    Are we restarting an initialization?\n\n"
          "use_cache: bool (optional)\n"
          "    Use a cache for the evolution of particles?\n\n"))
      /* Methods */
      .def("get_time_begin", &PyReader::GetTimeBegin,
           "Read the initial time of the simulation.\n\n"
           "Returns\n"
           "-------\n\n"
           "time: float\n"
           "  The initial time\n")
      .def("get_time_end", &PyReader::GetTimeEnd,
           "Read the final time of the simulation.\n\n"
           "Returns\n"
           "-------\n\n"
           "time: float\n"
           "  The initial time\n")
      .def("get_time_limits", &PyReader::GetTimeLimits,
           "Read the initial and final time of the simulation.\n\n"
           "Returns\n"
           "-------\n\n"
           "t0: float\n"
           "  The initial time\n"
           "t1: float\n"
           "  The final time\n")
      .def("get_list_fields", &PyReader::GetListFields,
           (arg("self"), arg("part_type") = bp::object()),
           "Read the list of available fields in the logfile.\n\n"
           "Parameters\n"
           "----------\n\n"
           "type: int, list\n"
           "  The particle type for the list of fields\n\n"
           "Returns\n"
           "-------\n"
           "fields: tuple\n"
           "  The list of fields present in the logfile.\n")
      .def("get_data", &PyReader::GetData,
           (arg("self"), arg("fields"), arg("time"),
            arg("part_type") = bp::object(),
            arg("filter_by_ids") = bp::object()),
           "Read some fields from the logfile at a given time.\n\n"
           "Parameters\n"
           "----------\n\n"
           "fields: list\n"
           "  The list of fields (e.g. 'Coordinates', 'Entropies', ...)\n\n"
           "time: float\n"
           "  The time at which the fields must be read.\n\n"
           "filter_by_ids: bool\n"
           "  Read only the IDs provided.\n\n"
           "part_type: int, list"
           "  Types to read (default all). This can be a single int or a list"
           " of boolean where the index corresponds to the particle type.\n\n"
           "Returns\n"
           "-------\n\n"
           "list_of_fields: dict\n"
           "  The key is given by the fields name and it contains a numpy "
           "array for each field.\n")
      .def("update_particles", &PyReader::UpdateParticles,
           (arg("self"), arg("fields"), arg("time")),
           "Update the particles read with get_data using the cache.\n\n"
           "Parameters\n"
           "----------\n\n"
           "fields: list\n"
           "  The list of fields (e.g. 'Coordinates', 'Entropies', ...)\n\n"
           "time: float\n"
           "  The time at which the fields must be read.\n\n"
           "Returns\n"
           "-------\n\n"
           "list_of_fields: dict\n"
           "  The key is given by the fields name and it contains a numpy "
           "array for each field.\n")
      .def("__enter__", &PyReader::Enter)
      .def("__exit__", &PyReader::Exit)
      /* Attributes */
      .add_property("verbose", &PyReader::GetVerbose, &PyReader::SetVerbose)
      .def_readonly("basename", &PyReader::mBasename)
      .def_readonly("number_index", &PyReader::mNumberIndex)
      .def_readonly("restart_init", &PyReader::mRestartInit)
      .def_readonly("use_cache", &PyReader::mUseCache);

  /* Now the particle types */
  auto test = enum_<part_type>("particle_types");
  for (int i = 0; i < csds_type_count + 1; i++) {
    test.value(part_type_names[i], (part_type)i);
  }
  test.export_values();
}

/**
 * @brief Read the particles from a file.
 *
 * @param fields The list of fields to read.
 * @param time The requested time
 * @param ids The IDs of the particle to read (NULL in order to read everything)
 *
 * @return The fields
 */
bp::dict PyReader::GetData(bp::list &py_fields, double time,
                           bp::object &py_part_type, bp::object &py_ids) {
  if (mReader == NULL) {
    csds_error("You need to open the reader with the keyword 'with'");
  }

  /* Check the input */
  const bool use_ids = !py_ids.is_none();
  if (use_ids && !py_part_type.is_none()) {
    csds_error("Cannot provide both the type of particles and the ids");
  }

  /* Convert the fields */
  std::vector<Field> fields = GetFieldsFromNames(py_fields);
  std::array<bool, csds_type_count> part_type =
      GetParticleTypesFromObject(py_part_type);
  const int n_fields = len(py_fields);

  /* Convert the IDs */
  bp::list py_ids_list;
  if (use_ids) {
    bp::extract<bp::list> get_ids(py_ids);
    if (!get_ids.check()) {
      csds_error("You need to provide a list for the IDs");
    }
    py_ids_list = get_ids();
    if (len(py_ids_list) != csds_type_count) {
      csds_error("The list of Ids should have a length of " << csds_type_count);
    }
  }

  /* Set the time. */
  mReader->SetTime(time);

  /* Get the number of particles. */
  std::array<uint64_t, csds_type_count> n_part;
  if (use_ids) {
    for (int i = 0; i < csds_type_count; i++) {
      bp::extract<bp::numpy::ndarray> get_array(py_ids_list[i]);
      if (!get_array.check()) {
        /* Do we need this type? object defaults to None */
        if (py_ids_list[i] == bp::object()) {
          n_part[i] = 0;
          continue;
        }

        csds_error(
            "The list of ids should contain numpy arrays or None."
            " Found: "
            << PYTHON_STR(py_ids_list[i]));
      }
      bp::numpy::ndarray array = get_array();
      n_part[i] = len(array);
    }
  }
  /* If the ids are provided, use them to get the number of particles */
  else {
    /* If no ids provided, read everything from the index files */
    mReader->GetNumberParticles(n_part, part_type);
  }

  /* Count the total number of particles. */
  uint64_t n_tot = 0;
  for (int i = 0; i < csds_type_count; i++) {
    n_tot += n_part[i];
  }

  /* Print the number of particles */
  if (mReader->GetVerbose() > 0) {
    message("Found " << n_tot << " particles");
  }

  /* Allocate the output memory. */
  std::vector<CsdsArray> output;
  output.reserve(n_fields);
  for (int i = 0; i < n_fields; i++) {
    output.emplace_back(n_tot, fields[i]);
  }

  /* Read the particles. */
  if (use_ids) {
    /* Get the arrays in a C compatible way */
    std::array<long long *, csds_type_count> part_ids;
    for (int i = 0; i < csds_type_count; i++) {

      /* Skip unnecessary types */
      if (n_part[i] == 0) {
        part_ids[i] = NULL;
        continue;
      }

      /* Now get the array */
      bp::extract<bp::numpy::ndarray> get_array(py_ids_list[i]);
      bp::numpy::ndarray array = get_array();
      part_ids[i] = (long long *)array.get_data();
    }

    /* Enable multithreading */
    Py_BEGIN_ALLOW_THREADS;

    /* Read the particles. */
    mReader->ReadParticlesFromIds(time, output, n_part, part_ids);

    /* Disable multithreading */
    Py_END_ALLOW_THREADS;

    /* Recompute n_tot for the particles removed */
    n_tot = 0;
    for (int i = 0; i < csds_type_count; i++) {
      n_tot += n_part[i];
    }
  } else {
    /* Enable multithreading */
    Py_BEGIN_ALLOW_THREADS;

    mReader->ReadAllParticles(time, output, n_part);

    /* Disable multithreading */
    Py_END_ALLOW_THREADS;
  }

  /* Create the python output. */
  bp::dict out;

  for (int i = 0; i < n_fields; i++) {
    out[py_fields[i]] = output[i].ToNumpy(/* stealing_buffer */ true);
  }

  return out;
}

/**
 * @brief Read the particles from a file.
 *
 * @param fields The list of fields to read.
 * @param time The requested time
 * @param ids The IDs of the particle to read (NULL in order to read everything)
 *
 * @return The fields
 */
bp::dict PyReader::UpdateParticles(bp::list &py_fields, double time) {
  if (mReader == NULL) {
    csds_error("You need to open the reader with the keyword 'with'");
  }

  /* Convert the fields */
  std::vector<Field> fields = GetFieldsFromNames(py_fields);
  bp::object py_part_type = bp::object();
  std::array<bool, csds_type_count> part_type =
      GetParticleTypesFromObject(py_part_type);
  const int n_fields = len(py_fields);

  /* Set the time. */
  mReader->SetTime(time);

  /* Get the number of particles. */
  std::array<uint64_t, csds_type_count> n_part;
  mReader->GetNumberParticles(n_part, part_type);

  /* Count the total number of particles. */
  uint64_t n_tot = 0;
  for (int i = 0; i < csds_type_count; i++) {
    n_tot += n_part[i];
  }

  /* Check if we have enough cache */
  for (int i = 0; i < csds_type_count; i++) {
    if (n_part[i] != 0 &&
        n_part[i] >= (uint64_t)mReader->GetCache()[i].Capacity()) {
      csds_error("Not enough memory for particle type "
                 << part_type_names[i]
                 << ". Are you sure that you are requesting the correct type?"
                    " You can try to increase Cache:OverAllocation or"
                    " Cache:InitialAllocation");
    }
  }

  /* Print the number of particles */
  if (mReader->GetVerbose() > 0) {
    message("Found " << n_tot << " particles");
  }

  /* Allocate the output memory. */
  std::vector<CsdsArray> output;
  output.reserve(n_fields);
  for (int i = 0; i < n_fields; i++) {
    output.emplace_back(n_tot, fields[i]);
  }

  /* Read the particles. */

  /* Enable multithreading */
  Py_BEGIN_ALLOW_THREADS;

  mReader->UpdateParticles(time, output, n_part);

  /* Disable multithreading */
  Py_END_ALLOW_THREADS;

  /* Create the python output. */
  bp::dict out;

  for (int i = 0; i < n_fields; i++) {
    out[py_fields[i]] = output[i].ToNumpy(/* stealing_buffer */ true);
  }

  return out;
}

/**
 * @brief Provides the list of fields available in the logfile.
 *
 * @param part_type The particle type (int or list of int)
 *
 * @return The list of fields by name.
 */
bp::list PyReader::GetListFields(bp::object &part_type) {
  if (mReader == NULL) {
    csds_error("You need to open the reader with the keyword 'with'");
  }

  /* Convert the inputs */
  std::array<bool, csds_type_count> types =
      GetParticleTypesFromObject(part_type);

  /* Inputs are done, now get the fields */
  const Header &h = mReader->GetHeader();

  /* Find if the fields are present in all the types. */
  std::vector<int> field_present(field_enum_count, 1);
  for (int i = 0; i < field_enum_count; i++) {
    /* Check all the types */
    for (int type = 0; type < csds_type_count; type++) {
      /* Skip the types that are not required
         and the field not found */
      if (types[type] == 0 || field_present[i] == 0) continue;

      /* Search among all the fields of the particle type */
      int found = 0;
      for (auto const &field : h.GetFields()[type]) {
        if (field.GetField() == (field_enum)i) {
          found = 1;
          break;
        }
      }

      /* Set the field as not found */
      if (!found) field_present[i] = 0;
    }
  }

  /* Remove the special flags */
  field_present[field_enum_special_flags] = 0;

  /* Count the number of fields found */
  int number_fields = 0;
  for (int i = 0; i < field_enum_count; i++) {
    number_fields += field_present[i];
  }

  /* Create the python list for the output*/
  bp::list list;
  for (int i = 0; i < field_enum_count; i++) {
    /* Keep only the field present. */
    if (field_present[i] == 0) continue;

    list.append(SingleField::GetNameFromEnum((field_enum)i));
  }

  return list;
}

/**
 * @brief Convert the python fields into a C++ vector
 *
 * @param fields The list of fields to read in the form of strings.
 *
 * @return The list of fields to read as enum.
 */
std::vector<Field> PyReader::GetFieldsFromNames(bp::list &fields) {
  const Header &header = mReader->GetHeader();

  /* Get the field enum from the header. */
  std::vector<Field> out;
  out.reserve(len(fields));
  for (int i = 0; i < len(fields); i++) {
    /* Get the an item in the list. */
    bp::extract<std::string> get_string(fields[i]);
    if (!get_string.check()) {
      csds_error(
          "You need to provide a field name, found: " << PYTHON_STR(fields[i]));
    }

    /* Get the field */
    bool found = false;
    for (int type = 0; type < csds_type_count; type++) {
      for (auto const &field : header.GetFields()[type]) {
        if (field.GetName().compare(get_string()) == 0) {
          out.emplace_back(field);
          found = true;
          break;
        }
      }
      /* Early escape */
      if (found) {
        break;
      }
    }

    /* Check if we got a field */
    if (!found) {
      csds_error("Unable to find field: " << get_string());
    }
  }

  return out;
}

/**
 * @brief Convert the type to read into a C++ vector
 *
 * @param types The object representing the types (None, int or array).
 *
 * @return An array containing if each type should be read or not
 */
std::array<bool, csds_type_count> PyReader::GetParticleTypesFromObject(
    bp::object &py_types) {

  bp::extract<int> get_type_int(py_types);
  bp::extract<bp::list> get_type_list(py_types);
  std::array<bool, csds_type_count> types;
  for (int i = 0; i < csds_type_count; i++) {
    types[i] = false;
  }
  /* The user does not provide anything */
  if (py_types.is_none()) {
    for (int i = 0; i < csds_type_count; i++) {
      types[i] = true;
    }
  }
  /* The user provides a single int */
  else if (get_type_int.check()) {
    types[get_type_int()] = true;
  }
  /* The users provides a list */
  else if (get_type_list.check()) {
    bp::list list_type = get_type_list();
    for (int i = 0; i < len(list_type); i++) {
      bp::extract<int> extract_int(list_type[i]);
      if (!extract_int.check())
        csds_error("Unexpected element: " << PYTHON_STR(list_type[i]));
      types[extract_int()] = true;
    }
  }
  /* Unexpected input */
  else {
    csds_error(
        "Unexpected input for the particle type: " << PYTHON_STR(py_types));
  }

  return types;
}
