/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_PARTICLE_H
#define CSDS_PARTICLE_H

/* Include the tools. */
#include "tools.hpp"

/* Include the other local files. */
#include "definitions.hpp"
#include "interpolation.hpp"
#include "logfile.hpp"
#include "parameters.hpp"
#include "time_array.hpp"

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Read a particle (of any type) record in the log file.
 *
 * @param offset offset of the record to read.
 * @param output Buffer for the requested field.
 * @param field_read The field to read.
 * @param derivative The derivative to read (0 for the field).
 * @param header (out) The record header.
 * @param fields The list of available fields
 * @param number_fields The number of fields
 * @param log_map The mmap logfile.
 *
 * @return position after the record.
 */
INLINE static size_t csds_particle_read_field(size_t offset, void *output,
                                              const Field &field_read,
                                              const int derivative,
                                              struct record_header &header,
                                              const std::vector<Field> &fields,
                                              LogFile &log_map) {

  /* Read the record's mask. */
  offset = log_map.ReadRecordHeader(offset, header);

#ifdef CSDS_DEBUG_CHECKS
  /* Check if it is not a time record. */
  if (header.mask == CSDS_TIMESTAMP_MASK) {
    csds_error("Cannot read a particle from timestep record.");
  }

  if (derivative == 1 && field_read.GetFirstDerivative() == field_enum_none) {
    csds_error("Trying to read the non existing first derivative.");
  }
  if (derivative == 2 && field_read.GetSecondDerivative() == field_enum_none) {
    csds_error("Trying to read the non existing second derivative.");
  }
#endif

  /* Get the field to read */
  const SingleField &read = derivative == 0 ? field_read.GetField()
                            : derivative == 1
                                ? field_read.GetFirstDerivative()
                                : field_read.GetSecondDerivative();

  /* Read the record and copy it to a particle. */
  for (auto const &current : fields) {

    /* Is the mask present? */
    if (!(current.GetField() & header.mask)) {
      continue;
    }

    if (current.GetField() == read) {
      /* Read the data. */
      offset = log_map.ReadData(offset, current.GetFieldSize(), output);
    } else {
      /* Update the buffer's position. */
      offset += current.GetFieldSize();
    }
  }

  return offset;
}

/**
 * @brief Extract the flag and its related data from the data inside a record.
 *
 * Flags and data are stored as a 32-bit unsigned int in which the lower 24 bits
 * contain the data, and the top 8 bits contain the flag value.

 * @param logfile_data The raw data taken from the logfile.
 * @param data (output) The data related to the flag.
 * @param part_type (output) The particle type raising this flag.
 *
 * @return The flag extracted from the raw data.
 */

INLINE static enum csds_special_flags csds_unpack_flags_and_data(
    uint32_t logfile_data, int *flag_data, int *part_type) {
  *part_type = (logfile_data & 0xFF);
  *flag_data = (logfile_data & 0xFFFF00) >> 8;

  const int tmp = ((logfile_data & 0xFF000000) >> 24);
  const enum csds_special_flags flag = (enum csds_special_flags)tmp;

#ifdef CSDS_DEBUG_CHECKS
  if (*part_type >= csds_type_count || *part_type < 0) {
    csds_error("Invalid particle type found:" << *part_type);
  }
#endif
  return flag;
}

/**
 * @brief Read the special flag of a particle (of any type) in the log file.
 *
 * @param offset offset of the record to read.
 * @param header (out) The header of the record.
 * @param data (out) The data of the flag.
 * @param part_type (out) The type of particle raising this flag.
 *
 * @return The special flag.
 */
INLINE static enum csds_special_flags csds_particle_read_special_flag(
    size_t offset, struct record_header &header, int *data, int *part_type,
    LogFile &map) {

  /* Read the record's mask. */
  offset = map.ReadRecordHeader(offset, header);

#ifdef CSDS_DEBUG_CHECKS
  /* Check if it is not a time record. */
  if (header.mask == CSDS_TIMESTAMP_MASK) {
    csds_error("Cannot read a particle from timestep record."
               << header.mask << " " << header.offset);
  }
#endif

  /* Read the special flag */
  uint32_t packed_data = 0;
  offset = map.ReadData(offset, CSDS_SPECIAL_FLAGS_SIZE, &packed_data);

  return csds_unpack_flags_and_data(packed_data, data, part_type);
}

/**
 * @brief Interpolate a field of the particle at the given time.
 *
 * @param before Pointer to the #csds_reader_field at a time < t.
 * @param after Pointer to the #csds_reader_field at a time > t.
 * @param otuput Pointer to the output value.
 * @param time_before Time of field_before (< t).
 * @param time_after Time of field_after (> t).
 * @param time Requested time.
 * @param field The field to reconstruct.
 * @param params The simulation's #Parameters.
 */
INLINE static void csds_particle_interpolate_field(
    const double time_before, const struct csds_reader_field &before,
    const double time_after, const struct csds_reader_field &after,
    void *__restrict__ output, const double time, const Field &field,
    const Parameters &params) {

#ifdef CSDS_DEBUG_CHECKS
  if (time_before > time || time_after < time) {
    csds_error(std::setprecision(10) << "Trying to extrapolate: " << time_before
                                     << " < " << time << " < " << time_after);
  }
#endif

  switch (field.GetField().GetFieldEnum()) {
      /* Coordinates */
    case field_enum_coordinates:
      interpolate_quintic<double, float, float>(
          time_before, before, time_after, after, output, time,
          /* dimension */ 3, params.IsPeriodic(), params);
      break;

      /* Velocities */
    case field_enum_velocities:
      interpolate_cubic<float, float>(
          time_before, before, time_after, after, output, time,
          /* dimension= */ 3, /* periodic= */ 0, params);
      break;

      /* Accelerations */
    case field_enum_accelerations:
      interpolate_linear<float>(time_before, before, time_after, after, output,
                                time,
                                /* dimension= */ 3, /* periodic= */ 0, params);
      break;

      /* Interpolation of float */
    case field_enum_masses:
    case field_enum_smoothing_lengths:
    case field_enum_internal_energies:
    case field_enum_entropies:
    case field_enum_densities:
      interpolate_linear<float>(time_before, before, time_after, after, output,
                                time, /* dimension */ 1, /* periodic= */ 0,
                                params);
      break;

      /* Constant float term */
    case field_enum_birth_time:
      if (*(float *)after.field != *(float *)before.field) {
        csds_error("Interpolating different particles.");
      }
      *(float *)output = *(float *)after.field;
      break;

      /* Particle IDs */
    case field_enum_particles_ids:
      /* Ensure to have the same particle */
      if (*(long long *)after.field != *(long long *)before.field) {
        csds_error("Interpolating different particles.");
      }
      *(long long *)output = *(long long *)after.field;
      break;

      /* Gear star formation */
    case field_enum_gear_star_formation: {
      interpolate_linear<float>(time_before, before, time_after, after, output,
                                time, 2, /* periodic */ 0, params);
      float *x = (float *)output;
      const float *bef = (float *)before.field;
      const float *aft = (float *)after.field;

      /* ProgenitorIDs */
      long long *id = (long long *)(x + 2);
      *id = *(long long *)(aft + 2);
      if (*id != *(long long *)(bef + 2)) {
        csds_error("Interpolating different particles.");
      }
      break;
    }

      /* GEAR chemistry (part) */
    case field_enum_gear_chemistry_part:
      interpolate_linear<double>(time_before, before, time_after, after, output,
                                 time, 2 * GEAR_CHEMISTRY_ELEMENT_COUNT,
                                 /* periodic */ 0, params);
      break;

      /* GEAR chemistry (spart) */
    case field_enum_gear_chemistry_spart:
      interpolate_linear<double>(time_before, before, time_after, after, output,
                                 time, GEAR_CHEMISTRY_ELEMENT_COUNT,
                                 /* periodic */ 0, params);
      break;

      /* Sphenix grouped fields */
    case field_enum_sphenix_secondary_fields: {
      /* Get some variables */
      float *x = (float *)output;
      const float *bef = (float *)before.field;
      const float *aft = (float *)after.field;

      /* Entropy + pressure + Viscosity + Diffusion + Laplacian*/
      const int n_linear = 5;
      interpolate_linear<float>(time_before, before, time_after, after, x, time,
                                n_linear, /* periodic */ 0, params);

      /* Div v */
      x[n_linear] = interpolate_cubic_hermite_spline<float, float>(
          time_before, bef[n_linear], bef[n_linear + 1], time_after,
          aft[n_linear], aft[n_linear + 1], time);

      /* d Div v / dt */
      x[n_linear + 1] = interpolate_linear_spline<float>(
          time_before, bef[n_linear + 1], time_after, aft[n_linear + 1], time);
      break;
    }

    default:
      csds_error("Field " << field.GetName() << " not implemented");
  }
}

#endif  // CSDS_PARTICLE_H
