/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include the corresponding header */
#include "index_file.hpp"

/* Include the standard headers */
#include <errno.h>
#include <fcntl.h>

/* Include local headers */
#include "mapped_file.hpp"

/**
 * @brief Read the index file header.
 *
 * @param filename The filename of the index file.
 * @param Should the file be sorted?
 * @param verbose The verbose level
 */
IndexFile::IndexFile(const std::string filename, bool sorted, int verbose)
    : MappedFile(filename, /* read_only */ true, /* track_mmap */ false) {

  /* Open the file */
  if (verbose > 1) {
    message("Reading " << filename);
  }

  /* Read times */
  ReadData(csds_index_time_offset, csds_index_time_size, &mTime);
  ReadData(csds_index_integer_time_offset, csds_index_integer_time_size,
           &mIntegerTime);

  /* Read the number of particles. */
  ReadData(csds_index_npart_offset, csds_index_npart_size, mNParts.data());

  /* Read whether the file is sorted. */
  ReadData(csds_index_is_sorted_offset, csds_index_is_sorted_size, &mIsSorted);

  /* Compute the position of the history of new particles. */
  size_t count = 0;
  for (int i = 0; i < csds_type_count; i++) {
    count += mNParts[i];
  }
  count *= sizeof(struct index_data);

  /* Read the number of new particles. */
  ReadData(csds_index_data_offset + count, csds_index_npart_size,
           mNPartsCreated.data());

  /* Compute the position of the history of particles removed. */
  size_t count_new = 0;
  for (int i = 0; i < csds_type_count; i++) {
    count_new += mNPartsCreated[i];
  }
  count_new *= sizeof(struct index_data);

  /* Read the number of particles removed. */
  ReadData(csds_index_data_offset + count + count_new + csds_index_npart_size,
           csds_index_npart_size, mNPartsRemoved.data());

  /* Sort the file if required */
  if (sorted) {
    SortFile(filename, verbose);
  }
}

/**
 * @brief Write that the file is sorted.
 *
 * WARNING The file must be mapped.
 */
void IndexFile::WriteSorted() {
  /* Set the value */
  const char is_sorted = 1;

  /* Write the value */
  WriteData(csds_index_is_sorted_offset, csds_index_is_sorted_size, &is_sorted);

  mIsSorted = true;
}

/**
 * @brief Check if a time array is written at the end.
 */
bool IndexFile::ContainsTimeArray() {
  /* Only the first index file should have a time array */
  if (mIntegerTime != 0) {
    csds_error("Only the first index file can have a time array.");
  }

  /* Get the file size */
  const size_t file_size = GetFileSize();

  /* Get the position after the previous array. */
  char *ret = (char *)GetRemovedHistory(csds_type_count);
  const size_t before_time_array = ret - mMap;

  return file_size != before_time_array;
}

/**
 * @param filename The index filename.
 * @param verbose the verbose level
 */
void IndexFile::SortFile(const std::string filename, int verbose) {
  /* Un-map previous file */
  Close();

  /* Check if need to sort the file */
  if (!mIsSorted) {
    if (verbose > 1) {
      message("Sorting the index file.");
    }
    /* Map the index file */
    Open(filename, /* read_only */ false, /* track_mmap */ false);
    /* Sort the file */
    for (int i = 0; i < csds_type_count; i++) {
      if (mNParts[i] != 0) {
        struct index_data *data = GetData((part_type)i);
        std::sort(data, data + mNParts[i], CompareIndexDataId);
      }
    }

    /* Write that the file is sorted */
    WriteSorted();

    /* Close the file in order to open it as read only */
    Close();

    if (verbose > 1) {
      message("Sorting done.");
    }
  }

  /* Map the index file */
  Open(filename, /* read_only */ true, /* track_mmap */ false);
}

/**
 * @brief Cleanup the memory of a csds_index
 *
 * @param index The #csds_index.
 */
IndexFile::~IndexFile() { Close(); }
