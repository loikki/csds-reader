/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/* Include standard library */
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>

/* Include corresponding header */
#include "reader.hpp"

/* Include CSDS headers */
#include "field.hpp"
#include "openmp.hpp"
#include "parameters.hpp"

using namespace std;

Reader::Reader(const string basename, int verbose, int number_index,
               bool restart_init, bool use_cache) {
  if (verbose > 1) message("Initializing the reader.");

  /* Set the variable to the default values */
  mTime.time = -1.;
  mTime.int_time = 0;
  mTime.time_offset = 0;
  mUseCache = use_cache;
  mBasename = basename;
  mIndex.index_prev = NULL;
  mIndex.index_next = NULL;

  /* Initialize the reader variables. */
  mVerbose = verbose;

  /* Generate filename */
  string params_filename = mBasename;
  /* Remove the rank number */
  params_filename.erase(params_filename.size() - 5, params_filename.size());
  params_filename += ".yml";
  mParams = Parameters(params_filename, mVerbose);

  /* Initialize the log file. */
  mLog = new LogFile(basename, /* only_header */ 0, mVerbose);

  /* Initialize the index files and creates them if needed */
  InitIndex(number_index, restart_init);

  /* Save the time array */
  string filename = basename + "_0000.index";
  mLog->SaveTimeArray(filename);

  if (verbose > 1) message("Initialization done.");
}

void Reader::InitIndex(int number_index, int restart) {
  /* Count the number of files */
  int count = 0;
  while (1) {
    string filename = GetIndexName(count);

    /* Check if file exists */
    std::ifstream file(filename);
    bool ret = file.good();
    file.close();
    if (ret) {
      count++;
    } else {
      break;
    }
  }

  /* Ensures that we do not have an unexpected situation */
  if (number_index == 1 || (count == 1 && !restart)) {
    csds_error(
        "The CSDS cannot run with only one index file."
        " Did you forget to restart their generation?");
  }

  /* Check if need to generate the index files or not. */
  if ((count != number_index && restart) || count == 0) {
    GenerateIndexFiles(number_index, count);
    count = number_index;
  } else if (mVerbose > 0 && count != number_index) {
    message("Found " << count << "index files. If you wish"
                     << "to have the correct number of files,"
                     << "please delete the old ones.");
  }

  mIndex.n_files = count;

  /* Initialize the arrays */
  mIndex.times.reserve(count);

  /* Get the information contained in the headers */
  for (int i = 0; i < mIndex.n_files; i++) {
    string filename = GetIndexName(i);

    /* Read the header */
    mIndex.index_prev = new IndexFile(filename, /* sorted */ false, mVerbose);

    /* Save the required information */
    mIndex.times.push_back(mIndex.index_prev->GetTime());
  }
}

Reader::~Reader() {
  /* Free the log. */
  delete mLog;

  /* Free the index */
  if (mTime.time != -1.) {
    delete mIndex.index_prev;
    delete mIndex.index_next;
  }
}

string Reader::GetIndexName(int count) {
  string filename = mBasename + "_";
  string s_count = to_string(count);
  filename.append(4 - s_count.length(), '0');
  filename += s_count + ".index";
  return filename;
}

void Reader::SetTime(double time) {
  /* Set the time */
  mTime.time = time;

  unsigned int left = 0;
  unsigned int right = mIndex.n_files - 1;

  const TimeArray &times = mLog->GetTimeArray();

  /* Check if the time is meaningful */
  if (mIndex.times[left] > time || mIndex.times[right] < time)
    csds_error("The requested time "
               << time << " is not within the index limits ["
               << mIndex.times[left] << ", " << mIndex.times[right] << "]");

  /* Get the index */
  auto it = std::lower_bound(mIndex.times.begin(), mIndex.times.end(), time);
  size_t index_ind = it - mIndex.times.begin();

  /* We need to time before, not after */
  /* Fix: If we request the last simulation time, we want the previous and next
     index files to exist (i.e. be non NULL). */
  if ((*it != time) or (*it == GetTimeEnd())) {
    index_ind--;
  }

  /* Generate the filename */
  string filename_prev = GetIndexName(index_ind);
  string filename_next = GetIndexName(index_ind + 1);

  /* Check if the file is already mapped */
  if (mIndex.index_prev != NULL) {
    delete mIndex.index_prev;
    mIndex.index_prev = NULL;
  }
  if (mIndex.index_next != NULL) {
    delete mIndex.index_next;
    mIndex.index_next = NULL;
  }

  /* Read the file */
  mIndex.index_prev = new IndexFile(filename_prev, /* sorted */ true, mVerbose);

  if ((int)index_ind != mIndex.n_files - 1) {
    mIndex.index_next =
        new IndexFile(filename_next, /* sorted */ true, mVerbose);
  }

  /* Get the offset of the time chunk */
  size_t ind = times.GetIndexFromTime(time);
  /* We need to time record just above the current time */
  ind++;

  /* For the final time, we wish to use the time record just before
     writing all the particles */
  if (ind == times.Size() - 1) {
    ind--;
  }

  /* Save the values */
  mTime.int_time = times[ind].int_time;
  mTime.time_offset = times[ind].offset;
}

size_t Reader::CountNumberNewParticles(enum part_type part_type) {

  /* Do we still have a last index file */
  if (mIndex.index_next == NULL) return 0;

  const size_t threshold = mTime.time_offset;

  /* Get the last created particle */
  return mIndex.index_next->GetCurrentNumberCreatedParticles(part_type,
                                                             threshold);
}

size_t Reader::CountNumberRemovedParticles(enum part_type part_type) {
  /* Do we still have a last index file */
  if (mIndex.index_next == NULL) return 0;

  const size_t threshold = mTime.time_offset;

  return mIndex.index_next->GetCurrentNumberRemovedParticles(part_type,
                                                             threshold);
}

void Reader::GetNumberParticles(
    std::array<uint64_t, csds_type_count> &n_parts,
    const std::array<bool, csds_type_count> &read_types) {

  const bool final_index = mIndex.index_prev->GetTime() == GetTimeEnd();

  for (size_t i = 0; i < n_parts.size(); i++) {
    /* Should we skip this type of particles? */
    if (!read_types[i]) {
      n_parts[i] = 0;
      continue;
    }

    /* The final index is slightly different */
    if (final_index) {
      if (mIndex.index_prev->GetNumberParticles((part_type)i) != 0) {
        csds_error("Found some existing particles in the last index file");
      }
      /* Here we simply need to count the number of particles removed between
         the two last time records */
      size_t size_max =
          mIndex.index_prev->GetNumberRemovedParticles((part_type)i);
      size_t removed = mIndex.index_prev->GetCurrentNumberRemovedParticles(
          (part_type)i, mTime.time_offset);
      n_parts[i] = size_max - removed;

      /* In between the last two records, we also store the particles active
         in the last step, therefore we need to count the particles created */
      size_max = mIndex.index_prev->GetNumberCreatedParticles((part_type)i);
      size_t created = mIndex.index_prev->GetCurrentNumberCreatedParticles(
          (part_type)i, mTime.time_offset);
      n_parts[i] -= size_max - created;
    }
    /* Normal case */
    else {
      /* Count the number of particles present in the last index file. */
      const uint64_t count_prev =
          mIndex.index_prev->GetNumberParticles((part_type)i);
      /* Count the number of particles that have been created since last index.
       */
      const uint64_t count_new = CountNumberNewParticles((enum part_type)i);
      /* Count the number of particles that have been removed since last index.
       */
      const uint64_t count_removed =
          CountNumberRemovedParticles((enum part_type)i);
      n_parts[i] = (count_prev + count_new) - count_removed;
    }
  }
}

int Reader::ReadParticle(double time, std::vector<CsdsArray> &output,
                         size_t offset_from_index, size_t output_index,
                         enum part_type type, int &number_jumps) {

  /* Get global variables */
  const Header &h = mLog->GetHeader();
  size_t offset_time = mTime.time_offset;

  const TimeArray &times = mLog->GetTimeArray();

  /* Index in the cache */
  int64_t cache_index = -1;

  /* Check if the offsets are correct. */
  if (offset_from_index > offset_time) {
    csds_error("Last offset is larger than the requested time.");
  }

  /* Record's header information */
  struct record_header header = {0, 0};

  /* The offset used for all the manipulations */
  size_t offset = offset_from_index;
  number_jumps = 0;

  /* Find the data for the previous record.
     As we start from a full record,
     no need to check if the field is found.
  */
  while (offset < offset_time) {
    number_jumps += 1;
    /* Read the particle. */
    mLog->ReadRecordHeader(offset, header);

    /* Is the particle removed from the logfile? */
    if (header.mask & CSDS_SPECIAL_FLAGS_MASK) {
      int data = 0;
      int part_type = 0;
      enum csds_special_flags flag = csds_particle_read_special_flag(
          offset, header, &data, &part_type, *mLog);

#ifdef CSDS_DEBUG_CHECKS
      if (part_type != type) {
        csds_error("Found particles that do not correspond.");
      }
#endif

      if (flag == csds_flag_change_type || flag == csds_flag_mpi_exit ||
          flag == csds_flag_delete) {
        return 1;
      }
    }

    /* Check if there is a next record (avoid infinite loop). */
    if (header.offset == 0) {
      csds_error("No next record found.");
    }

    /* Go to the next record. */
    offset += header.offset;
  }

  /* Set the next record */
  size_t offset_next = offset;
  /* Go back before offset_time */
  offset -= header.offset;

  /* Loop over each field. */
  for (size_t field = 0; field < output.size(); field++) {
    const Field &full_field = output[field].GetField();
    const SingleField &current_field = full_field.GetField();
    const SingleField &current_first = full_field.GetFirstDerivative();
    const SingleField &current_second = full_field.GetSecondDerivative();
    const int size_first =
        current_first == field_enum_none ? 0 : current_first.GetSize();
    const int size_second =
        current_second == field_enum_none ? 0 : current_second.GetSize();
    char *current_output = output[field][output_index];

    /* Read the field */
    csds_particle_read_field(offset, current_output, full_field,
                             /* derivative */ 0, header, h.GetFields()[type],
                             *mLog);

    /* Deal with the first derivative. */
    int first_found =
        current_first != field_enum_none && current_first & header.mask;
    char first_deriv[size_first];

    if (first_found) {
      /* Read the first derivative */
      csds_particle_read_field(offset, first_deriv, full_field,
                               /* derivative */ 1, header, h.GetFields()[type],
                               *mLog);
    }

    /* Deal with the second derivative. */
    int second_found =
        current_second != field_enum_none && current_second & header.mask;
    char second_deriv[size_second];

    if (second_found) {
      /* Read the first derivative */
      csds_particle_read_field(offset, second_deriv, full_field,
                               /* derivative */ 2, header, h.GetFields()[type],
                               *mLog);
    }

    /* Get the time. */
    // TODO reduce search interval
    double time_before = times.GetRecordFromOffset(offset).time;

    /* Get the mask */
    mLog->ReadRecordHeader(offset_next, header);

    /* Output after the requested time. */
    char output_after[current_field.GetSize()];

    /* Read the field */
    csds_particle_read_field(offset_next, output_after, full_field,
                             /* derivative */ 0, header, h.GetFields()[type],
                             *mLog);

    /* Deal with the first derivative. */
    char first_deriv_after[size_first];

    /* Did we find the derivative before and in this record? */
    first_found = current_first != field_enum_none && first_found &&
                  current_first & header.mask;
    if (first_found) {
      /* Read the first derivative */
      csds_particle_read_field(offset_next, first_deriv_after, full_field,
                               /*derivative*/ 1, header, h.GetFields()[type],
                               *mLog);
    }

    /* Deal with the second derivative. */
    char second_deriv_after[size_second];

    /* Did we find the derivative before and in this record? */
    second_found = current_second != field_enum_none && second_found &&
                   current_second & header.mask;
    if (second_found) {
      /* Read the second derivative */
      csds_particle_read_field(offset_next, second_deriv_after, full_field,
                               /* derivative */ 2, header, h.GetFields()[type],
                               *mLog);
    }

    /* Get the time. */
    // TODO reduce search interval
    double time_after = times.GetRecordFromOffset(offset_next).time;

    /* Deal with the derivatives */
    struct csds_reader_field before;
    struct csds_reader_field after;
    before.field = current_output;
    before.first_deriv = first_found ? first_deriv : NULL;
    before.second_deriv = second_found ? second_deriv : NULL;
    after.field = output_after;
    after.first_deriv = first_found ? first_deriv_after : NULL;
    after.second_deriv = second_found ? second_deriv_after : NULL;

    /* Interpolate the data. */
    csds_particle_interpolate_field(time_before, before, time_after, after,
                                    current_output, time, full_field, mParams);

    /* Now deal with the cache */
    if (mUseCache) {
      cache_index =
          mCache[type].Save(cache_index, time_before, before, time_after, after,
                            offset_next, full_field);
    }
  }

  return 0;
}

void Reader::ReadAllParticlesSingleType(
    double time, std::vector<CsdsArray> &output,
    const std::array<uint64_t, csds_type_count> &n_part, enum part_type type) {

  const bool have_next_index = mIndex.index_next != NULL;

  /* Count the number of previous parts for the shift in output */
  uint64_t prev_npart = 0;
  for (int i = 0; i < type; i++) {
    prev_npart += n_part[i];
  }

  /* Create some information from the index files */
  const size_t size_index = mIndex.index_prev->GetNumberParticles(type);
  const size_t size_history = CountNumberNewParticles(type);

  struct index_data *data = mIndex.index_prev->GetData(type);
  struct index_data *data_created =
      have_next_index ? mIndex.index_next->GetCreatedHistory(type) : NULL;

  /* Current index to read */
  size_t current_index = 0;
  size_t particles_read = 0;
  int local_counter = 0;
  size_t total_number_jumps = 0;
  const int local_update_limit = 1000;
  const chrono::high_resolution_clock::time_point init =
      chrono::high_resolution_clock::now();

  /* Read the particles */
#pragma omp parallel for firstprivate(local_counter) \
    reduction(+ : total_number_jumps)
  for (size_t i = 0; i < n_part[type]; i++) {

    /* Try to find a particle that is not removed */
    int particle_removed = 1;
    while (particle_removed) {

      size_t local_index = 0;
#pragma omp atomic capture
      local_index = current_index++;

      /* Are we reading the history? */
      int reading_history = local_index >= size_index;

      /* Check if we still have some particles available. */
      if (reading_history && local_index == size_history + size_index) {
        csds_error("The CSDS was not able to find enough particles.");
      }

      /* Get the offset */
      const size_t current =
          reading_history ? local_index - size_index : local_index;
      size_t offset =
          reading_history ? data_created[current].offset : data[current].offset;

      /* Get the offset of the particle */
      int number_jumps = 0;
      particle_removed =
          ReadParticle(time, output, offset, i, type, number_jumps);

      /* Update the number of jumps */
      if (!particle_removed) {
        total_number_jumps += number_jumps;
      }
    }

    /* Do we need to print something? */
    if (!(mVerbose > 0)) continue;

    /* Update the counters */
    local_counter++;
    if (local_counter < local_update_limit) continue;

      /* Add the local counter to the global one */
#pragma omp atomic update
    particles_read += local_counter;
    local_counter = 0;

    /* Only the master is printing */
    const int current_thread = csds_get_thread_num();
    if (current_thread != 0) continue;

    float ratio = (float)particles_read / (float)n_part[type];

    /* Print the message */
    tools_print_progress(100 * ratio, init, "Reading particles");
  }

  /* Finish the progess bar */
  if (mVerbose > 0) {
    printf("\n");
  }

  /* Print the number of jumps if required */
  if (mVerbose > 0 || mVerbose == CSDS_VERBOSE_TIMERS) {
    float avg =
        (float)total_number_jumps / (float)(output.size() * n_part[type]);
    message("Average number of jumps for " << part_type_names[type] << ": "
                                           << avg);
  }
}

void Reader::ReadAllParticles(
    double time, std::vector<CsdsArray> &output,
    const std::array<uint64_t, csds_type_count> &n_part) {

  const chrono::high_resolution_clock::time_point init =
      chrono::high_resolution_clock::now();

  /* Prepare the cache */
  if (mUseCache) {
    for (int i = 0; i < csds_type_count; i++) {
      mCache[i] = Cache(n_part[i], output, mParams.GetCacheOverAllocation(),
                        mParams.GetCacheInitialSize()[i], mTime.time_offset);
    }
  }

  /* Read the particles */
  for (int i = 0; i < csds_type_count; i++) {
    if (n_part[i] != 0) {
      ReadAllParticlesSingleType(time, output, n_part, (part_type)i);
    }
  }

  /* Print the time */
  if (mVerbose > 0 || mVerbose == CSDS_VERBOSE_TIMERS)
    message("took " << GetDeltaTime(init) << "ms");
}

void Reader::ReadParticlesFromIdsSingleType(
    double time, std::vector<CsdsArray> &output,
    std::array<uint64_t, csds_type_count> &n_part, enum part_type type,
    long long *ids) {

  /* Count the number of previous parts for the shift in output */
  uint64_t prev_npart = 0;
  for (int i = 0; i < type; i++) {
    prev_npart += n_part[i];
  }

  /* Read the particles */
  size_t total_number_jumps = 0;
  size_t n_part_removed = 0;
  size_t index_output = 0;

  /* Initialize the temporary output */
  std::vector<CsdsArray> tmp;
  for (size_t field = 0; field < output.size(); field++) {
    const int num_threads = csds_get_max_threads();
    tmp.emplace_back(num_threads, output[field].GetField());
  }

#pragma omp parallel for reduction(+ : total_number_jumps, n_part_removed)
  for (size_t i = 0; i < n_part[type]; i++) {

    /* Get the offset */
    size_t offset = 0;
    bool found = mIndex.index_prev->GetOffset((int64_t)ids[i], type, offset);

    /* Deal with the particles not found */
    if (!found) {
      n_part_removed++;
      continue;
    }

    /* Read the particle */
    const int thread = csds_get_thread_num();
    int number_jumps = 0;
    int particle_removed = ReadParticle(
        time, tmp, offset, /* output_index */ thread, type, number_jumps);

    total_number_jumps += number_jumps;

    /* Did we found one? */
    if (particle_removed) {
      n_part_removed++;
    } else {
      /* Get the output index */
      size_t local_index = 0;
#pragma omp atomic capture
      {
        local_index = index_output;
        index_output++;
      }
      /* Write the particle into the output */
      for (size_t field = 0; field < output.size(); field++) {
        memcpy(output[field][local_index], tmp[field][thread],
               output[field].ElementSize());
      }
    }
  }

  /* Update the number of particles */
  n_part[type] -= n_part_removed;

  /* Print the number of jumps if required */
  if (mVerbose > 1 || mVerbose == CSDS_VERBOSE_TIMERS) {
    float avg =
        (float)total_number_jumps / (float)(output.size() * n_part[type]);
    message("Average number of jumps for " << part_type_names[type] << ": "
                                           << avg);
  }
}

void Reader::ReadParticlesFromIds(
    double time, std::vector<CsdsArray> &output,
    std::array<uint64_t, csds_type_count> &n_part,
    std::array<long long *, csds_type_count> &ids) {

  const chrono::high_resolution_clock::time_point init =
      chrono::high_resolution_clock::now();

  /* Prepare the cache */
  if (mUseCache) {
    for (int i = 0; i < csds_type_count; i++) {
      mCache[i] = Cache(n_part[i], output, mParams.GetCacheOverAllocation(),
                        mParams.GetCacheInitialSize()[i], mTime.time_offset);
    }
  }

  /* Read the particles */
  for (int i = 0; i < csds_type_count; i++) {
    if (n_part[i] != 0) {
      ReadParticlesFromIdsSingleType(time, output, n_part, (part_type)i,
                                     ids[i]);
    }
  }

  /* Print the time */
  if (mVerbose > 0 || mVerbose == CSDS_VERBOSE_TIMERS)
    message("took " << GetDeltaTime(init) << "ms");
}

double Reader::GetTimeBegin() { return mLog->GetTimeArray()[0].time; }

double Reader::GetTimeEnd() {
  const TimeArray &times = mLog->GetTimeArray();
  const size_t ind = times.Size();
  return times[ind - 1].time;
}

size_t Reader::GetNextOffsetFromTime(double time) {
  const TimeArray &times = mLog->GetTimeArray();
  size_t ind = times.GetIndexFromTime(time);
  /* We do not want to have the sentiel */
  if (times.Size() - 2 == ind) {
    ind -= 1;
  }
  return times[ind + 1].offset;
}

bool Reader::UpdateSingleParticle(const size_t &index,
                                  const enum part_type &type,
                                  const double &time, const size_t &offset_time,
                                  const size_t &field_index, void *output) {

  Cache &cache = mCache[type];
  const TimeArray &times = mLog->GetTimeArray();

  /* Get the time */
  double time_before = cache.GetTimeBefore(index);
  double time_after = cache.GetTimeAfter(index);
  size_t offset_next = cache.GetOffsetAfter(index);

  /* Update the cache if needed */
  if (offset_next < offset_time) {
    struct record_header header;

    /* Find the correct record */
    while (offset_next < offset_time) {
      /* Read the particle. */
      mLog->ReadRecordHeader(offset_next, header);

      /* Is the particle removed? */
      if (header.mask & CSDS_SPECIAL_FLAGS_MASK) {
        int data = 0;
        int part_type = 0;
        enum csds_special_flags flag = csds_particle_read_special_flag(
            offset_next, header, &data, &part_type, *mLog);

#ifdef CSDS_DEBUG_CHECKS
        if (part_type != type) {
          csds_error("Found particles that do not correspond.");
        }
#endif
        if (flag == csds_flag_change_type || flag == csds_flag_mpi_exit ||
            flag == csds_flag_delete) {
          return true;
        }
      }

      /* Check if there is a next record (avoid infinite loop). */
      if (header.offset == 0) {
        csds_error("No next record found.");
      }

      offset_next += header.offset;
    }

    /* Get a few variables for later */
    size_t offset_before = offset_next - header.offset;
    time_before = times.GetRecordFromOffset(offset_before).time;
    time_after = times.GetRecordFromOffset(offset_next).time;

    /* Update the cache */
    cache.UpdateTime(index, time_before, time_after, offset_next);

    /* Get the fields for the next record */
    const Header &h = mLog->GetHeader();
    for (int i = 0; i < cache.GetNumberFields(); i++) {
      const Field &field = cache.GetFieldFromIndex(i);
      struct csds_reader_field after = cache.GetFieldAfter(index, i);
      struct csds_reader_field before = cache.GetFieldBefore(index, i);

      /* Read the field */
      csds_particle_read_field(offset_before, before.field, field,
                               /* derivative */ 0, header, h.GetFields()[type],
                               *mLog);
      csds_particle_read_field(offset_next, after.field, field,
                               /* derivative */ 0, header, h.GetFields()[type],
                               *mLog);

      /* Read the first derivative */
      if (field.HasFirstDerivative()) {
        csds_particle_read_field(offset_before, before.first_deriv, field,
                                 /* derivative */ 1, header,
                                 h.GetFields()[type], *mLog);
        csds_particle_read_field(offset_next, after.first_deriv, field,
                                 /* derivative */ 1, header,
                                 h.GetFields()[type], *mLog);
      }

      /* Read the second derivative */
      if (field.HasSecondDerivative()) {
        csds_particle_read_field(offset_before, before.second_deriv, field,
                                 /* derivative */ 2, header,
                                 h.GetFields()[type], *mLog);
        csds_particle_read_field(offset_next, after.second_deriv, field,
                                 /* derivative */ 2, header,
                                 h.GetFields()[type], *mLog);
      }
    }
  }

  /* Get the field */
  const Field &field = cache.GetFieldFromIndex(field_index);

  /* Generate the interpolation structure */
  struct csds_reader_field before = cache.GetFieldBefore(index, field_index);
  struct csds_reader_field after = cache.GetFieldAfter(index, field_index);

  /* Interpolate the data. */
  csds_particle_interpolate_field(time_before, before, time_after, after,
                                  output, time, field, mParams);

  return false;
}

void Reader::UpdateParticlesSingleType(
    double time, std::vector<CsdsArray> &output,
    const std::array<uint64_t, csds_type_count> &n_part, enum part_type type) {

  const bool have_next_index = mIndex.index_next != NULL;

  /* Count the number of previous parts for the shift in output */
  size_t prev_npart = 0;
  for (int i = 0; i < type; i++) {
    prev_npart += n_part[i];
  }

  /* Get the time offset of the current time */
  size_t offset_time = mTime.time_offset;

  /* Get the field index */
  std::vector<int> field_index_in_cache(output.size());
  for (size_t field = 0; field < output.size(); field++) {
    field_index_in_cache[field] =
        mCache[type].GetFieldIndex(output[field].GetField());
  }

  /* Declare a few variables for the loop */
  const size_t initial_size_cache = mCache[type].Size();

  size_t number_particle_removed = 0;
  std::vector<size_t> particle_removed(0.05 * initial_size_cache);

  size_t output_index = prev_npart;

  message(initial_size_cache);

  /* Update the particles */
#pragma omp parallel for
  for (size_t cache_index = 0; cache_index < initial_size_cache;
       cache_index++) {
    size_t current_output_index = 0;
    bool removed = false;

    /* Do all the fields */
    for (size_t field = 0; field < output.size(); field++) {
      /* Generate a temporary output */
      const size_t field_size = output[field].GetField().GetFieldSize();
      char temporary_output[field_size];

      /* Update the particle */
      removed =
          UpdateSingleParticle(cache_index, type, time, offset_time,
                               field_index_in_cache[field], temporary_output);

      /* Move to the next particle if this one is removed */
      if (removed) {
#ifdef CSDS_DEBUG_CHECKS
        if (field != 0) {
          csds_error("Should not happen");
        }
#endif
        /* Increase the array size if needed */
        if (number_particle_removed == particle_removed.capacity()) {
#pragma omp critical
          if (number_particle_removed == particle_removed.capacity())
            particle_removed.resize(2 * particle_removed.size());
        }

        /* Add an element to the array of removed particles */
        size_t current;
#pragma omp atomic capture
        {
          current = number_particle_removed;
          number_particle_removed++;
        }
        particle_removed[current] = cache_index;
        break;
      }

      /* Get the output index */
      if (field == 0) {
#pragma omp atomic capture
        {
          current_output_index = output_index;
          output_index++;
        }
      }

#ifdef CSDS_DEBUG_CHECKS
      /* Ensures that we get a correct index */
      if (current_output_index >= prev_npart + initial_size_cache) {
        csds_error("Something wrong happened with the slice");
      }
#endif

      /* Copy back into the output */
      memcpy(output[field][current_output_index], temporary_output, field_size);
    }
  }

  /* Remove the particles from the cache */
  // TODO this can be merged with the addition of particles
  particle_removed.resize(number_particle_removed);
  std::sort(particle_removed.begin(), particle_removed.end());
  for (size_t i = 0; i < number_particle_removed; i++) {
    // TODO thread safe
    size_t ind = number_particle_removed - 1 - i;
    ind = particle_removed[ind];
    mCache[type].RemoveParticle(ind);
  }

#ifdef CSDS_DEBUG_CHECKS
  /* Check if we have the correct next offsets  */
  for (size_t i = 0; i < mCache[type].Size(); i++) {
    if (mCache[type].GetOffsetAfter(i) < offset_time) {
      csds_error("Error within the cache");
    }
  }
#endif

  /* Now look for the new particles */
  mCache[type].SetCurrentIndex(mCache[type].Size());

  /* Check if we need to update the cache with the previous index file */
  const TimeArray &times = mLog->GetTimeArray();
  size_t offset_index = times.GetIndexFromTime(mIndex.index_prev->GetTime());
  offset_index = times[offset_index].offset;
  const bool do_previous = mCache[type].GetLastTimeOffset() < offset_index;

  /* Get the histories */
  // TODO use a pointer to IndexFile
  struct index_data *prev_created = mIndex.index_prev->GetCreatedHistory(type);
  struct index_data *next_created =
      have_next_index ? mIndex.index_next->GetCreatedHistory(type) : NULL;

  /* Go to the last particle created */
  struct index_data *index_data = do_previous ? prev_created : next_created;
  const size_t size_next =
      have_next_index ? mIndex.index_next->GetNumberCreatedParticles(type) : 0;
  size_t size_max = do_previous
                        ? mIndex.index_prev->GetNumberCreatedParticles(type)
                        : size_next;

  /* Get the last created particle */
  size_t center = do_previous
                      ? mIndex.index_prev->GetCurrentNumberCreatedParticles(
                            type, mCache[type].GetLastTimeOffset())
                      : mIndex.index_next->GetCurrentNumberCreatedParticles(
                            type, mCache[type].GetLastTimeOffset());

  /* Create a temporary buffer */
  std::vector<CsdsArray> buffers;
  buffers.reserve(output.size());
  for (auto const &current : output) {
    buffers.emplace_back(1, current.GetField());
  }

  /* Now read all the missing particles */
  for (size_t i = center; i < size_max; i++) {
    /* Stop as soon as we reach the required offset */
    if (index_data[i].offset > offset_time) break;

    /* Now read and store the particle in the cache */
    int number_jumps = 0;
    int removed = ReadParticle(time, buffers, index_data[i].offset,
                               /* output_index */ 0, type, number_jumps);

    /* Deal with the case where the particle is removed */
    if (removed) {
      /* No need to remove from the cache,
         the particle was not written into it. */
      continue;
    }

    /* Write the particle back into the array */
    for (size_t field = 0; field < output.size(); field++) {
      const Field &current = output[field].GetField();
      char *output_buffer = output[field][output_index];
      memcpy(output_buffer, buffers[field][0], current.GetFieldSize());
    }

    /* Update the counter */
    output_index++;
  }

  /* Do the same for the next index file */
  // TODO use a function for this and the previous one
  if (have_next_index && do_previous) {
    index_data = next_created;
    size_max = mIndex.index_next->GetNumberCreatedParticles(type);
    for (size_t i = 0; i < size_max; i++) {
      /* Stop as soon as we reach the required offset */
      if (index_data[i].offset > offset_time) break;

      /* Now read and store the particle in the cache */
      int number_jumps = 0;
      int removed = ReadParticle(time, buffers, index_data[i].offset,
                                 /* output_index */ 0, type, number_jumps);

      /* Deal with the case where the particle is removed */
      if (removed) {
        /* No need to remove from the cache,
           the particle was not written into it. */
        continue;
      }

      /* Write the particle back into the array */
      for (size_t field = 0; field < output.size(); field++) {
        const Field &current = output[field].GetField();
        char *output_buffer = output[field][output_index];
        memcpy(output_buffer, buffers[field][0], current.GetFieldSize());
      }

      /* Update the counter */
      output_index++;
    }
  }

  /* Ensures that we found the correct number of particles */
  if (output_index - prev_npart != output[type].Size()) {
    csds_error("Some particles are missing: " << output_index - prev_npart
                                              << " != " << output[type].Size());
  }
}

void Reader::UpdateParticles(
    double time, std::vector<CsdsArray> &output,
    const std::array<uint64_t, csds_type_count> &n_part) {
  const chrono::high_resolution_clock::time_point init =
      chrono::high_resolution_clock::now();

  const TimeArray &times = mLog->GetTimeArray();

  /* Check if the cache is enabled */
  if (!mUseCache) {
    csds_error("Cannot update the particles without cache");
  }

  /* Check if we are not evolving the particles over more than one index file */
  for (size_t i = 0; i < mIndex.times.size(); i++) {
    if (mIndex.times[i] == mIndex.index_prev->GetTime()) {
      if (i == 0) break;

      const size_t offset = times.GetIndexFromTime(mIndex.times[i - 1]);
      if (mCache[0].GetLastTimeOffset() < offset) {
        csds_error(
            "Trying to update the particles over more than one"
            " index file");
      }
      break;
    }
  }

  /* Prepare the cache */
  for (int i = 0; i < csds_type_count; i++) {
    mCache[i].ResetCurrentIndex();
  }

  /* Update the particles */
  for (int i = 0; i < csds_type_count; i++) {
    if (n_part[i] != 0) {
      UpdateParticlesSingleType(time, output, n_part, (part_type)i);
    }
    /* Update the last offset read */
    mCache[i].SetLastTimeOffset(mTime.time_offset);
  }

  /* Print the time */
  if (mVerbose > 0 || mVerbose == CSDS_VERBOSE_TIMERS)
    message("took " << GetDeltaTime(init) << "ms");
}
