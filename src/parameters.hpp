/*******************************************************************************
 * This file is part of CSDS.
 * Copyright (c) 2019 Loic Hausammann (loic.hausammann@epfl.ch)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/**
 * @file csds_parameters.h
 * @brief This file contains the functions that deals with the yaml file.
 */

#ifndef CSDS_PARAMETERS_H
#define CSDS_PARAMETERS_H

/* Local include */
#include "cosmology.hpp"
#include "particle_type.hpp"

/* Standard headers */
#include <array>
#include <stdint.h>
#include <string>

/**
 * @brief Structure containing the simulation's parameters.
 */
class Parameters {

 public:
  Parameters() {}

  /**
   * @brief Initialize the parameters from the yaml file.
   * @param filename The filename to use.
   * @param verbose the verbose level
   */
  Parameters(const std::string filename, int verbose);

  /* Getters and setters */
  int GetDimension() const { return mDimension; }
  std::array<double, 3> const& GetBoxSize() const { return mBoxSize; }
  bool IsPeriodic() const { return mPeriodic; }
  bool IsWithCosmology() const { return mWithCosmology; }
  std::array<long long, csds_type_count> const& GetApproximateNumberParticles()
      const {
    return mApproximateNumberParticles;
  }
  Cosmology const& GetCosmology() const { return mCosmo; }
  float GetCacheOverAllocation() const { return mCacheOverAllocation; }
  std::array<int64_t, csds_type_count> const& GetCacheInitialSize() const {
    return mCacheInitSize;
  }

 protected:
  /* Number of dimension */
  int mDimension;

  /* Simulation volume */
  std::array<double, 3> mBoxSize;

  /* Is the volume periodic? */
  bool mPeriodic;

  /* Are we doing a cosmological simulation? */
  bool mWithCosmology;

  /* Number of particles initially present. */
  std::array<long long, csds_type_count> mApproximateNumberParticles;

  /* The cosmological model */
  Cosmology mCosmo;

  /* Over allocation for the cache. */
  float mCacheOverAllocation;

  /* Initial cache for in-existing particles */
  std::array<int64_t, csds_type_count> mCacheInitSize;
};

#endif  // CSDS_PARAMETERS_H
