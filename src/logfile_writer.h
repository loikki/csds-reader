/*******************************************************************************
 * This file is part of CSDS
 * Copyright (c) 2021 loic.hausammann@epfl.ch
 *               2016 Pedro Gonnet (pedro.gonnet@durham.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#ifndef CSDS_LOGFILE_WRITER_H
#define CSDS_LOGFILE_WRITER_H

/* WARNING THIS FILE SHOULD NOT INCLUDE CONFIG.H*/

/* Include local headers */
#include "definitions.hpp"
#include "inline.hpp"

/* Standard headers */
#include <omp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

/** The structure for writing the logfile. */
struct csds_logfile_writer {

  /* The memory-mapped data of this dump. */
  char *data;

  /* The size of the memory-mapped data, in bytes. */
  size_t size;

  /* The number of bytes that have been dumped. */
  size_t count;

  /* The offset of the data within the current file. */
  size_t file_offset;

  /* The file with which this memory is associated. */
  int fd;

  /* Mask containing the significant bits for page addresses. */
  size_t page_mask;
};

/* Function prototypes. */
EXTERN void csds_logfile_writer_init(struct csds_logfile_writer *d,
                                     const char *filename, size_t size);
EXTERN void csds_logfile_writer_restart(struct csds_logfile_writer *d,
                                        const char *filename);
EXTERN void csds_logfile_writer_ensure(struct csds_logfile_writer *d,
                                       size_t required_size, size_t increase_size);
EXTERN void csds_logfile_writer_sync(struct csds_logfile_writer *d);
EXTERN void csds_logfile_writer_close(struct csds_logfile_writer *d);
EXTERN float csds_logfile_writer_get_current_filesize_used_gb(
    const struct csds_logfile_writer *log);
EXTERN char *csds_logfile_writer_write_begining_header(
    struct csds_logfile_writer *logfile);

/**
 * @brief Write the header of a record (offset + mask).
 *
 * This is maybe broken for big(?) endian.
 *
 * @param buff The buffer where to write the mask and offset.
 * @param mask The mask to write inside the buffer.
 * @param offset The offset of the previous record.
 * @param offset_new The offset of the current record.
 *
 * @return updated buff
 */
INLINE static char *csds_write_record_header(char *buff,
                                             const unsigned int *mask,
                                             const size_t *offset,
                                             const size_t offset_new) {
  /* write mask. */
  memcpy(buff, mask, CSDS_MASK_SIZE);
  buff += CSDS_MASK_SIZE;

  /* write offset. */
  uint64_t diff_offset = offset_new - *offset;
  memcpy(buff, &diff_offset, CSDS_OFFSET_SIZE);
  buff += CSDS_OFFSET_SIZE;

  return buff;
}

/**
 * @brief Obtain a chunk of memory from a csds_logfile_writer.
 *
 * @param d The #csds_logfile_writer.
 * @param count The number of bytes requested.
 * @param offset The offset of the returned memory address within the
 * csds_logfile_writer file.
 * @return A pointer to the memory-mapped chunk of data.
 */
INLINE static char *csds_logfile_writer_get(struct csds_logfile_writer *d,
                                            size_t count, size_t *offset) {
  size_t local_offset = 0;

  // Without OMP, this is strongly thread unsafe
#pragma omp atomic capture
  {
    local_offset = d->count;
    d->count += count;
  }

  if (d->count > d->size) {
    printf("The logfile is too small.\n");
    exit(1);
  }
  *offset = local_offset + d->file_offset;
  return (char *)d->data + local_offset;
}

/**
 * @brief Generate the data for the special flags.
 *
 * @param flag The special flag to use.
 * @param flag_data The data to write in the record.
 * @param type The type of the particle.
 */
INLINE static uint32_t csds_pack_flags_and_data(enum csds_special_flags flag,
                                                int flag_data, int type) {
  if (flag & 0xFFFFFF00) {
    printf(
        "The special flag in the particle CSDS cannot"
        "be larger than 1 byte.\n");
    exit(1);
  }
  if (flag_data & ~0xFFFF) {
    printf(
        "The data for the special flag in the particle "
        "CSDS cannot be larger than 2 bytes.");
    exit(1);
  }
  return ((uint32_t)flag << (3 * 8)) | ((flag_data & 0xFFFF) << 8) |
         (type & 0xFF);
}

/**
 * @brief Write to the logfile.
 *
 * @param d #csds_logfile_writer file
 * @param offset (return) offset of the data
 * @param size number of bytes to write
 * @param p pointer to the data
 */
INLINE static void csds_write_data(struct csds_logfile_writer *d,
                                   size_t *offset, size_t size, const char *p) {
  /* get buffer. */
  char *buff = csds_logfile_writer_get(d, size, offset);

  /* write data to the buffer. */
  memcpy(buff, p, size);

  /* Update offset to end of record. */
  *offset += size;
}

/**
 * @brief Conclude the logfile header.
 *
 * @param logfile The #csds_logfile_writer.
 * @param offset The place where to write the offset
 * to the first record (provided by
 * #csds_logfile_writer_write_beginning_header)
 */
INLINE static void csds_logfile_writer_write_end_header(
    ATTR_UNUSED struct csds_logfile_writer *logfile, char *offset) {
  /* last step: write first offset. */
  size_t file_offset = logfile->file_offset + logfile->count;
  memcpy(offset, &file_offset, CSDS_OFFSET_SIZE);

#ifndef CSDS_TEST
  /* Ensure that OpenMP is defined */
  __attribute__((unused)) int test = omp_get_thread_num();
#endif
}

#endif /* CSDS_LOGFILE_WRITER_H */
