# This file is part of CSDS.
# Copyright (C) 2021 loic.hausammann@epfl.ch.
#               SWIFT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Define the version
m4_define([major_version], [2])
m4_define([minor_version], [0])

# Init the project.
AC_INIT([CSDS],[major_version.minor_version],
    [https://gitlab.cosma.dur.ac.uk/lhausammann/csds-reader])
csds_config_flags="$*"

# Save the version
AC_SUBST([MAJOR_VERSION],[major_version])
AC_SUBST([MINOR_VERSION],[minor_version])

#  We want to stop when given unrecognised options. No subdirs so this is safe.
enable_option_checking=${enable_option_checking:-fatal}
if test -n "$ac_unrecognized_opts"; then
    case $enable_option_checking in
        no)
        ;;
        fatal)
            { $as_echo "$as_me: error: unrecognized options: $ac_unrecognized_opts" >&2
              { (exit 1); exit 1; }; }
        ;;
        *)
            $as_echo "$as_me: WARNING: unrecognized options: $ac_unrecognized_opts" >&2
        ;;
    esac
fi

AC_COPYRIGHT
AC_CONFIG_SRCDIR([src/header.cpp])
AC_CONFIG_AUX_DIR([.])
AM_INIT_AUTOMAKE([subdir-objects])

# Add local macro collection.
AC_CONFIG_MACRO_DIR([m4])

# Stop default CXXFLAGS from anyone except the environment.
: ${CXXFLAGS=""}

# Use c++11
CXXFLAGS="$CXXFLAGS -std=c++11"

# Generate header file.
AM_CONFIG_HEADER(config.hpp)

# Find and test the compiler.
AX_CHECK_ENABLE_DEBUG
AC_LANG(C++)
AC_PROG_CXX
AC_PROG_CXX_C_O
AC_OPENMP

# Check if openmp is disabled
with_openmp="yes"
if [[ -z "$ac_cv_prog_cxx_openmp" ]]
then
  CXXFLAGS="$CXXFLAGS -Wno-unknown-pragmas"
  with_openmp="no"
else
  AC_DEFINE([CSDS_WITH_OPENMP], 1, [Compile the code with OpenMP])
fi

# Check for compiler version and vendor.
AX_COMPILER_VENDOR
AX_COMPILER_VERSION

# Add libtool support (now that CXX is defined).
LT_INIT

# If debugging try to show inlined functions.
if test "$enable_debug" = "yes"; then
   #  Show inlined functions.
   if test "$ax_cv_cxx_compiler_vendor" = "gnu"; then
      # Would like to use -gdwarf and let the compiler pick a good version
      # but that doesn't always work.
      AX_CHECK_COMPILE_FLAG([-gdwarf -fvar-tracking-assignments],
        [inline_EXTRA_FLAGS="-gdwarf -fvar-tracking-assignments"],
        [inline_EXTRA_FLAGS="-gdwarf-2 -fvar-tracking-assignments"])
      CXXFLAGS="$CXXFLAGS $inline_EXTRA_FLAGS"
   elif test "$ax_cv_cxx_compiler_vendor" = "intel"; then
      CXXFLAGS="$CXXFLAGS -debug inline-debug-info"
   fi
else
  case $ax_cv_cxx_compiler_vendor in
  gnu)
    # default optimization flags for gcc on all systems
    CXXFLAGS="$CXXFLAGS -O3 -fomit-frame-pointer"

    # -malign-double for x86 systems
    AX_CHECK_COMPILE_FLAG(-malign-double, CXXFLAGS="$CXXFLAGS -malign-double")

    #  -fstrict-aliasing for gcc-2.95+
    AX_CHECK_COMPILE_FLAG(-fstrict-aliasing,
    CXXFLAGS="$CXXFLAGS -fstrict-aliasing")

    # note that we enable "unsafe" fp optimization with other compilers, too
    AX_CHECK_COMPILE_FLAG(-ffast-math, CXXFLAGS="$CXXFLAGS -ffast-math")

    # not all codes will benefit from this.
    AX_CHECK_COMPILE_FLAG(-funroll-loops, CXXFLAGS="$CXXFLAGS -funroll-loops")

    AX_GCC_ARCHFLAG($acx_maxopt_portable)
    ;;
  *)
    echo WARNING: your compiler is not known, optimizations will only use -O3
    CXXFLAGS="$CXXFLAGS -O3"
    ;;
  esac
fi

# Check if expensive debugging is on.
AC_ARG_ENABLE([debugging-checks],
   [AS_HELP_STRING([--enable-debugging-checks],
     [Activate expensive consistency checks @<:@yes/no@:>@]
   )],
   [enable_debugging_checks="$enableval"],
   [enable_debugging_checks="no"]
)
if test "$enable_debugging_checks" = "yes"; then
   AC_DEFINE([CSDS_DEBUG_CHECKS],1,[Enable expensive debugging])
fi

# Check if the mmap tracking should be enabled.
AC_ARG_ENABLE([mmap-tracking],
  [AS_HELP_STRING([--enable-mmap-tracking],
    [Activate expensive mmap tracking @<:@yes/no@:>@]
  )],
  [enable_mmap_tracking="$enableval"],
  [enable_mmap_tracking="no"]
)
if test "$enable_mmap_tracking" = "yes"; then
   AC_DEFINE([CSDS_MMAP_TRACKING],1,[Enable mmap tracking])
fi


# Add address sanitizer options to flags, if requested. Only useful for GCC
# version 4.8 and later and clang.
AC_ARG_ENABLE([sanitizer],
   [AS_HELP_STRING([--enable-sanitizer],
     [Enable memory error detection using address sanitizer @<:@no/yes@:>@]
   )],
   [enable_san="$enableval"],
   [enable_san="no"]
)

if test "$enable_san" = "yes"; then
   if test "$ax_cv_cxx_compiler_vendor" = "gnu"; then
      AX_COMPARE_VERSION( $ax_cv_cxx_compiler_version, [ge], [4.8.0],
                          [enable_san="yes"], [enable_san="no"] )
   elif test "$ax_cv_cxx_compiler_vendor" = "clang"; then
      AX_COMPARE_VERSION( $ax_cv_cxx_compiler_version, [ge], [3.2.0],
                          [enable_san="yes"], [enable_san="no"] )
   fi
   if test "$enable_san" = "yes"; then
      CXXFLAGS="$CXXFLAGS -fsanitize=address -fno-omit-frame-pointer"
      AC_MSG_RESULT([added address sanitizer support])
   else
      AC_MSG_WARN([Compiler does not support address sanitizer option])
   fi
fi
AM_CONDITIONAL([HAVE_SANITIZER], [test x$enable_san = xyes])

# Add the undefined sanitizer option to flags. Only useful for GCC
# version 4.9 and later and clang to detected undefined code behaviour
# such as integer overflow and memory alignment issues.
AC_ARG_ENABLE([undefined-sanitizer],
   [AS_HELP_STRING([--enable-undefined-sanitizer],
     [Enable detection of code that causes undefined behaviour @<:@no/yes@:>@]
   )],
   [enable_ubsan="$enableval"],
   [enable_ubsan="no"]
)

if test "$enable_ubsan" = "yes"; then
   if test "$ax_cv_cxx_compiler_vendor" = "gnu"; then
      AX_COMPARE_VERSION( $ax_cv_cxx_compiler_version, [ge], [4.9.0],
                          [enable_ubsan="yes"], [enable_ubsan="no"] )
   elif test "$ax_cv_cxx_compiler_vendor" = "clang"; then
      AX_COMPARE_VERSION( $ax_cv_cxx_compiler_version, [ge], [3.7.0],
                          [enable_ubsan="yes"], [enable_ubsan="no"] )
   fi
   if test "$enable_ubsan" = "yes"; then
      CXXFLAGS="$CXXFLAGS -fsanitize=undefined"
      AC_MSG_RESULT([added undefined sanitizer support])
   else
      AC_MSG_WARN([Compiler does not support undefined sanitizer option])
   fi
fi

# Add the code coverage
AC_ARG_ENABLE([coverage],
   [AS_HELP_STRING([--enable-coverage],
     [Enable gcc's coverage tool @<:@no/yes@:>@]
   )],
   [enable_coverage="$enableval"],
   [enable_coverage="no"]
)

if test "$enable_coverage" = "yes"; then
   if test "$ax_cv_cxx_compiler_vendor" = "gnu"; then
      CXXFLAGS="$CXXFLAGS -fprofile-arcs -ftest-coverage"
      # Remove inline in order to profile headers
      CXXFLAGS="$CXXFLAGS -fno-inline -fno-inline-small-functions -fno-default-inline"
      # unused function is an issue without inlining
      CXXFLAGS="$CXXFLAGS -Wno-unused-function"
      # Add to linker
      LDFLAGS="$LDFLAGS -fprofile-arcs -lstdc++"
   else
      AC_MSG_ERROR(Cannot use coverage without gcc)
   fi
   AC_DEFINE([CSDS_NO_INLINE], 1, [Disable forced inlining])
fi

# Autoconf stuff.
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_HEADER_STDC

# Check for python.
AC_ARG_ENABLE([python],
    [AS_HELP_STRING([--enable-python],
       [Enable the python wrapper (requires boost)]
    )],
    [],
    [enable_python=no]
)
if test "$enable_python" != "no"; then
   PYTHON_VERSION=3
   AX_PYTHON_DEVEL([>= '3'])
   enable_python="yes"
   AC_DEFINE([HAVE_PYTHON],1,[Python appears to be present.])

   # Now add boost
   AX_BOOST_BASE([1.63.0], [],
       [AC_MSG_ERROR(something is wrong with the boost library!)])
   AX_BOOST_PYTHON

   # Add numpy
   PYTHON_VERSION=`$PYTHON -c "import sys; \
                           ver = sys.version[[:1]]; \
                           print(ver.replace('.', ''))"`
   BOOST_PYTHON_LIB="$BOOST_PYTHON_LIB -lboost_numpy$PYTHON_VERSION"

fi
AC_SUBST([PYTHON_CPPFLAGS])
AC_SUBST([PYTHON_LIBS])
AM_CONDITIONAL([HAVEPYTHON],[test -n "$PYTHON_CPPFLAGS"])


# Check for timing functions needed by csds_cycle.h.
AC_HEADER_TIME
AC_CHECK_HEADERS([sys/time.h c_asm.h intrinsics.h mach/mach_time.h])
AC_CHECK_TYPE([hrtime_t],[AC_DEFINE(HAVE_HRTIME_T, 1, [Define to 1 if hrtime_t
is defined in <sys/time.h>])],,
[#if HAVE_SYS_TIME_H
#include <sys/time.h>
#endif])
AC_CHECK_FUNCS([gethrtime read_real_time time_base_to_time clock_gettime mach_absolute_time])
AC_MSG_CHECKING([for _rtc intrinsic])
rtc_ok=yes
AC_LINK_IFELSE([AC_LANG_PROGRAM(
[[#ifdef HAVE_INTRINSICS_H
#include <intrinsics.h>
#endif]],
[[_rtc()]])],
[AC_DEFINE(HAVE__RTC,1,[Define if you have the UNICOS _rtc() intrinsic.])],[rtc_ok=no])
AC_MSG_RESULT($rtc_ok)

# Add warning flags by default, if these can be used. Option =error adds
# -Werror to GCC, clang and Intel.  Note do this last as compiler tests may
# become errors, if that's an issue don't use CXXFLAGS for these, use an AC_SUBST().
AC_ARG_ENABLE([compiler-warnings],
   [AS_HELP_STRING([--enable-compiler-warnings],
     [Enable compile time warning flags, if compiler is known @<:@error/no/yes)@:>@]
   )],
   [enable_warn="$enableval"],
   [enable_warn="error"]
)
if test "$enable_warn" != "no"; then
    # AX_CXXFLAGS_WARN_ALL does not give good warning flags for the Intel compiler
    # We will do this by hand instead and only default to the macro for unknown compilers
    case "$ax_cv_cxx_compiler_vendor" in
          gnu | clang)
             CXXFLAGS="$CXXFLAGS -Wall -Wextra -Wshadow"
          ;;
	  intel)
             CXXFLAGS="$CXXFLAGS -w2 -Wunused-variable -Wshadow"
          ;;
	  *)
	     AX_CXXFLAGS_WARN_ALL
	  ;;
    esac

    # Add a "choke on warning" flag if it exists
    if test "$enable_warn" = "error"; then
       case "$ax_cv_cxx_compiler_vendor" in
          intel | gnu | clang)
             CXXFLAGS="$CXXFLAGS -Werror"
          ;;
       esac
    fi
fi

# Handle .in files.
AC_CONFIG_FILES([Makefile src/Makefile tests/Makefile])

# Save the compilation options
AC_DEFINE_UNQUOTED([CSDS_CONFIG_FLAGS],["$csds_config_flags"],[Flags passed to configure])

# Generate output.
AC_OUTPUT

# Report general configuration.
AC_MSG_RESULT([
 ------- Summary --------

   $PACKAGE_NAME v.$PACKAGE_VERSION

   Compiler             : $CXX
    - vendor            : $ax_cv_cxx_compiler_vendor
    - version           : $ax_cv_cxx_compiler_version
    - flags             : $CXXFLAGS $OPENMP_CXXFLAGS
   OpenMP enabled       : $with_openmp

   Python enabled       : $enable_python
    - includes          : $PYTHON_CPPFLAGS $BOOST_CPPFLAGS
    - libraries         : $PYTHON_LIBS $BOOST_LDFLAGS $BOOST_PYTHON_LIB

   Debugging checks     : $enable_debugging_checks
   Coverage enabled     : $enable_coverage
   Sanitizer enabled    : $enable_san

------------------------])
