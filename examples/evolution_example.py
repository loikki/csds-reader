#!/usr/bin/env python3
"""
Read a csds file by using an index file.
Example: ./reader_example.py -t 0.1 ../../examples/SedovBlast_3D/index_*dump
"""
import sys
from glob import glob
import argparse
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
sys.path.append("../src/.libs/")

import libcsds as csds


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Read a logfile and plots some basic properties')

    default_files = "../../examples/SmallCosmoVolume/SmallCosmoVolume_DM/index*.dump"
    default_files = glob(default_files)

    parser.add_argument("-n", '--number_steps', dest='n',
                        type=int, default=20,
                        help='Number of steps in the evolution')
    parser.add_argument('files', metavar='filenames', type=str, nargs="*",
                        help='The filenames of the logfiles')
    args = parser.parse_args()
    if len(args.files) == 0:
        args.files = default_files
    return args


def plot_center_mass(pos):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot(pos[:, 0], pos[:, 1], pos[:, 2], markersize=0.1)


args = parse_arguments()
print("basename: %s" % args.files)
print("Number steps: %i" % args.n)

# read the csds
positions = np.zeros((len(args.files), args.n, 3))
n_particles = np.zeros((len(args.files), args.n), dtype=int)
m_tot = np.zeros((len(args.files), args.n))
file_index = -1
for f in args.files:
    file_index += 1
    if f.endswith(".dump"):
        filename = f[:-5]
    else:
        raise Exception(
            "It seems that you are not providing a logfile (.dump)")
    with csds.Reader(filename, verbose=0, number_index=10,
                     restart_init=True, use_cache=True) as reader:

        # Ensure that the fields are present
        fields = ["Coordinates", "Masses", "ParticleIDs"]
        missing = set(fields).difference(
            set(reader.get_list_fields(part_type=csds.particle_types.gas)))

        if missing:
            raise Exception("Fields %s not found in the logfile." % missing)

        # Get the time limits
        t0, t1 = reader.get_time_limits()

        # Read all the particles
        out = reader.get_data(
            fields=fields, time=t0,
            part_type=csds.particle_types.dark_matter)

        times = np.linspace(t1, t0, args.n, endpoint=False)[::-1]
        for i, t in enumerate(times):
            # Update the particles
            out = reader.update_particles(fields=fields, time=t)

            n_particles[file_index, i] = len(out["ParticleIDs"])
            m_tot[file_index, i] = np.sum(out["Masses"])
            cm = np.sum(out["Masses"][:, np.newaxis] * out["Coordinates"],
                        axis=0)
            cm /= m_tot[file_index, i]
            positions[file_index, i, :] = cm

# Check the number of particles
total = np.sum(n_particles[:, 0])
for i in range(args.n):
    tmp = np.sum(n_particles[:, i])
    if total != tmp:
        raise Exception(
            "A particle is missing: ", tmp, total)

# Compute the CM
positions = np.sum(positions * m_tot[:, :, np.newaxis], axis=0)
positions /= np.sum(m_tot, axis=0)[:, np.newaxis]
plot_center_mass(positions)
plt.show()
