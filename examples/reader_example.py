#!/usr/bin/env python3
"""
Read a csds file by using an index file.
Example: ./reader_example.py -t 0.1 ../../examples/SedovBlast_3D/index_*dump
"""
import sys
from glob import glob
import argparse
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
sys.path.append("../src/.libs/")

import libcsds as csds


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Read a logfile and plots some basic properties')

    default_files = "../../examples/HydroTests/SedovBlast_3D/index_*dump"
    default_files = glob(default_files)

    parser.add_argument("-t", '--time', dest='time',
                        type=float, default=0.01,
                        help='Simulation time')
    parser.add_argument('files', metavar='filenames', type=str, nargs="*",
                        help='The filenames of the logfiles')
    args = parser.parse_args()
    if len(args.files) == 0:
        args.files = default_files
    return args


def plot3D(pos):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot(pos[:, 0], pos[:, 1], pos[:, 2], ".", markersize=0.1)


def plot2D(pos, energies):
    plt.figure()
    center = np.array([0.5]*3)
    r2 = np.sum((pos - center)**2, axis=1)

    # plot energy vs distance
    plt.plot(np.sqrt(r2), energies, '.',
             markersize=0.2)

    plt.xlabel("Radius")
    plt.ylabel("Internal Energy")


args = parse_arguments()
print("basename: %s" % args.files)
print("time: %g" % args.time)

# read the csds
positions = np.empty((0, 3))
energies = np.empty(0)
for f in args.files:
    if f.endswith(".dump"):
        filename = f[:-5]
    else:
        raise Exception("It seems that you are not providing a logfile (.dump)")
    with csds.Reader(filename, verbose=0, number_index=10,
                     restart_init=False, use_cache=True) as reader:

        # Check the time limits
        t0, t1 = reader.get_time_limits()
        if t0 > args.time > t1:
            raise Exception("The time is outside the simulation range")

        # Ensure that the fields are present
        fields = ["Coordinates", "InternalEnergies", "ParticleIDs"]
        missing = set(fields).difference(
            set(reader.get_list_fields(part_type=csds.particle_types.gas)))

        if missing:
            raise Exception("Fields %s not found in the logfile." % missing)

        if ("Coordinates" not in fields or
            "InternalEnergies" not in fields):
            raise Exception("Field not found in the logfile")

        # Rewind a bit the time in order to update the particles
        dt = 1e-3 * (args.time - t0)

        # Read all the particles
        out = reader.get_data(
            fields=fields, time=args.time - dt,
            part_type=csds.particle_types.gas)

        # Update the particles
        out = reader.update_particles(fields=fields, time=args.time)

        # Get the particle ids and select a fraction of the IDs
        gas_ids = out["ParticleIDs"]
        gas_ids = gas_ids[:len(gas_ids)//2]

        # Read from the ids
        # As we are filtering by particle ids, the field "ParticleIDs" is
        # required in order to verify the particle obtained.
        ids = [None] * csds.particle_types.count
        ids[csds.particle_types.gas] = gas_ids
        out = reader.get_data(
            fields=fields, time=args.time, filter_by_ids=ids)

        # Print the missing ids
        gas_ids, ids_found = set(gas_ids), set(out["ParticleIDs"])
        diff_ids = list(gas_ids.difference(ids_found))
        diff_found = list(ids_found.difference(gas_ids))
        print("The following ids were not found: ",
              np.array(diff_ids))
        print("The following ids are wrongly missing: ",
              np.array(diff_found))

        # add the data to the list
        positions = np.append(positions, out["Coordinates"], axis=0)
        energies = np.append(energies, out["InternalEnergies"], axis=0)

print("Min/Max of the position:", positions.min(), positions.max())
print("Min/Max of the energy:", energies.min(), energies.max())
plot3D(positions)
plot2D(positions, energies)
plt.show()
