#!/usr/bin/env python3
"""
Read a CSDS file by using an index file and then write a snapshot.
Example: python3 create_snapshot -t 0.1 -o out.hdf5 ../../examples/SedovBlast_3D/index_*dump
"""
import sys
import h5py
import numpy as np
from glob import glob
import argparse
sys.path.append("../src/.libs/")

import libcsds as csds


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Read a logfile and plots some basic properties')

    default_files = "../../examples/HydroTests/SedovBlast_3D/index_*dump"
    default_files = glob(default_files)

    parser.add_argument("-o", '--output', dest='out',
                        type=str, default="out.hdf5",
                        help='Output filename')
    parser.add_argument("-t", '--time', dest='time',
                        type=float, default=0.01,
                        help='Simulation time')
    parser.add_argument('files', metavar='filenames', type=str, nargs="*",
                        help='The filenames of the logfiles')
    args = parser.parse_args()
    if len(args.files) == 0:
        args.files = default_files
    return args


def write_particle_type(snap, part_type, args):
    """
    Write a particle type inside an HDF5 file.

    Parameters
    ----------

    snap: h5py.File
        The output file.

    part_type: int
        The particle type to write

    args: ArgumentParser
        The argument parser

    Returns
    -------

    npart: int
        The number of particles of the given type.
    """
    fields = None
    fields_name = None

    # Read all the fields
    for f in args.files:
        if f.endswith(".dump"):
            filename = f[:-5]
        else:
            raise Exception("It seems that you are not providing a logfile (.dump)")

        # Open the csds
        with csds.Reader(filename, verbose=0) as reader:

            t0, t1 = reader.get_time_limits()
            if args.time < t0 or args.time > t1:
                raise Exception("Unexpected time")

            # Get the list of fields
            if fields_name is None:
                fields_name = reader.get_list_fields(part_type)

            # Skip non implemented types
            if len(fields_name) == 0:
                print("Part type %i not implemented, skipping it" % part_type)
                continue

            # Read the fields
            fields_tmp = reader.get_data(
                fields=fields_name, time=args.time, part_type=part_type)

            if fields is None:
                fields = fields_tmp
            else:
                for i, field in enumerate(fields):
                    fields[i] = np.append(fields[i], fields_tmp[i], axis=0)

    # Do we have this particle type?
    if fields is None:
        return 0
    first_field = list(fields.values())[0]
    if first_field.shape[0] == 0:
        return 0

    # Get the number of particles
    npart = first_field.shape[0]

    # Create the group
    name = "PartType%i" % part_type
    grp = snap.create_group(name)

    # Save the data
    for field in fields_name:
        grp.create_dataset(field, data=fields[field])

    return npart


if __name__ == "__main__":
    args = parse_arguments()
    print("Output: %s" % args.out)
    print("basename: %s" % args.files)
    print("time: %g" % args.time)

    # read the csds
    n_types = 6

    # Create a snapshot
    with h5py.File(args.out, "w") as snap:
        npart = np.zeros(n_types)
        for part_type in range(n_types):
            npart[part_type] = write_particle_type(snap, part_type, args)


        # Write the header
        grp = snap.create_group("Header")
        grp.attrs["NumPart_Total"] = npart
        grp.attrs["NumPart_Total_HighWord"] = [0] * n_types
        grp.attrs["NumPart_ThisFile"] = npart
        grp.attrs["Time"] = args.time
        grp.attrs["NumFilesPerSnapshot"] = 1
        grp.attrs["MassTable"] = [0.0] * n_types

