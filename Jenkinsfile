pipeline {
    agent any
    stages {
       stage('Standard') {
          steps {

             updateGitlabCommitStatus name: 'Standard', state: 'pending'

             
             sh '''#!/bin/bash

             set -x
             set -e

             module purge
             module load gnu_comp/10.2.0
             module load llvm/10.0.1
             export CLANG_FORMAT_CMD="clang-format"

             git clean -fdx
             ./autogen.sh

             ./configure
             make -j 2
             make check
             make clean
             '''

             updateGitlabCommitStatus name: 'Standard', state: 'success'
          }
       }
       stage('Python') {
           steps {
             updateGitlabCommitStatus name: 'Python', state: 'pending'

             sh '''#!/bin/bash

             set -x
             set -e

             module purge
             module avail
             module load gnu_comp/10.2.0
             module load boost
             module load llvm/10.0.1
             module load python/3.6.5
             export CLANG_FORMAT_CMD="clang-format"

             git clean -fdx
             ./autogen.sh

             ls /cosma/local/boost/gnu_10.2.0/1_67_0/lib
             # Python is messy => cannot fail on warning
             # ./configure --enable-python --enable-compiler-warnings=yes
             # make -j 2
             # make check
             # make clean
             '''

             updateGitlabCommitStatus name: 'Python', state: 'success'
           }
       }
       stage('No OpenMP') {
           steps {
             updateGitlabCommitStatus name: 'No OpenMP', state: 'pending'

             sh '''#!/bin/bash

             set -x
             set -e

             module purge
             module load gnu_comp/10.2.0
             module load python/3.8.7-C8  # For coverage
             module load llvm/10.0.1
             export CLANG_FORMAT_CMD="clang-format"

             git clean -fdx
             ./autogen.sh

             ./configure --disable-openmp
             make -j 2
             make check            
             make clean
             '''

             updateGitlabCommitStatus name: 'No OpenMP', state: 'success'
           }
       }
       stage('Debugging/Coverage') {
           steps {
             updateGitlabCommitStatus name: 'Debugging/Coverage', state: 'pending'

             sh '''#!/bin/bash

             set -x
             set -e

             module purge
             module load gnu_comp/10.2.0
             module load python/3.8.7-C8  # For coverage
             module load llvm/10.0.1
             export CLANG_FORMAT_CMD="clang-format"

             git clean -fdx
             ./coverage.sh -c -r -i
             make clean
             '''

             updateGitlabCommitStatus name: 'Debugging/Coverage', state: 'success'
           }
           post {
              always {
                  step([$class: 'CoberturaPublisher', autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'coverage.xml', failUnhealthy: false, failUnstable: false, maxNumberOfBuilds: 0, onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false])
              }
          }
       }
       stage('Intel') {
           steps {
             updateGitlabCommitStatus name: 'Intel', state: 'pending'

             sh '''#!/bin/bash

             set -x
             set -e

             module purge
             module load intel_comp/2018
             module load llvm/10.0.1
             export CLANG_FORMAT_CMD="clang-format"

             git clean -fdx
             ./autogen.sh

             ./configure --enable-debugging-checks
             make -j 2
             make check
             make clean
             '''

             updateGitlabCommitStatus name: 'Intel', state: 'success'
           }
       }
    }
 }
